Lecture 12: Semantic substructural typing
=========================================

Systems programming languages such as C and C++ avoid the use of a garbage
collector by requiring the programmer to manually deallocate/free memory when
it is no longer used. While manually memory management improves performance and
is necessary for certain systems applications, it comes at the price that one
can easily create programs that suffer from use-after-free or double-free bugs,
or have memory leaks.

In this lecture, we will investigate **ownership-based**, also known as
**substructural**, type systems for languages with manual memory management.
These type systems statically ensure the absence of memory-related bugs.
Concretely, we will study a variant of the simply-typed lambda-calculus with
references and a `free` operation. This language can be seen as a mini version
of Rust (but without borrowing, lifetimes, traits, concurrency, etc). The
theorem we prove for our language is:

**Theorem: Strong type safety** If `A ∈ {Unit,Int,Bool}` and `∅ ⊢ e : A` then
1. `e` cannot have use-after-free and double-free
2. `e` does not have memory leaks

The topics we will study are:

- Substructural typing rules
- Copyable types and function types
- Subtyping
- Type safety via semantic typing in separation logic

## Motivation

Let us assume we have a type system similar to the one from week 6, but
extended with a `free` operation. Naively, we could give the memory access the
following types:

```
alloc : A → Ref A
load  : Ref A → A
store : Ref A → A → Unit
free  : Ref A → Unit
```

**Question** Does this language satisfy our strong type safety theorem?

**No**, see the following counterexamples:

```
f₁ := (λ l, free l; load l)         : Ref A → A              (* Use after free *)
f₂ := (λ l, free l; free l)         : Ref A → Unit           (* Double-free *)
f₃ := (λ l k, free l; free k)       : Ref A → Ref B → Unit   (* OK *)
f₄ := (λ l, f₃ l l)                 : ref A → Unit           (* Double-free *)
f₅ := (λ x, let l := alloc x in ()) : A → Unit               (* Memory leak *)
f₆ := (λ x, let l := alloc (alloc x) in free l) : A → Unit   (* Memory leak *)
```

The problems that occur are as follows:

- After a `free`, the location can still be used (example 1)
- After a `free`, the location be freed again (example 3,4)
- One can forget to call `free` on a reference itself (example 5) or on a nested
  reference (example 6).

A similar problem occurs if we were to attempt to add safe APIs to access other
resources, such as files. The type system will not be able to guarantee that a
file is not used after it has been closed.

## Substructural/ownership typing

The goal of a **substructural** or **ownership** type system is to restrict the
type system in such a way that buggy examples like the ones above become
ill-typed, and are thus statically rejected by the compiler. The intuitive idea
is to let the type system not only express the knowledge that a value has a
certain type, but also the sole ownership to use it and the obligation to free
it.

We start by studying a simple type system with ownership by adapting the
simply-typed lambda calculus, so let us first recapitulate the typing rules of
the ordinary simply-typed lambda calculus:

```
                     x:A; Γ ⊢ e : B       Γ ⊢ e₁ : A → B    Γ ⊢ e₂ : A
--------------     ------------------     ----------------------------
x:A; Γ ⊢ x : A     Γ ⊢ λ x, e : A → B             Γ ⊢ e₁ e₂ : B
```

In the simply-typed lambda calculus one can type-check terms in which a variable
is unused (e.g., `λ x y, x`) or is used multiple times (e.g., `λ x y, x y y`).
This can be formally described by two lemmas (which you could prove by
induction over the typing derivation):

- **Weakening** If `Γ ⊢ e : A` and `x ∉ FV(Γ)` then `Γ;x:B ⊢ e : A`
- **Contraction** If `Γ;x:B,y:B ⊢ e : A`, then `Γ;x:B ⊢ e[y:=x] : A`

The weakening lemma says that by adding extra unused variables, an expression
remains typed. The contraction lemma says that if a term can be typed using two
variables of identical type, it can also be typed by a single variable of that
type.

These properties are problematic if we want to have precise control over the
use of resources like locations. The idea of a substructural (or ownership) type
system is to restrict the typing rules so that (some of) these properties will
no longer hold.

The simplest such type system is the **linear (simply-typed) lambda calculus**:

```
                  x:A; Γ ⊢ e : B        Γ₁ ⊢ e₁ : A ⊸ B    Γ₂ ⊢ e₂ : A
-----------     ------------------      ------------------------------
x:A ⊢ x : A     Γ ⊢ λ x, e : A ⊸ B           Γ₁; Γ₂ ⊢ e₁ e₂ : B
```

(We have changed the notation of the function type `→` into the "lolly" `⊸`. The
significance of this change will be clarified later.)

The key differences w.r.t. the ordinary simply-typed lambda calculus are:

- In the variable rule, the context should be empty. This ensures that every
  variable is used **at least once**, and the type system does not satisfy
  weakening.
- In the application rule, the context should be split between the typing
  judgments for `e₁` and `e₂`. This ensures that every variable is used
  **at most once**, and the type system does not satisfy contraction.

Together, these properties ensure that every variable is used **exactly once**
and therefore the type system can be used to enforce an ownership disciple.

## Affine versus linear typing

One could also choose to only let go of the contraction lemma, but keep the
weakening lemma. This is achieved by restricting only the function application
rule, while keeping the variable rule unchanged. This gives rise to a type
system in which each variable is used at most once instead of exactly once. Such
a type system is called **affine**.

Affine type systems **do** guarantee the absence of use-after-free and
double-free, but **do not** guarantee the absence of memory leaks. One could
say that Rust is more like an affine type system instead of a linear one.
Using [`forget`](https://doc.rust-lang.org/core/mem/fn.forget.html) one
can "leak" memory without calling destructors, and using reference counted
pointers [`Rc`](https://doc.rust-lang.org/std/rc/struct.Rc.html) one can create
reference cycles that will never be freed.

Since we consider a rather simple language in this lecture (instead of Rust) we
discuss linear typing instead of affine typing, but most ideas in this lecture
are applicable to both linear and affine typing.

Aside from affine and linear typing, there are a number of other variations of
substructural type systems. Walker [1] gives a good overview.

## Product types

Now that we have our base language in place, let us add some additional
features. First, we will add product types `A₁ ⨯ A₂`. The typing rules are:

```
Γ₁ ⊢ e₁ : A₁    Γ₂ ⊢ e₂ : A₂      Γ₁ ⊢ e : A₁ ⨯ A₂   x₁:A₁; x₂:A₂; Γ ⊢ e' : B
----------------------------      -------------------------------------------
 Γ₁; Γ₂ ⊢ (e₁,e₂) : A₁ ⨯ A₂           Γ₁; Γ₂ ⊢ let (x₁,x₂) := e in e' : B
```

The typing rule for pair introduction `(e₁,e₂)` is similar to the usual one,
but the context is split between the typing judgments for `e₁` and `e₂`.

Pair elimination is more involved. One cannot simply have the usual projections
`fst : A₁ ⨯ A₂ ⊸ A₁` and `snd : A₁ ⨯ A₂ ⊸ A₂` from ordinary lambda calculus
With the projections `fst` and `snd`, one component of the pair would get lost.
It is thus common to use a pairing-`let`/pattern-match construct. (There is a
way to type the projections, see the challenge exercise in the Coq homework).

**Semantics** One could add a `let (x₁,x₂) := e in e'` construct to the
operational semantics. In a big-step semantics, the reduction rule would be:

```
e ⇓ (v₁,v₂)          e'[x₁:=v₁][x₂:=v₂] ⇓ w
-------------------------------------------
    (let (x₁,x₂) := e in e') ⇓ w
```

Alternatively, as done in the Coq homework, we can **define** the pairing-`let`
in terms of the projections. The construct `let (x₁,x₂) := e in e'` is syntactic
sugar for (in Coq `Notation`):

```
(λ y p, y (fst p) (snd p)) e (λ x₁ x₂, e')
```

This definition is a bit tricky. We need the lambdas to ensure that `e` and `e'`
are evaluated exactly once, are evaluated in the correct order, and do not
capture the wrong variables (here `y` and `p`). On the plus side, this
definition avoids us having to change the language definition so we can simply
reuse the one from week 10.

## References

Now let us extend our language with references. As usual, we will add a type
`Ref A` for locations with value of type `A`. The typing rules are:

```
     Γ ⊢ e : A                 Γ ⊢ e : Ref A
-------------------            --------------
Γ ⊢ alloc e : Ref A            Γ ⊢ free e : A

      Γ ⊢ e : Ref A            Γ₁ ⊢ e₁ : Ref Moved    Γ₂ ⊢ e₂ : A
--------------------------     ----------------------------------
Γ ⊢ load e : Ref Moved ⨯ A        Γ₁; Γ₂ ⊢ store e₁ e₂ : Ref A
```

The typing rule for `alloc` is as expected: it takes a value of type `A` and
turns it into a location with type `Ref A`. In the typing rule for `free`, it is
crucial that the value `A` is returned (and not `Unit`), otherwise ownership of
the value would get lost.

The key difference w.r.t. the ordinary lambda calculus with references comes
from the fact that we have a substructural type system: the result of `alloc`
needs to be used **exactly once**. This rules out buggy programs such as:

- Double free: `λ x, let l := alloc x in free l; free l`
- Memory leak: `λ x, let l := alloc x in ()`
- Nested memory leak: `λ x, let l := alloc (alloc x) in free l` (since `free`
  does not return `Unit`)

The rules for `load` and `store` are a bit more complicated. Let first take a
look at the types one would use in a language like ML or C:

```
load_wrong  : Ref A ⊸ A
store_wrong : Ref A ⊸ A ⊸ Unit
```

There are two problems here. First, one would loose access to the `Ref` since
the load and store consume them, but do not return them. In the actual typing
rules, this is fixed by letting `load` and `store` return the `Ref`.

Second, one needs to make sure that when the value of a reference is read, it is
**moved out** so that its ownership cannot be retrieved twice. Dually, when
storing, one needs to make sure that the ownership of the previous value does
not get lost. The typing rule for `load` thus takes a `Ref A`, but returns a
`Ref Moved`, signifying that the value has moved. Dually, the typing rule for
`store` takes a `Ref Moved` and returns a `Ref A`, ensuring that ownership of
the previous value has already been taken out.

If we were to have `Ref A` instead of `Ref Moved` in the rule for `load`, one
can type check the following buggy counterexample:

```
let l := alloc (alloc 10) in
let (l,k) := load l in
let (l,k') := load l in
free k;
free k' (* double free *)
```

Here, `k` and `k'` will both point to the inner reference. This causes a double
free. It is thus essential to move out ownership on a `load`.

**Practicality** Programming with our `load` and `store` becomes somewhat
clumsy because one has to thread through references. Practical programming
languages with a substructural type system (such as Rust) avoid this clumsiness
by introducing additional concepts to the language. In particular, using
borrowing and lifetimes, a C/ML-like load/store operation can be typed in Rust.

**Semantics** In Coq, we define `load` in terms of the ML-like load `!`/C-like
dereference `*` construct that returns the value of a location:

```
(λ l, (l, !l)) e
```

Similar to the pairing-`let`, this definition is carefully designed so that `e`
is evaluated exactly once.

## Copyable types

Types such as Booleans, integers, units contain pure functional data, and
therefore do not describe ownership of locations. In the version of the linear
lambda-calculus we developed so far, every type is treated substructurally, and
variables of every type are subject to the "use exactly once" discipline. This
is overly restrictive because it rules out _good_ programs like:

- Variable used twice `(λ x, x + x) : Nat ⊸ Nat ⊸ Nat`
- Variable not used `(λ x, 10) : Nat ⊸ Nat`

To make the type system less restrictive, while retaining its strong safety
guarantees, we only want to make it substructural for types that describe
ownership. We do this through a predicate `copy` that carves out the "copyable"
types/purely functional types. The rules for this predicate are:

```
                                          copy A₁     copy A₂
---------     ---------      --------     -------------------
copy Unit     copy Bool      copy Nat       copy (A₁ ⨯ A₂)
```

We see that the unit, Boolean, and integer type are copyable, and that copyable
types are closed under products.

For copyable types, we explicitly add the "weakening" and "contraction" rules
to the system (they are no longer lemmas):


```
copy A     x:A; x:A; Γ ⊢ e : B       copy A     Γ ⊢ e : B
------------------------------       --------------------
       x:A; Γ ⊢ e : B                    x:A; Γ ⊢ e : B
```

These rules allow one to duplicable and remove a variable with copyable type,
respectively.

**Context representation** For the above rules to make sense, we need to
represent variable contexts `Γ` as multisets so that variables can occur
multiple times. In binary rules, such as application, we use the multiset
sum/disjoint union to split the context.

With copyable types at hand, we can provide relaxed versions of the typing
rules for load and store that do not move the value:

```
copy A    Γ ⊢ e : Ref A     copy A    Γ₁ ⊢ e₁ : Ref A    Γ₂ ⊢ e₂ : A
-----------------------     ----------------------------------------
Γ ⊢ load e : Ref A ⨯ A             Γ₁; Γ₂ ⊢ store e₁ e₂ : Ref A
```

## Function types

**Question** Are functions copyable, i.e., is the following rule sound?

```
copy A₁     copy A₂
-------------------
   copy (A₁ ⊸ A₂)
```

**No**, it is not. Consider the following counterexample:

```
l : Ref nat ⊢ (λ x, free l) : Unit ⊸ Nat
```

If functions `Unit ⊸ Nat` were copyable, we can create a double free:

```
let l := alloc 10 in       (** l : Ref Nat *)
let g := (λ x, free l) in  (** g : Unit ⊸ Nat *)
g () + g ()
```

The problem is that a function can "capture" variables using its context, and
then use these variables each time it is called. This breaks the principle that
a variable is used exactly once.

To obtain the best of both worlds, languages with substructural types typically
provide two versions of the function type:

- **Linear/substructural functions** `A₁ ⊸ A₂` that can capture variables from
  the context, but should be called exactly once (called `FnOnce` in Rust). We
  often pronounce `⊸` as "lolly".
- **Unrestricted functions** `A₁ → A₂` that can be used zero-or-many times but
  cannot capture variables from the context (called `Fn` in Rust).

(Real programming languages with substructural types might have even more
function types. For example, Rust also has `FnMut`.)

We have the following rules for `⊸` and `→`:

```
  x:A; Γ ⊢ e : B           copy Γ   x:A; Γ ⊢ e : B
------------------         -----------------------       ------------
Γ ⊢ λ x, e : A ⊸ B            Γ ⊢ λ x, e : A → B         copy (A → B)
```

To introduce `⊸` we can capture the whole context `Γ`. To introduce `→` we
can only capture the context if all variables in `Γ` have a copyable type.

## Subtyping

The last feature that we consider is **subtyping**. In our language, some
values can be given multiple types. In particular, a function `g` that does
not capture variables from its context, can both be given type `A₁ ⊸ A₂` and
type `A₁ → A₂`. When defining such a function, we want to give it the strongest
type possible (here, `g : A₁ → A₂`) but still be able to use it as an argument
to a higher-order function `f : (A₁ ⊸ A₂) → B` that expects a `⊸` function.

To allow to turn a value with a stronger type into a value with a weaker type
we define a **subtyping relation** `A₁ <: A₂`, which says that every value of
type `A₁` can also be used at type `A₂`. The base rules for this relation are:

```
                      copy A
--------------      ----------
A → B <: A ⊸ B      A <: Moved
```

Using the **subsumption** rule subtyping is integrated into the type system:

```
Γ ⊢ e : A₁   A₁ <: A₂
---------------------
    Γ ⊢ e : A₂
```

Subtyping is a pre-order (i.e., reflexive and transitive) and is closed under
the usual type formers:

```
         A <: B   B <: C     A₁ <: A₂    B₁ <: B₂       A₁ <: A₂
         ---------------     --------------------    ----------------
A <: A        A <: C          A₁ ⨯ B₁ <: A₂ ⨯ B₂     Ref A₁ <: Ref A₂
```

**Question** What are the subtyping rules for functions?

These are as follows:

```
A₂ <: A₁    B₁ <: B₂    A₂ <: A₁    B₁ <: B₂
--------------------    --------------------
 A₁ → B₁ <: A₂ → B₂      A₁ ⊸ B₁ <: A₂ ⊸ B₂
```

Importantly, the direction of `A₂ <: A₁` for the argument is reversed
(i.e., **contravariant**) while the direction of `B₁ <: B₂` for the result is
not (i.e., **covariant**). Convince yourself why this is necessary.

**Note** Subtyping is by no means specific to languages with substructural
types. It is most prominent in languages with object-oriented features. You can
find more information about subtyping in Chapter 15 of Pierce et al. [2].

## Type soundness via semantic typing in separation logic

Now that we have presented the definition of our substructural type system, it
is time to prove its key property:

**Theorem: Strong type safety** If `copy A` and `∅ ⊢ e : A` then
1. `e` cannot have use-after-free and double-free
2. `e` does not have memory leaks

**Note** Memory leak freedom does not hold for expressions that return a
reference or a linear function. The program `(alloc 10) : Ref Nat` clearly leaks
memory, and so does `(let l := alloc 10 in λ x, free l) : Unit ⊸ Nat`. We thus
restrict the type safety theorem to copyable/purely functional types.

To prove the type safety theorem, we will use the technique of semantic typing
that we have already seen in week 6. But we do this with a twist. Instead of
modeling types as predicates over values (i.e., `val → Prop` in Coq), we define
them as **separation logic predicates**:

```
ty := val → sepProp
```

Recall that `sepProp := heap → Prop`, so using separation logic we hide the
fact that types can talk about ownership. In all of our definitions we will use
the separation logic connectives, and in our proofs in Coq we will use the Iris
Proof Mode. This means that we will never have to reason explicitly about
disjointness of heaps.

We define our type system as a shallow embedding to avoid the need to first
define a syntax for types. The connectives of the type system will simply be
modeled as combinators on `ty`:

```
Unit    := λ v, v = ()
Bool    := λ v, ∃ (b : bool), v = b
Nat     := λ v, ∃ (n : nat), v = n
Moved   := λ v, emp
Ref A   := λ v, ∃ (l : loc) (w : val), (v = l) ∗ (l ↦ w) ∗ A w
A₁ ⨯ A₂ := λ v, ∃ (w₁ w₂ : val), (v = (w₁,w₂)) ∗ A₁ w₁ ∗ A₂ w₂
A₁ ⊸ A₂ := λ v, ∀ (w : val), A₁ w -∗ WP (v w) A₂
A₁ → A₂ := λ v, ∀ (w : val), @[ A₁ w ⊢ WP (v w) A₂ ]
```

The definitions of `Unit`, `Bool`, and `Nat` generalize the usual semantic
definitions from ordinary predicates to predicates in separation logic. They say
that `()` is the only value of type `Unit`, and that Boolean and natural number
literals are the only values of type `Bool` and `Nat`, respectively. It is
crucial that the separation logic version of `=` says that the heap is empty.
This makes sure that `Bool` and `Nat` do not capture ownership of any location.

The type `Moved` says that the value can be arbitrary, but it does not hold any
ownership. The definition of `Ref A` says that references are locations that
point to values of type `A`. We use the `↦` from separation logic to ensure that
the location is indeed in the heap, and we have unique ownership of it.

The definition of `A₁ ⨯ A₂` also generalizes the usual semantic definition.
However, instead of a conjunction `∧`, we use the `∗` to signify that the
ownership of both components should be separate. Similarly, `A₁ ⊸ A₂`
generalizes the usual semantic definition, but instead of an implication, we
use a magic wand `-∗`. It says that if we an input value `w` and ownership
of `A₁ w`, then `v w` will safety terminate and result in a value of type `A₂`.

The definition of unrestricted functions `A₁ → A₂` is more involved. We need to
ensure that unrestricted functions do not capture ownership and can thus be
copied and discarded. To formalize that no resources can be captured using
separation logic, we use the pattern `@[ ... ⊢ ... ]`. Since the pure embedding
`@[ p ]` (for `p : Prop`) requires the heap to be empty, `@[ p ]` is trivially
duplicable (i.e., `@[ p ] ⊢ @[ p ] ∗ @[ p ]`) and discardable (i.e.,
`@[ p ] ⊢ emp`).

We now define the typing judgment, subtyping judgment, and copy predicate
semantically.

```
(⊢ e : A) := emp ⊢ WP e A`
A₁ <: A₂  := ∀ v, A₁ v ⊢ A₂ v
copy A    := ∀ v, A v ⊢ @[ emp ⊢ A v ]
```

The typing judgment `⊢` says that the expression `e` safely terminates and results
in a value of type `A` (the full version with contexts `Γ` can be found in the
Coq homework). The subtyping relation `<:` formalizes its intuitive semantics:
every value of type `A₁` also has type `A₂`. The `copy` predicate says that if
`A` holds for a certain value `v`, then it also holds in the empty context.
Because `@[ p ]` is duplicable and discardable, this gives us contraction and
weakening for pure types.

In the Coq homework you will:

- Define the typing judgment for arbitrary contexts `Γ`.
- Prove the typing rules of the typing system. Since we use a shallow embedding,
  the typing rules will be formalized as lemmas in Coq.
- Prove the strong type safety theorem.
- Define sum types and prove the typing rules.
- Study a notion of context subtyping that lifts subtyping to contexts.
- Define operator typing and prove the typing rules for operators.

## References and further reading

In this lecture we have seen how to prove a strong version of type safety for
a small language with substructural types. Walker's book chapter [1] provides
more background on substructural type systems, and Chapter 15 of the Pierce's
book [2] provides more background on subtyping. The RustBelt paper [3] and PhD
thesis of Jung [4] show how to generalize this approach to a large fragment of
the Rust language (including borrowing, lifetimes, and unsafe libraries).

[1] David Walker. Substructural Type Systems. Book chapter in "Advanced Topics
    in Types and Programming Languages". Editor Benjamin Pierce. 2002.\
[2] Benjamin Pierce. Types and Programming Languages. Book. 2002.\
[3] Ralf Jung, Jacques-Henri Jourdan, Robbert Krebbers and Derek Dreyer.
    RustBelt: Securing the Foundations of the Rust Programming Language. In
    ACM SIGPLAN Symposium on Principles of Programming Languages (POPL). 2018.
    https://robbertkrebbers.nl/research/articles/rustbelt.pdf \
[4] Ralf Jung. Understanding and Evolving the Rust Programming Language. PhD
    thesis. 2020. https://www.ralfj.de/research/thesis.html
