# Rust exercises: week 7 and week 8

This directory contains the Rust exercises for week 7 and week 8.
See below for external resources, and a description of how to run the
files and set up your editor.

## External resources

To familiarize yourself with Rust, read the "Rust book":
https://doc.rust-lang.org/book/

Read until and including Section 11. A lot of the material you can skim, or read
on a by-need basis, but focus on:

- Chapter 4 on ownership
- Chapter 10.3 on lifetimes

You should be able to explain concepts about memory layout, ownership,
borrowing, and lifetimes on the exam, so make sure to read Chapter 4 and 10.3
carefully and make the exercises so that you get sufficient practice.

While doing the exercises, the Rust standard library documentation may also be
useful: https://doc.rust-lang.org/std/

## Running and compiling Rust

There are several options for running Rust:

- Download the Rust compiler (from https://www.rust-lang.org/tools/install,
  or use your package manager).
  Then install a Rust extension for your editor. We recommend VS code with
  the Rust analyzer extension (https://rust-analyzer.github.io/).
  (You may also want to enable "Adjustment Hints" in the settings,
  so that VS code displays automatically inserted `&` and `*`).
- The Rust playground https://play.rust-lang.org/ provides an online IDE and
  compiler for Rust. This is particularly useful if you do not want to install a
  compiler/IDE on your computer. **Be careful**, the Rust playground sometimes
  leads to timeouts, so we do not recommend this option.

## How to do these exercises

To complete the exercises, you should replace all uses of `unimplemented!()`
by working code. The bottom of this file contains a number of tests. These tests
are by no means exhaustive, but if the Rust compiler accepts your file, and the
tests pass, there is a reasonable chance your solutions are correct.

If you use the VS code extension, the editor should display "run" buttons for
the test cases (and for the `main` function if you create one).

You can also do `cargo build` and `cargo test` to build the code and run the
tests, respectively (see Chapter 1.3 and 11 of the Rust book).

If you use the `rustc` compiler directly,  you should compile with the `--test`
flag to create a test runner executable. In order to execute the test runner,
you need to run the executable, so:

```
rustc --test weekX.rs && ./weekX
```

If you use the Rust playground, it will compile and execute your tests directly.

If you want to perform some output tests, you can of course also add a main
function, i.e., `fn main() { do stuff }`. To run main, you need to compile
without `--test`.
