#![allow(dead_code)]
use std::mem;

/** In this week's lecture, we have looked at the basics of programming in Rust.
Most importantly, we looked at the concepts of ownership, borrowing and
lifetimes, and how they avoid memory-safety bugs in languages like C++. In this
week's homework, you will get some hands-on experience with Rust. */

/// Part 1: Basic Rust

/** Implement a function that computes the sum of a vector of integers. */

pub fn vec_sum(xs : &Vec<i32>) -> i32 {
  unimplemented!()
}

/** Implement a function that given two references to vectors returns a
reference to the vector with the biggest sum. */

pub fn biggest_sum<'a>
    (xs : &'a mut Vec<i32>, ys : &'a mut Vec<i32>) -> &'a mut Vec<i32> {
  unimplemented!()
}

/** Implement a function that returns a reference to the minimal element in a
vector. If an element occurs multiple times, you should return a reference to
the first. Try to use an iterator `for x in xs` instead of going over the
array indexes `i` and use `xs[i]`. */

pub fn vec_min(xs : &Vec<i32>) -> &i32 {
  assert!(xs.len() != 0, "`vec_min` requires vector to be non-empty");

  unimplemented!()
}

/// Part 2: Option

/** The "Option"/"Maybe" type that we know and love from functional programming
is defined as follows in Rust (`enum` is similar to `Inductive` in Coq): */

#[derive(Clone,Copy,Debug,PartialEq,Eq)]
pub enum MyOption<T> {
  None,
  Some(T)
}
use MyOption::{None,Some};

/** The syntax `<T>` makes sure that the option type is generic in its elements.
The `derive` pragma generates a number of trait implementations so that we can
clone, copy, print, and compare options (provided the element type `T` allows
that too). The `use` command allows us to write just `None`/`Some` instead of
`MyOption::None` and `MyOption::Some`.

Note that `Option` is in the Rust standard library, so we use the name
`MyOption` here. */

/** Using `impl` blocks we can collect functions. To refer to a function `f` in
the `impl` block, you can use `MyOption::f(x)`, or simply `x.f()` if the
argument is `self`. */

impl<T> MyOption<T> {
  /** The function `as_ref` converts an `&MyOption<T>` into `MyOption<&T>`.
  Explain to yourself how both types are laid out in memory (draw this on a
  piece of paper) and why this conversion makes sense. */

  fn as_ref(&self) -> MyOption<&T> {
    match self {
      None => None,
      Some(ref r) => Some(r)
    }
  }

  /** Similar but for mutable references. */

  fn as_mut(&mut self) -> MyOption<&mut T> {
    match self {
      None => None,
      Some(ref mut r) => Some(r)
    }
  }

  /* It is important to note that Rust has different pattern matching
  constructs:

  - If you use `ref` (or `ref mut`) in the branches, you indicate that
    you are matching on a (mutable) reference. Here you will get a pointer to
    the payload of the variant of the `enum`.
  - If you do not use `ref` (or `ref mut`), you indicate that you are matching
    on a value. Here you get the value of the variant of the `enum`.

  Rust can often infer the `ref`/`ref mut` annotations from the type of `e` in
  `match e ..`, allowing you to write: */

  fn as_mut_v2(&mut self) -> MyOption<&mut T> {
    match self {
      None => None,
      Some(r) => Some(r)
    }
  }

  /** That said, even if Rust can infer the `ref`/`ref mut`, it is often good to
  be explicit so as to get a better understanding of the code you write. */

  /** The function `take` takes a `&mut MyOption<T>` and turns it into
  `MyOption<T>` by setting the input to `None`. In other words, it moves out the
  ownership of the element if it exists. The implementation makes use of the
  function `mem::replace` that reads and replaces the value of a reference. */

  fn take(&mut self) -> MyOption<T> {
    mem::replace(self,None)
  }
}

/** Implement the following functions. Take a look at the tests if you are
unsure what they should be doing. */

impl<T> MyOption<T> {
  fn is_some(&self) -> bool {
    unimplemented!()
  }

  fn unwrap_or(self, default : T) -> T {
    unimplemented!()
  }
}

impl<T> MyOption<MyOption<T>> {
  fn flatten(self) -> MyOption<T> {
    unimplemented!()
  }
}

/** The previous function `vec_min` panics due to the failure of `assert` on
empty vectors. Implement a better version that uses `MyOption` to deal with
empty vectors. */

pub fn vec_min_option(xs : &Vec<i32>) -> MyOption<&i32> {
  unimplemented!()
}

/// Part 3: Priority queue as sorted linked association list

/** In the last part of this week's exercise, you will implement a priority
queue. A priority queue stores elements (of type `T`) with an associated
priority (of type `i32`). Using the priority queue methods, you can create an
empty priority queue (`new`), obtain/remove the value with minimal associated
priority (`min`, `min_prio`, `remove_min`) and insert values with associated
priority (`insert`). Our priority queue is represented as a linked list, in
which elements are stored in ascending order of priority. We allow for elements
with the same priority. In that case, the element that is added first is
considered minimal.

To define a priority queue as a linked list in Rust, we introduce a number of
types. The type `PQueue<T>` contains a pointer to the first node of the priority
queue. The type `Node<T>` represents nodes of the priority queue, they contain
`prio`rities and `val`ues, as well as a pointer to the `next` node. For pointers
to nodes, we use the type alias `Link<T>`, which uses:

- The type `Box` to represent pointers on the heap.
- The type `MyOption` to deal with `NULL` pointers in a safe manner.

Interesting fact: The Rust compiler optimizes `MyOption<Box<U>>` to regular
efficient pointers with NULL values, but shields us from the unsafety of NULL
pointers when programming. We have to explicitly pattern match to detect if
a pointer is NULL (i.e., `None`) or not (i.e., `Some`).

You can use `Box` values in a similar way as references. You can dereference
them using `*`, but this is often not needed due to Rust's auto dereferencing.
New `Box`es can be created using `Box::new` (which is similar to C's `malloc`).
You can find more information about `Box` at:

  https://doc.rust-lang.org/std/boxed/index.html

*/

#[derive(Clone,Debug)]
struct Node<T> {
  prio : i32,
  val : T,
  next : Link<T>,
}

type Link<T> = MyOption<Box<Node<T>>>;

#[derive(Clone,Debug)]
pub struct PQueue<T> {
  head: Link<T>,
}

impl<T> PQueue<T> {
  fn new() -> Self {
    PQueue { head: None }
  }

  /** Implement a function that gives a reference to the element with the
  minimal priority. Since the list is sorted, this should be the first element
  in the list. If the priority queue is empty, it should return `None`. */

  fn min(&self) -> MyOption<&T> {
    unimplemented!()
  }

  /** Implement a function that gives the minimal priority of the elements in
  the priority queue. Since the list is sorted, this should be the first element
  in the list. If the priority queue is empty, the function should return
  `None`. */

  fn min_prio(&self) -> MyOption<i32> {
    unimplemented!()
  }

  /** Implement a function that removes the element with the minimal priority.
  Since the list is sorted, this should be the first element in the list. If the
  priority queue is empty, the function should return `None`. One of the
  functions on options that we defined might come in handy to make the borrow
  checker happy. */

  fn remove_min(&mut self) -> MyOption<(i32,T)> {
    unimplemented!()
  }

  fn find_link(link : &mut Link<T>, prio : i32) -> &mut Link<T> {
    match link {
      None => link,
      Some(node) if prio < node.prio => link,
      Some(node) => Self::find_link(&mut node.next, prio)
    }
  }

  /** Implement a function that inserts a value into the priority queue. This
  function is tricky, since you need to find the right position in the list to
  ensure it remains sorted. To implement this function, make use of `find_link`
  defined above. We have given the code for `find_link` since its implementation
  is a bit tricky: we need the `if` guard in the `match` to convince Rust's
  borrow checker. Also some of the functions on options that we defined might
  come in handy. */

  fn insert(&mut self, prio : i32, val : T) {
    unimplemented!()
  }

  /** Implement functions that convert the priority queue to a vector of
  tuples. The tuples should appear in ascending and descending order of
  priority, respectively. Note these functions take ownership of the priority
  queue, which means that you can remove all elements of the priority queue. For
  both of these functions, think carefully if you want to use recursion or a
  loop. */

  fn to_vec(mut self) -> Vec<(i32,T)> {
    unimplemented!()
  }

  fn to_vec_reverse(mut self) -> Vec<(i32,T)> {
    unimplemented!()
  }
}

/// Tests

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn vec_sum_test() {
    assert_eq!(vec_sum(&vec![]), 0);
    assert_eq!(vec_sum(&vec![10]), 10);
    assert_eq!(vec_sum(&vec![20,40]), 60);
    for i in 2..30 {
      assert_eq!(vec_sum(&(1..i).collect()), (i * (i-1)) / 2, "for i = {}", i);
    }
  }

  #[test]
  fn vec_biggest_sum_test() {
    let mut xs = vec![10,12];
    let mut ys = vec![40];
    let zs = biggest_sum(&mut xs,&mut ys);
    assert_eq!(vec![40], *zs);

    zs.push(10);
    xs.push(60);
    let ws = biggest_sum(&mut xs,&mut ys);
    assert_eq!(vec![10,12,60], *ws);
  }

  #[test]
  fn vec_min_test() {
    assert_eq!(*vec_min(&vec![20]), 20);
    assert_eq!(*vec_min(&vec![20,10]), 10);
    assert_eq!(*vec_min(&vec![1, 20,10]), 1);

    let xs = vec![20, 20];
    assert_eq!(vec_min(&xs) as *const i32, &xs[0] as *const i32);
  }

  #[test]
  fn is_some_test() {
    assert_eq!(MyOption::is_some(&None::<i32>), false);
    assert_eq!(MyOption::is_some(&Some(10)), true);
  }

  #[test]
  fn unwrap_or_test() {
    assert_eq!(MyOption::unwrap_or(None, 10), 10);
    assert_eq!(MyOption::unwrap_or(Some(10), 12), 10);
  }

  #[test]
  fn flatten_test() {
    assert_eq!(MyOption::flatten(None::<MyOption<i32>>), None::<i32>);
    assert_eq!(MyOption::flatten(Some(None::<i32>)), None::<i32>);
    assert_eq!(MyOption::flatten(Some(Some(14))), Some(14));
  }

  #[test]
  fn vec_min_option_test() {
    assert_eq!(vec_min_option(&vec![]), None);
    assert_eq!(vec_min_option(&vec![20]), Some(&20));
    assert_eq!(vec_min_option(&vec![20,10]), Some(&10));
    assert_eq!(vec_min_option(&vec![1,20,10]), Some(&1));
  }

  #[test]
  fn priority_queue_empty_test() {
    let mut pq = PQueue::<String>::new();

    assert_eq!(pq.min_prio(), None);
    assert_eq!(pq.min(), None);
    assert_eq!(pq.remove_min(), None);
    assert_eq!(pq.clone().to_vec(), vec![]);
    assert_eq!(pq.clone().to_vec_reverse(), vec![]);
  }

  #[test]
  fn priority_queue_one_elem_test() {
    let mut pq = PQueue {
      head : Some(Box::new(Node{
        prio : 10,
        val : String::from("ten"),
        next : None
      }))
    };
    assert_eq!(pq.min_prio(), Some(10));
    assert_eq!(pq.min(), Some(&String::from("ten")));
    assert_eq!(pq.clone().to_vec(), vec![(10,String::from("ten"))]);
    assert_eq!(pq.clone().to_vec_reverse(), vec![(10,String::from("ten"))]);

    match pq.remove_min() {
      None => panic!("priority queue should not be empty"),
      Some(px) => {
        assert_eq!(px, (10, String::from("ten")));
        assert_eq!(pq.min(), None)
      }
    }
  }

  #[test]
  fn priority_queue_two_elem_test() {
    let mut pq = PQueue {
      head : Some(Box::new(Node{
        prio : 10,
        val : String::from("ten"),
        next : Some(Box::new(Node{
          prio : 20,
          val : String::from("twenty"),
          next : None
        }))
      }))
    };
    assert_eq!(pq.min_prio(), Some(10));
    assert_eq!(pq.min(), Some(&String::from("ten")));
    assert_eq!(pq.clone().to_vec(),
      vec![(10,String::from("ten")), (20,String::from("twenty"))]);
    assert_eq!(pq.clone().to_vec_reverse(),
      vec![(20,String::from("twenty")), (10,String::from("ten"))]);

    match pq.remove_min() {
      None => panic!("priority queue should not be empty"),
      Some(px) => {
        assert_eq!(px, (10, String::from("ten")));
        assert_eq!(pq.min(), Some(&String::from("twenty")));
      }
    }
  }

  #[test]
  fn priority_queue_two_same_elem_test() {
    let mut pq = PQueue {
      head : Some(Box::new(Node{
        prio : 10,
        val : String::from("ten first"),
        next : Some(Box::new(Node{
          prio : 10,
          val : String::from("ten again"),
          next : None
        }))
      }))
    };
    assert_eq!(pq.min(), Some(&String::from("ten first")));

    match pq.remove_min() {
      None => panic!("priority queue should not be empty"),
      Some(px) => {
        assert_eq!(px, (10, String::from("ten first")));
        assert_eq!(pq.min(), Some(&String::from("ten again")));
      }
    }
  }

  #[test]
  fn priority_queue_insert_ascending_test() {
    let mut pq = PQueue::new();
    pq.insert(1, String::from("one"));
    pq.insert(2, String::from("two"));
    pq.insert(3, String::from("three"));
    pq.insert(5, String::from("five"));

    assert_eq!(pq.clone().to_vec(),
      vec![(1,String::from("one")), (2,String::from("two")),
           (3,String::from("three")), (5,String::from("five"))]);
    assert_eq!(pq.clone().to_vec_reverse(),
      vec![(5,String::from("five")), (3,String::from("three")),
           (2,String::from("two")), (1,String::from("one"))]);
  }

  #[test]
  fn priority_queue_insert_descending_test() {
    let mut pq = PQueue::new();
    pq.insert(5, String::from("five"));
    pq.insert(3, String::from("three"));
    pq.insert(2, String::from("two"));
    pq.insert(1, String::from("one"));

    assert_eq!(pq.clone().to_vec(),
      vec![(1,String::from("one")), (2,String::from("two")),
           (3,String::from("three")), (5,String::from("five"))]);
    assert_eq!(pq.clone().to_vec_reverse(),
      vec![(5,String::from("five")), (3,String::from("three")),
           (2,String::from("two")), (1,String::from("one"))]);
  }

  #[test]
  fn priority_queue_insert_random_test() {
    let mut pq = PQueue::new();
    pq.insert(5, String::from("five"));
    pq.insert(2, String::from("two"));
    pq.insert(3, String::from("three"));
    pq.insert(1, String::from("one"));

    assert_eq!(pq.clone().to_vec(),
      vec![(1,String::from("one")), (2,String::from("two")),
           (3,String::from("three")), (5,String::from("five"))]);
    assert_eq!(pq.clone().to_vec_reverse(),
      vec![(5,String::from("five")), (3,String::from("three")),
           (2,String::from("two")), (1,String::from("one"))]);
  }

  #[test]
  fn priority_queue_insert_same_test() {
    let mut pq = PQueue::new();
    pq.insert(5, String::from("five"));
    pq.insert(2, String::from("two first"));
    pq.insert(2, String::from("two again"));
    pq.insert(1, String::from("one"));

    assert_eq!(pq.clone().to_vec(),
      vec![(1,String::from("one")), (2,String::from("two first")),
           (2,String::from("two again")), (5,String::from("five"))]);
    assert_eq!(pq.clone().to_vec_reverse(),
      vec![(5,String::from("five")), (2,String::from("two again")),
           (2,String::from("two first")), (1,String::from("one"))]);
  }
}
