(** * Lecture 5: Type systems *)
From pv Require Export library.

(* REMINDER:

          #####################################################
          ###  PLEASE DO NOT DISTRIBUTE SOLUTIONS PUBLICLY  ###
          #####################################################

*)

(* ########################################################################## *)
(** * Introduction *)
(* ########################################################################## *)

(** In this lecture we study the following properties of typed programming
languages:

1. Type safety: All well-typed programs are type safe (i.e., cannot "go wrong").
2. Termination: All well-typed programs terminate.

For part (1) about safety, we use the method of "Progress and Preservation".
This is the standard method for proving type safety, and works by proving two
key lemmas:

- Preservation: Well-typed programs step to well-typed programs.
- Progress: Well-typed programs are either values or they can step.

Together, these two lemmas imply type safety, which is formalized as:

- Type safety: if a well-typed program [e] takes multiple steps to [e]',
               then [e'] is a value or [e'] can step.

For part (2) about termination, we use the method of "Logical Relations" aka
"Semantic typing". This method consists of the following steps:

- We define a "wp"-based program logic for proving termination of untyped
  programs.
- We define a semantic interpretation of types as logical predicates, and define
  a semantic typing judgment.
- We use the lemmas of the "wp" logic to prove:
  + Fundamental theorem: well-typed programs are semantically typed (by
    induction on typing).
  + Adequacy: Semantically typed programs terminate. *)


(* ########################################################################## *)
(** * Call-by-value lambda calculus *)
(* ########################################################################## *)

(** Similar to previous week, we define a call-by-value lambda calculus with
the unit value, natural numbers and Booleans as primitives. The syntax is:

  Operators:    op ::= + | - | <= | < | ==
  Values:        v ::= () | b | n | λ x. e
  Expressions:   e ::= v | x | λ x. e | e1 e2 |
                       e1 `op` e2 | if e1 then e2 else e3

As before, [b] ranges over Booleans, [n] over natural numbers, and [x] over
variables, which are bound by the lambda construct. *)

(** The definitions below are mostly the same to those of previous week. An
important change is that we have a separate [EVal v] constructor for values
(Booleans, natural numbers, and closures). A lambda expression [ELam x e]
evaluates to the value [EVal (VClosure x e)] (see rule [Lam_head_step]). This
simplifies our proofs, because we use our typing rules (see [Closure_val_typed])
to ensure that expressions [e] inside closures [VClosure x e] must have no free
variables other than [x]. Thus, we can define our substitution function [subst]
to be the identity on values [EVal]. If we had not made this change, then we
would need to manually keep track of closedness. By baking this into the
definition of substitution, keeping track of closedness is not necessary. *)

Inductive bin_op :=
  | AddOp
  | SubOp
  | LeOp
  | LtOp
  | EqOp.

Inductive expr :=
  | EVal : val -> expr
  | EVar : string -> expr
  | ELam : string -> expr -> expr
  | EApp : expr -> expr -> expr
  | EOp : bin_op -> expr -> expr -> expr
  | EIf : expr -> expr -> expr -> expr
with val :=
  | VUnit : val
  | VBool : bool -> val
  | VNat : nat -> val
  | VClosure : string -> expr -> val.

Fixpoint subst (x : string) (w : val) (e : expr) : expr :=
  match e with
  | EVal _ => e
  | EVar y => if String.eq_dec y x then EVal w else EVar y
  | ELam y e => if String.eq_dec y x then ELam y e else ELam y (subst x w e)
  | EApp e1 e2 => EApp (subst x w e1) (subst x w e2)
  | EOp op e1 e2 => EOp op (subst x w e1) (subst x w e2)
  | EIf e1 e2 e3 => EIf (subst x w e1) (subst x w e2) (subst x w e3)
  end.

Definition eval_bin_op (op : bin_op) (v1 v2 : val) : option val :=
  match op, v1, v2 with
  | AddOp, VNat n1, VNat n2 => Some (VNat (n1 + n2))
  | SubOp, VNat n1, VNat n2 => Some (VNat (n1 - n2))
  | LeOp, VNat n1, VNat n2 => Some (VBool (Nat.leb n1 n2))
  | LtOp, VNat n1, VNat n2 => Some (VBool (Nat.ltb n1 n2))
  | EqOp, VUnit, VUnit => Some (VBool true)
  | EqOp, VNat n1, VNat n2 => Some (VBool (Nat.eqb n1 n2))
  | EqOp, VBool n1, VBool n2 => Some (VBool (Bool.eqb n1 n2))
  | _, _, _ => None
  end.

Inductive head_step : expr -> expr -> Prop :=
  | Lam_head_step x e :
     head_step (ELam x e) (EVal (VClosure x e))
  | App_head_step y e1 v2 :
     head_step (EApp (EVal (VClosure y e1)) (EVal v2)) (subst y v2 e1)
  | Op_head_step op v1 v2 v :
     eval_bin_op op v1 v2 = Some v ->
     head_step (EOp op (EVal v1) (EVal v2)) (EVal v)
  | If_head_step_true e2 e3 :
     head_step (EIf (EVal (VBool true)) e2 e3) e2
  | If_head_step_false e2 e3 :
     head_step (EIf (EVal (VBool false)) e2 e3) e3.

(** Remember that we evaluate arguments in right-to-left order, similar to
the OCaml language. *)
Inductive step : expr -> expr -> Prop :=
  | do_head_step e e' :
     head_step e e' -> step e e'
  | App_step_r e1 e2 e2' :
     step e2 e2' -> step (EApp e1 e2) (EApp e1 e2')
  | App_step_l e1 v2 e1' :
     step e1 e1' -> step (EApp e1 (EVal v2)) (EApp e1' (EVal v2))
  | Op_step_r op e1 e2 e2' :
     step e2 e2' -> step (EOp op e1 e2) (EOp op e1 e2')
  | Op_step_l op e1 v2 e1' :
     step e1 e1' -> step (EOp op e1 (EVal v2)) (EOp op e1' (EVal v2))
  | If_step e1 e1' e2 e3 :
     step e1 e1' -> step (EIf e1 e2 e3) (EIf e1' e2 e3).

Inductive steps : expr -> expr -> Prop :=
  | steps_refl e :
     steps e e
  | steps_step e1 e2 e3 :
     step e1 e2 ->
     steps e2 e3 ->
     steps e1 e3.


(* ########################################################################## *)
(** * Type system *)
(* ########################################################################## *)

(** We turn to defining a type system for our language. As a first step we
need to define an inductive type representing the syntax of types. Recall that
our values are either natural numbers, Booleans, or closures, so types [ty] have
three corresponding constructors:

- [TUnit] for the unit type.
- [TNat] for the type of natural numbers.
- [TBool] for the type of Booleans.
- [TFun A B] for the type of closures. *)

Inductive ty :=
  | TUnit : ty
  | TNat : ty
  | TBool : ty
  | TFun : ty -> ty -> ty.

(** Next we define the type signatures of binary operations. This is a relation
[bin_op_typed op A1 A2 B], where:

- [op] is the operation.
- [A1] and [A2] are the input types.
- [B] is the output type.

For example, the inference rule for [EqOp] is as follows:

  EqOp_typed : bin_op_typed EqOp TNat TNat TBool

This rule says that equality takes two natural numbers as input, and gives us a
Boolean as output. *)

Inductive bin_op_typed : bin_op -> ty -> ty -> ty -> Prop :=
  | AddOp_typed :
     bin_op_typed AddOp TNat TNat TNat
  | SubOp_typed :
     bin_op_typed SubOp TNat TNat TNat
  | LeOp_typed :
     bin_op_typed LeOp TNat TNat TBool
  | LtOp_typed :
     bin_op_typed LtOp TNat TNat TBool
  | EqOp_Unit_typed :
     bin_op_typed EqOp TUnit TUnit TBool
  | EqOp_Nat_typed :
     bin_op_typed EqOp TNat TNat TBool
  | EqOp_Bool_typed :
     bin_op_typed EqOp TBool TBool TBool.


(** We are ready to define the typing judgment [Γ ⊢ e : A], which expresses that
an expression [e] has type [A] in context [Γ]. In Coq, we represent this
judgment using the inductive relation [typed Gamma e A]. To represent contexts
[Gamma] we use the type [stringmap ty] of finite maps with strings as keys. This
type works similarly as [natmap], which we have seen in the lecture of week 3. *)

(** Because we have a mutually inductively defined type for expressions and
values, the typing judgment also becomes mutually inductive. We have the ordinary
typing judgment [typed Γ e A] which means [Γ ⊢ e : A], but we also have a value
typing judgment [val_typed v A], which says that the value [v] is closed and has
type [A]. We use [val_typed] in the [EVal] rule of [typed], and conversely we
use [typed] in the [VClosure] rule of [val_typed]. *)

Inductive typed : stringmap ty -> expr -> ty -> Prop :=
  | Val_typed Gamma v A :
     val_typed v A ->
     typed Gamma (EVal v) A
  | Var_typed Gamma x A :
     StringMap.lookup Gamma x = Some A ->
     typed Gamma (EVar x) A
  | Lam_typed Gamma x e A B :
     typed (StringMap.insert x A Gamma) e B ->
     typed Gamma (ELam x e) (TFun A B)
  | App_typed Gamma e1 e2 A B :
     typed Gamma e1 (TFun A B) ->
     typed Gamma e2 A ->
     typed Gamma (EApp e1 e2) B
  | Op_typed Gamma op e1 e2 A1 A2 B :
     bin_op_typed op A1 A2 B ->
     typed Gamma e1 A1 ->
     typed Gamma e2 A2 ->
     typed Gamma (EOp op e1 e2) B
  | If_typed Gamma e1 e2 e3 B :
     typed Gamma e1 TBool ->
     typed Gamma e2 B ->
     typed Gamma e3 B ->
     typed Gamma (EIf e1 e2 e3) B
with val_typed : val -> ty -> Prop :=
  | Unit_val_typed :
     val_typed VUnit TUnit
  | Bool_val_typed b :
     val_typed (VBool b) TBool
  | Nat_val_typed n :
     val_typed (VNat n) TNat
  | Closure_val_typed x e A B :
     typed (StringMap.singleton x A) e B ->
     val_typed (VClosure x e) (TFun A B).


(* ########################################################################## *)
(** * Type safety using progress & preservation *)
(* ########################################################################## *)

(** We prove *type safety*, which states that well-typed programs are safe:

  if  ⊢ e : A  then  safe e

This is formalized as the Coq theorem [type_safety]. Here, [safe e] intuitively
means that the program cannot get stuck. Formally, an expression [e] is safe if
all expressions [e'] that it can step to are either final or can themselves step
further. *)

Definition final (e : expr) := exists v, e = EVal v.
Definition can_step (e : expr) := exists e', step e e'.
Definition safe (e : expr) :=
  forall e',
    steps e e' ->
    final e' \/ can_step e'.

(* We prove type safety using the technique of progress and preservation.

- *Type preservation*
  Whenever a term is well-typed, it will remain well-typed for its entire
  execution:

    if  ⊢ e : A  and  e ⇒ e'  then  ⊢ e' : A

  This is formalized as the Coq theorem [preservation].

- *Progress*
  Well-typed terms are not stuck:

    if ⊢ e : A  then  e can take a step, or e is a value

  This is formalized as the Coq theorem [progress]

When we have proved both these results, the theorem [type_safety] will follow as
a consequence. *)


(* ========================================================================== *)
(** * Substitution lemma *)
(* ========================================================================== *)

(** We turn our attention to the preservation theorem. To prove this theorem,
we need an auxiliary result:

The *substitution lemma* says that when we substitute a well-typed value [v]
into a well-typed expression [e], the substitution [subst x v e] is well-typed:

  if v : A   and   (x:A); Γ ⊢ e : B  then  Γ ⊢ subst e x v : B

Note that we did not [Import StringMap], so we have to use the [StringMap.]
qualifier for all definitions and lemmas. *)

Lemma typed_subst Gamma x e v A B :
  val_typed v A ->
  typed (StringMap.insert x A Gamma) e B ->
  typed Gamma (subst x v e) B.
Proof.
  intros Hv He.
  (** Coq's induction tactic generalizes over the context [Gamma] when doing
  induction on [typed Gamma e A]. While this is generally fine, in this lemma
  it is not, we would lose that the context is [StringMap.insert x A Gamma].
  We use the remember tactic to introduce an explicit equality. (Try to remove
  the use of [remember] and see what goes wrong.) *)
  remember (StringMap.insert x A Gamma) as Gamma' eqn:HGamma'.
  revert Gamma HGamma'.
  induction He; intros ? ->; simpl; eauto using typed.
  (** Again most cases are solved automatically. (You can try to solve them by
  hand by removing the call to [eauto]. *)
  - (** Var case *)
    rewrite StringMap.lookup_insert in H.
    destruct (String.eq_dec _ _); simplify_eq.
    + eauto using StringMap.incl_empty_l, Val_typed.
    + by apply Var_typed.
  - (** Lam case *)
    destruct (String.eq_dec _ _); simplify_eq.
    + rewrite StringMap.insert_insert in He.
      destruct (String.eq_dec _ _); [|done].
      by apply Lam_typed.
    + apply Lam_typed. apply IHHe. StringMap.map_solver.
Qed.


(* ========================================================================== *)
(** * Exercise: Preservation *)
(* ========================================================================== *)

(** Hint: You can use tacticals ([;] and friends) and [eauto] to automatically
discharge many cases in your proofs. *)

(** The lemma [bin_op_preservation] says that when the arguments of an operator
are of the right type (as specified by [bin_op_typed]), then the output is of
the right type (as specified by [bin_op_typed]). *)

Lemma bin_op_preservation op v1 v2 v A1 A2 B :
  bin_op_typed op A1 A2 B ->
  eval_bin_op op v1 v2 = Some v ->
  val_typed v1 A1 ->
  val_typed v2 A2 ->
  val_typed v B.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** We prove the preservation theorem in two stages: first for head reduction
(lemma [head_step_preservation]) and then for the whole-program reduction
(lemma [preservation]). *)

(** HINTS: You will need to use well-typedness of substitutions (lemma
[typed_subst] in one of the cases of the induction. *)

Lemma head_step_preservation e1 e2 A :
  head_step e1 e2 ->
  typed StringMap.empty e1 A ->
  typed StringMap.empty e2 A.
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.

Lemma preservation e1 e2 A :
  step e1 e2 ->
  typed StringMap.empty e1 A ->
  typed StringMap.empty e2 A.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.


(* ========================================================================== *)
(** * Exercise: Progress *)
(* ========================================================================== *)

(** Prove that [eval_bin_op], when given arguments of the right type, it cannot
go wrong (i.e., cannot return [None]), and its result is of the right type. *)

Lemma bin_op_progress op v1 v2 A1 A2 B :
  bin_op_typed op A1 A2 B ->
  val_typed v1 A1 ->
  val_typed v2 A2 ->
  exists w,
    eval_bin_op op v1 v2 = Some w /\
    typed StringMap.empty (EVal w) B.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Prove the progress theorem. Recall that this theorem makes use of two
predicates on expressions (defined above):

- [final e], which states that e is a value.
- [can_step e], which states that e can take a step.

The proof proceeds by:

- Induction on the expression [e], followed by,
- Inversion the typing judgment.

Equivalently, you could perform induction on the typing judgment, but then you
need to use the [remember] tactic.

Do not forget to generalize the induction hypothesis in the appropriate manner,
and do not forget to make use of the lemmas you have already proved. *)

Lemma progress e A :
  typed StringMap.empty e A ->
  final e \/ can_step e.
Proof. (* FILL IN HERE (24 LOC proof) *) Admitted.

(** Prove the final type safety theorem as a corrolary of [progress] and
[preservation]. (This proof should not involve induction on the expression or
typing derivation!) *)

Lemma type_safety e A :
  typed StringMap.empty e A ->
  safe e.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Semantic typing  *)
(* ########################################################################## *)

(** Progress and preservation can be used to establish type safety. There is
a different approach to proving properties of type systems that is more powerful
than progress and preservation. This method is called "logical relations" or
"semantic typing". It can be used to establish type safety, but also strong and
weak normalization, parametricity, security properties, and more.

We will use the method to establish termination of well-typed programs: we want
to prove [∅ ⊢ e : A] implies [terminates e]. To prove this property using
induction on typing, we formulate a strengthened induction hypothesis
[Γ ⊨ e : A], and prove these lemmas:

- *Fundamental theorem*: [Γ ⊢ e : A] implies [Γ ⊨ e : A].
- *Adequacy theorem*: [∅ ⊨ e : A] implies [terminates e].

We define [Γ ⊨ e : A] in a factorized manner by defining a program logic for
reasoning about untyped. With this logic, we can manually prove that a program
has a certain type semantically, even when the syntactic typing rules would
reject the program.

The logic consists of a predicate [wp e P] which says that [e] terminates, and
its return value [v] satisfies the predicate [P v]. The [wp] is useful because
it has many logical rules that we can prove as lemmas.

We then define a semantic interpretation of types [sem A : val -> Prop] as
predicates over values. Then we can state that a closed program [e] semantically
satisfies the type [A] as [wp e (sem A)]. In this manner, we can similarly
define [Γ ⊨ e : A] in terms of [sem] and [wp] for open programs with free
variables. By defining [Γ ⊨ e : A] in terms of [wp], we can use the lemmas of
[wp] to prove the fundamental theorem and adequacy theorem.

The semantic interpretation of types also allows us to prove that certain
programs semantically inhabit their type, even if they do not syntactically
inhabit the type. For instance, the following program is semantically of type
[TNat] (i.e., we can prove [⊨ e : TNat] , even though it is not syntactically
of that type (i.e., we cannot prove [⊢ e : TNat]:

   e := if true then 10 else 10 10

The reason why this is semantically of type [TNat], is that the condition is a
constant [true], and the then branch correctly returns a [TNat]. It is not
syntactically of type [TNat], because the else branch has a type error.

The fact that semantic typing allows one to take into account semantically, but
not syntactically typed code is crucial to reason about "unsafe" code in
real-life programming languages such as Rust. *)

(* In this section we give a simple example of a semantic typing proof for the
termination of well-typed programs in our language. For this proof, we will use
the big-step semantics from week 4. *)

Inductive big_step : expr -> val -> Prop :=
  | Val_big_step v :
     big_step (EVal v) v
  | Lam_big_step y e :
     big_step (ELam y e) (VClosure y e)
  | App_big_step y e1 e1' e2 v2 v :
     big_step e1 (VClosure y e1') ->
     big_step e2 v2 ->
     big_step (subst y v2 e1') v ->
     big_step (EApp e1 e2) v
  | Op_big_step e1 e2 op v1 v2 v :
     big_step e1 v1 ->
     big_step e2 v2 ->
     eval_bin_op op v1 v2 = Some v ->
     big_step (EOp op e1 e2) v
  | If_big_step_true e1 e2 e3 v :
     big_step e1 (VBool true) ->
     big_step e2 v ->
     big_step (EIf e1 e2 e3) v
  | If_big_step_false e1 e2 e3 v :
     big_step e1 (VBool false) ->
     big_step e3 v ->
     big_step (EIf e1 e2 e3) v.

(** The [inv_big] tactic can be used to repeatedly perform inversion on
[big_step] hypotheses where the first argument is a constructor. *)

Ltac inv_big :=
  repeat match goal with
  | _ => progress simplify_eq
  | H : big_step (EVal _) _ |- _ => inv H
  | H : big_step (ELam _ _) _ |- _ => inv H
  | H : big_step (EApp _ _) _ |- _ => inv H
  | H : big_step (EOp _ _ _) _ |- _ => inv H
  | H : big_step (EIf _ _ _) _ |- _ => inv H
  end.


(* ########################################################################## *)
(** * A program logic for termination  *)
(* ########################################################################## *)

(** We want to create a program logic for showing termination of programs in
our language. Termination is defined as follows. *)

Definition terminates e := exists v, big_step e v.

(** Unfortunately, establishing [terminates e] directly is difficult. We usually
need a stronger property during intermediate reasoning steps. The predicate
[terminates e] does not tell us anything about the return value of [e], while
the return value becomes relevant for termination if [e] is embedded inside a
larger program.

The main construct of our logic for reasoning about termination is a
strengthening of [terminates e] that can also tell us something about the return
value [wp e P]. The statement [wp e P] says that the program [e] terminates with
a value and this value satisfies predicate [P]. *)

Definition wp (e : expr) (P : val -> Prop) : Prop :=
  exists v, big_step e v /\ P v.

(** We show that [wp e P] implies termination of [e].  *)
Lemma wp_adequacy e P :
  wp e P -> terminates e.
Proof. intros (v&Hbig&?). by exists v. Qed.

(** Originally "wp" stood for "weakest precondition", but this terminology is
rather confusing in a more general context. You can instead interpret "wp" as
"Well-behaved Program". In general, we can define different wp's for different
properties of interest. For instance, the following [wp_safe e P] can be used
for establishing the [safe e] property of programs with small step semantics. *)

Definition wp_safe (e : expr) (P : val -> Prop) : Prop :=
  forall e', steps e e' -> (exists v, e' = EVal v /\ P v) \/ can_step e'.

(** The key to defining a wp for a property of interest is that it should imply
the original property, while also telling us something about the return values.
We will not use [wp_safe] in the rest of the exercises, because we have already
proved safety using the method of progress and preservation. *)

(** Let us prove some properties of wp to get a feeling about it. If the
expression is already a value, then [wp] holds if the value satisfies the
predicate. *)

Lemma EVal_wp (P : val -> Prop) v :
  P v -> wp (EVal v) P.
Proof.
  intros. exists v. eauto using Val_big_step.
Qed.

(** A corollary of the previous lemma for [ELam] *)

Lemma Lam_wp (P : val -> Prop) x e :
  P (VClosure x e) -> wp (ELam x e) P.
Proof.
  intros HP. exists (VClosure x e). eauto using big_step.
Qed.

(** The first real lemma is for [EApp]. The lemma says that if [subst x v e]
terminates and the return value satisfies [P] then [EApp (ELam x e) (EVal v)]
terminates and satisfies [P]. *)

Lemma App_wp P x e v :
  wp (subst x v e) P ->
  wp (EApp (EVal (VClosure x e)) (EVal v)) P.
Proof.
  intros (w & Hbig & HP).
  exists w. split; [|done].
  eauto using big_step, Val_big_step.
Qed.

(** The above lemma only applies if the arguments of [EApp] have already been
evaluated to values. The lemmas [App_wp_l] and [App_wp_r] below are called
"bind rules" for [EApp]. In effect, these allow use to split up a termination
proof for [EApp e1 e2] into steps:

- We first prove that [e1] and [e2] terminate.
- We then prove that [EApp (EVal v1) (EVal v2)] terminates, and satisfies the
  required predicate [P].

The proof of these rules involve some reasoning about the big-step semantics. *)

Lemma App_wp_l P e1 e2 :
  wp e1 (fun v1 => wp (EApp (EVal v1) e2) P) ->
  wp (EApp e1 e2) P.
Proof.
  intros (v1 & Hbig1 & v & Hbig & HP).
  exists v. split; [|done]. inv_big. by eapply App_big_step.
Qed.
Lemma App_wp_r P v1 e2 :
  wp e2 (fun v2 => wp (EApp (EVal v1) (EVal v2)) P) ->
  wp (EApp (EVal v1) e2) P.
Proof.
  intros (v2 & Hbig2 & v & Hbig & HP).
  exists v. split; [|done]. inv_big. eauto using big_step.
Qed.


(* ========================================================================== *)
(** * Exercise: Weakest precondition *)
(* ========================================================================== *)

(* Prove that [wp e P] is monotone in [P]. *)

Lemma wp_mono (P Q : val -> Prop) e :
  wp e P ->
  (forall v, P v -> Q v) ->
  wp e Q.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(** Prove the rules for [Op] and [If]. They are similar to those for [App]. *)

Lemma Op_wp (P : val -> Prop) op v1 v2 w :
  eval_bin_op op v1 v2 = Some w ->
  P w ->
  wp (EOp op (EVal v1) (EVal v2)) P.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma Op_wp_l P op e1 e2 :
  wp e1 (fun v1 => wp (EOp op (EVal v1) e2) P) ->
  wp (EOp op e1 e2) P.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.
Lemma Op_wp_r P op v1 e2 :
  wp e2 (fun v2 => wp (EOp op (EVal v1) (EVal v2)) P) ->
  wp (EOp op (EVal v1) e2) P.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma If_true_wp P e2 e3 :
  wp e2 P ->
  wp (EIf (EVal (VBool true)) e2 e3) P.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma If_false_wp P e2 e3 :
  wp e3 P ->
  wp (EIf (EVal (VBool false)) e2 e3) P.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma If_wp P e1 e2 e3 :
  wp e1 (fun v => wp (EIf (EVal v) e2 e3) P) ->
  wp (EIf e1 e2 e3) P.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Termination via semantic typing  *)
(* ########################################################################## *)

(** Now that we have our wp-logic for reasoning about termination, we can use it
to prove termination for all well-typed expressions. To do so, we first define a
semantic interpretation of syntactic types as predicates over values. *)

Fixpoint sem (A : ty) (v : val) : Prop :=
  match A with
  | TUnit => v = VUnit
  | TNat => exists n, v = VNat n
  | TBool => exists b, v = VBool b
  | TFun A1 A2 => forall w, sem A1 w -> wp (EApp (EVal v) (EVal w)) (sem A2)
  end.

(** Here we define a parallel substitution function, that can substitute
multiple variables simultaneously. This will later be used to state theorems
about open expressions in terms of closed expressions. *)

Fixpoint subst_map (vs : stringmap val) (e : expr) : expr :=
  match e with
  | EVal _ => e
  | EVar y =>
     match StringMap.lookup vs y with
     | Some v => EVal v
     | None => EVar y
     end
  | ELam y e => ELam y (subst_map (StringMap.delete y vs) e)
  | EApp e1 e2 => EApp (subst_map vs e1) (subst_map vs e2)
  | EOp op e1 e2 => EOp op (subst_map vs e1) (subst_map vs e2)
  | EIf e1 e2 e3 => EIf (subst_map vs e1) (subst_map vs e2) (subst_map vs e3)
  end.

(** We prove some properties about [subst_map] that we need later. *)

Lemma subst_map_empty e :
  subst_map StringMap.empty e = e.
Proof. induction e; simpl; by f_equal. Qed.

Lemma subst_map_insert x v e vs :
  subst_map (StringMap.insert x v vs) e
  = subst x v (subst_map (StringMap.delete x vs) e).
Proof.
  revert vs. induction e; intros vs; simpl; try (by f_equal).
  - (** Case var *)
    rewrite StringMap.lookup_delete, StringMap.lookup_insert.
    destruct (String.eq_dec _ _); simplify_eq.
    + by destruct (String.eq_dec _ _).
    + destruct (StringMap.lookup vs s) eqn:E; simpl; eauto.
      by destruct (String.eq_dec _ _).
  - (** Case lam *)
    destruct (String.eq_dec _ _); simplify_eq.
    + f_equal. f_equal. StringMap.map_solver.
    + f_equal. rewrite StringMap.delete_delete.
      destruct (String.eq_dec _ _); [done|].
      rewrite <-IHe. f_equal. StringMap.map_solver.
Qed.

Lemma subst_map_singleton x v e :
  subst_map (StringMap.singleton x v) e = subst x v e.
Proof.
  rewrite <-StringMap.insert_empty, subst_map_insert.
  by rewrite StringMap.delete_empty, subst_map_empty.
Qed.

(** We define semantic typing for contexts, [ctx_typed Gamma vs] to say that all
the variables in the environment [vs] have types given by the type environment
[Gamma]. *)

Definition ctx_typed (Gamma : stringmap ty) (vs : stringmap val) :=
  forall x A,
    StringMap.lookup Gamma x = Some A ->
    exists v, StringMap.lookup vs x = Some v /\ sem A v.

(** We are now ready to state our semantic typing judgment [sem_typed Gamma e A].
This judgment is the semantic analogue of [typed Gamma e A]. It says that
program [e] terminates for all variable assignments consistent with [Gamma], and
returns a value satisfying [A]. *)

Definition sem_typed (Gamma : stringmap ty) (e : expr) (A : ty) :=
  forall vs,
    ctx_typed Gamma vs ->
    wp (subst_map vs e) (sem A).

(** The adequacy theorem is easy to prove. We need some simple helper lemmas
about context typing, and then it follows nearly immediately from the definition
of the semantic typing judgment.*)

Lemma ctx_typed_empty :
  ctx_typed StringMap.empty StringMap.empty.
Proof. intros ? A. by rewrite StringMap.lookup_empty. Qed.

Lemma adequacy e A :
  sem_typed StringMap.empty e A ->
  terminates e.
Proof.
  intros Hsem. apply wp_adequacy with (sem A).
  rewrite <-(subst_map_empty e). apply Hsem.
  apply ctx_typed_empty.
Qed.

(** The next step is to prove a semantic version of every typing rule in
[typed]. For example, the following [Var_sem_typed] is the same as [Bool_typed],
but with [sem_typed] instead of [typed]. *)

Lemma Var_sem_typed Gamma x A :
  StringMap.lookup Gamma x = Some A ->
  sem_typed Gamma (EVar x) A.
Proof.
  intros Hx vs Hvs. simpl.
  unfold ctx_typed in Hvs. (** Recall that [ctx_typed] is a [forall ..] *)
  destruct (Hvs _ _ Hx) as (v&->&?). by apply EVal_wp.
Qed.

Lemma App_sem_typed Gamma e1 e2 A B :
  sem_typed Gamma e1 (TFun A B) ->
  sem_typed Gamma e2 A ->
  sem_typed Gamma (EApp e1 e2) B.
Proof.
  intros H1 H2 vs Hvs; simpl in *.
  eapply App_wp_l, wp_mono; [by apply H1|]. intros v1 Hv1.
  eapply App_wp_r, wp_mono; [by apply H2|]. intros v2 Hv2; simpl in *.
  by apply Hv1.
Qed.

Lemma ctx_typed_insert Gamma vs x v A :
  sem A v ->
  ctx_typed Gamma vs ->
  ctx_typed (StringMap.insert x A Gamma) (StringMap.insert x v vs).
Proof.
  intros HA HGamma y B Hy. rewrite StringMap.lookup_insert in Hy.
  destruct (String.eq_dec _ _); simplify_eq.
  - exists v. split; [|done]. StringMap.map_solver.
  - apply HGamma in Hy as (v'&?&?).
    exists v'. split; [|done]. StringMap.map_solver.
Qed.

Lemma Lam_sem_typed Gamma x e A B :
  sem_typed (StringMap.insert x A Gamma) e B ->
  sem_typed Gamma (ELam x e) (TFun A B).
Proof.
  intros He Hx vs; simpl. apply Lam_wp. intros w Hw.
  eapply App_wp. rewrite <-subst_map_insert.
  eauto using ctx_typed_insert.
Qed.


(* ========================================================================== *)
(** * Exercise: Semantic typing *)
(* ========================================================================== *)

(** Prove the following semantic typing rules. *)

Lemma bin_op_sem_progress op v1 v2 A1 A2 B :
  bin_op_typed op A1 A2 B ->
  sem A1 v1 ->
  sem A2 v2 ->
  exists w, eval_bin_op op v1 v2 = Some w /\ sem B w.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Op_sem_typed Gamma op e1 e2 A1 A2 B :
  bin_op_typed op A1 A2 B ->
  sem_typed Gamma e1 A1 ->
  sem_typed Gamma e2 A2 ->
  sem_typed Gamma (EOp op e1 e2) B.
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma If_sem_typed Gamma e1 e2 e3 B :
  sem_typed Gamma e1 TBool ->
  sem_typed Gamma e2 B ->
  sem_typed Gamma e3 B ->
  sem_typed Gamma (EIf e1 e2 e3) B.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

Lemma Val_sem_typed Gamma v A :
  sem A v ->
  sem_typed Gamma (EVal v) A.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma VUnit_sem : sem TUnit VUnit.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma VBool_sem b : sem TBool (VBool b).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma VNat_sem n : sem TNat (VNat n).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma VClosure_sem x e A B :
  sem_typed (StringMap.singleton x A) e B ->
  sem (TFun A B) (VClosure x e).
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

(** Now that we have all the typing rules, we can prove the "fundamental
theorem", which says that [typed Gamma e A] implies [sem_typed Gamma e A].
Use one of the preceding lemmas in each case. *)

(** Because our [typed] and [val_typed] judgments are mutually inductive, we
also have to use a mutually inductive proof to establish the fundamental theorem.
This is done in Coq using the syntax [Lemma lemma1... with lemma2...]. That
gives us the lemmas themselves as induction hypotheses. We have to manually
make sure that our proof is well-founded; immediately applying the induction
hypothesis will of course not work (you can try and you will see that Coq
complains at [Qed]). We first have to perform case analysis on the typing
judgment, and then we must be careful to only apply the induction hypothesis to
the smaller expressions and values that we get out of the [destruct].

Use the following proof structure.

  Proof.
    - intros He. destruct He.
      + ...
      + ...
    - intros Hv; destruct Hv.
      + ...
      + ...
  Qed.

For all cases you should use the lemmas we have proved. You should never unfold
the definition of [sem_typed] or [sem]. *)

Lemma fundamental Gamma e A :
  typed Gamma e A -> sem_typed Gamma e A
with fundamental_val v A :
  val_typed v A -> sem A v.
Proof. (* FILL IN HERE (12 LOC proof) *) Admitted.

(** With the fundamental property and adequacy theorem at hand, we can prove
that all well-typed programs terminate. *)

Lemma termination e A :
  typed StringMap.empty e A ->
  terminates e.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(** Because of the semantic typing approach, which separates the wp logic from
the semantic typing rules, we can manually prove that programs are semantically
well-typed, even if they are not syntactically well-typed. *)

Definition my_prog : expr :=
  EIf (EVal (VBool true))
    (EVal (VNat 10))
    (EApp (EVal (VNat 10)) (EVal (VNat 10))).

Lemma my_prog_not_typed Gamma :
  ~typed Gamma my_prog TNat.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma my_prog_sem_typed Gamma :
  sem_typed Gamma my_prog TNat.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Challenge *)
(* ########################################################################## *)

(** Prove that for our simple languages, termination implies safety. This tells
us that our semantic typing judgment actually gives both safety *and*
termination. *)

Lemma terminates_safe e :
  terminates e ->
  safe e.
Proof. (* FILL IN HERE (32 LOC helpers and 2 LOC proof) *) Admitted.

Lemma sem_typed_safe e A :
  sem_typed StringMap.empty e A ->
  safe e.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
