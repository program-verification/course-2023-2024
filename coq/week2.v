(** * Lecture 2: Introduction to Coq (part 2) *)
From pv Require Export library.

(** REMINDER:

          #####################################################
          ###  PLEASE DO NOT DISTRIBUTE SOLUTIONS PUBLICLY  ###
          #####################################################

*)


(** ######################################################################### *)
(** * Introduction *)
(** ######################################################################### *)

(** In this lecture we will cover the following topics:

- We discuss a number of Coq features to automatically find proofs, to look for
  lemmas from the standard library, and to write proofs in a more compact
  manner.
- We discuss the difference between algorithms and specifications by studying
  Boolean algorithms and Prop-based specifications for even and odd natural
  numbers.
- We take a look at the polymorphic list and option data types and the familiar
  functions on those. *)


(** ######################################################################### *)
(** * Coq tactics and searching for lemmas *)
(** ######################################################################### *)

(** ** The [tauto] tactic *)

(** The [tauto] tactic is a complete solver for intuitionistic propositional
logic. If you have a goal that can be solved solely by introduction and
elimination of the propositional connectives (truth [True], falsum [False],
conjunction [/\], disjunction [\/], implication [->], and negation [~]), then
[tauto] will solve it for you. *)

Lemma tauto_example_1 (P1 P2 P3 : Prop) :
  P1 /\ (P2 \/ P3) ->
  (P1 /\ P2) \/ (P1 /\ P3).
Proof. tauto. Qed.

(** The [tauto] tactic treats concrete propositions (like equality) as atoms. *)

Lemma tauto_example_2 (x y z1 z2 : nat) :
  x = y /\ (y = z1 \/ y = z2) ->
  (x = y /\ y = z1) \/ (x = y /\ y = z2).
Proof. tauto. Qed.

Lemma tauto_example_3 (P1 P2 Q R : Prop) :
  (P1 -> Q \/ False) ->
  P1 \/ P2 \/ Q ->
  ~(P2 /\ Q) ->
  ~P2 <-> Q.
Proof. tauto. Qed.


(** ** Searching for lemmas *)

(** In large proofs it is essential to avoid reinventing the wheel. You should
use the Coq standard library when possible (unless we explicitly ask you not to
in certain exercises or on the exam). You can search for definitions and lemmas
in Coq's libraries using the [Search] command. For example, let us assume
we are interested in inequality of natural numbers, then the [Search] command
below will display all binary relations on natural numbers. *)

Search (nat -> nat -> Prop).

(** We see [Nat.lt : nat -> nat -> Prop] and [lt : nat -> nat -> Prop], which
are likely the relations that we are looking for.

Note that [lt] is an unqualified version of [Nat.lt]. You can see that using the
[Print] command.  *)
Print lt.
Print Nat.lt.

(** Using the [Search] command below we can obtain all lemmas about a
definition. In some cases if there is both a qualified and non qualified version
(such as [Nat.lt] and [lt]) it could matter which one to use, which is a bit
unfortunate. *)

Search lt. (* many results, so we should use [lt] in [Search]. *)
Search Nat.lt.

(** The above list is very long, and finding the right lemma is not always easy.
We can limit our search as follows. *)
Locate lt.
Search "pos" lt.          (** All lemmas about [lt] with "pos" in the name. *)
Search lt Nat.add.        (** All lemmas about [lt] and [Nat.add]. *)
Search "<" "+".           (** The same, but using notations instead of names. *)
Search (_ + _ < _ + _).   (** Search by pattern. *)
Search (?n + _ < ?n + _). (** More refined search by pattern. *)


(** ** The [lia] tactic *)

(** Perhaps the most useful tactic for reasoning about natural numbers is [lia].
It automatically solves goals that involve linear arithmetic (in)equalities.
It is complete for propositional goals involving [<], [<=], [=], [+], [-], [S],
[0], and [*] where one argument is a constant. For goals involving arbitrary
multiplication [lia] is not complete, but is often still able to find a
proof. *)

Lemma lia_example_1 n m :
  n < 8 + 5*m ->
  m <= 3 - 4*n ->
  m < 4 /\ n < 8.
Proof.
  lia.
Qed.

Lemma lia_example_2 n m x y i :
  S m < S n ->
  x < S m ->
  (i < n -> x <> y) ->
  2*i + x < 2*n + x ->
  ~ x < y ->
  y + i < m + n.
Proof.
  lia.
Qed.

(** The following example cannot be solved by [lia]: It involves an unknown
function [f] and the premise involves a quantifier. Yet, [lia] can be of help
in a manual proof. *)

Lemma lia_example_3 (f : nat -> bool) i j :
  (forall n m, f (n + m) = f n && f m) ->
  f (S ((i + j) * 10)) = f (i + S (2 * j)) && f (j * 8 + i * 9).
Proof.
  intros Hf.
  (** We would like to rewrite with [Hf], but the its left-hand side does not
  match our goal. We use [replace ... with ... by lia] to turn our goal into the
  right shape to use [Hf]. *)
  replace (S ((i + j) * 10)) with ((i + S (2 * j)) + (j * 8 + i * 9)) by lia.
  (** Now we can use [Hf] to complete our proof. *)
  rewrite Hf.
  reflexivity.
Qed.

(** The above pattern occurs often: The lemma that you want to [apply] or
[rewrite] nearly matches the goal, but not entirely. Using [replace] you can
turn your goal into the right shape.

Note that you can use [replace t1 with t2 by tac] with any tactic [tac], not
just [lia]. You can also leave out [by tac], which results in an explicit goal
with the equality [t1 = t2] that you have to prove yourself. *)


(** ** The [congruence] and [simplify_eq] tactics *)

(** The [congruence] tactic implements a solver for equalities that can be
proved without knowing anything about the symbols involved, except that
constructors are injective. *)

Lemma congruence_example_1 a b c :
  (S a, S a) = (S b, S c) -> b = c.
Proof.
  congruence.
Qed.

Lemma congruence_example_2 (f g : nat -> nat) a  :
  f (f (f (f (f (f (f (f a))))))) = a ->
  f (f (f (f (f a)))) = a ->
  f a = a.
Proof.
  congruence.
Qed.

(* The [congruence] tactic also solves inconsistent goals, and can thus be used
as a more powerful version of [discriminate]. *)

Lemma congruence_example_3 (f g : nat -> nat) a  :
  f (f (f (f (f (f (f (f a))))))) = a ->
  f (f (f (f (f a)))) = a ->
  f a <> a ->
  0 = 1.
Proof.
  congruence.
Qed.

(** The [simplify_eq] tactic repeatedly applies [simpl], [subst], [injection]
and [discriminate]. See [library.v] for its definition. Its effect is somewhat
similar to [congruence], but where [congruence] fails if it cannot completely
solve the goal, [simplify_eq] returns a goal. *)

Lemma simplify_eq_example_1 a b c :
  (S a, S a) = (S b, S c) ->
  b + c = 2 * b.
Proof.
  intros H.
  simplify_eq.
  lia. (** The [lia] tactic does not work on the initial goal, since it involves
  pairs, i.e., it is not in the domain of linear integer arithmetic. *)
Qed.


(** ** The [done] tactic *)

(** As you might have noticed in the exercises of week 1, Coq proofs often end
with [reflexivity], [assumption] or [discriminate]. Instead of having to pick
such a tactic yourself for each leaf of your proof, the [done] tactic will do
so automatically. Roughly, [done] introduces all universal quantifiers,
implications and conjunctions, and then solves the goal with [reflexivity],
[assumption], or [discriminate]. Additionally, it uses symmetry of equality,
and solves simple inconsistent goals (e.g., with contradicting hypotheses [P]
and [~P], or [x <> x]).

You can use the syntax [by tac] for [tac; done] as a shorthand. *)

(** Let us see some examples from previous week. *)

Lemma add_0_l n :
  0 + n = n.
Proof. done. Qed.

(** The tactics [done] and [by tac] are useful when chaining tactics with [;].
Often many subgoals are solved in a similar but slightly different manner (e.g.,
one goal by [discriminate] and another by [reflexivity]). *)

Lemma xorb_false_inv b1 b2 :
  xorb b1 b2 = false -> b1 = b2.
Proof. by destruct b1, b2. Qed.

Lemma mul_0_r n :
  n * 0 = 0.
Proof. by induction n. Qed.

Lemma eq_add_0_1 n m :
  n + m = 0 -> n = 0 /\ m = 0.
Proof. by destruct n. Qed.

(** Finally we give some examples where [done] uses symmetry of equality or
derives contradictions. *)

Lemma done_example_1 n :
  n = 1 -> 1 = n.
Proof. done. Qed.

Lemma done_example_2 (n m : nat) :
  n = m -> n <> m -> 1 = 2.
Proof. done. Qed.

Lemma done_example_3 n :
  n <> n -> n = 10.
Proof. done. Qed.

Lemma done_example_4 n :
  n <> 1 -> 1 <> n.
Proof. done. Qed.


(** ** Destruct with equations *)

(** In week 1 we have used the [destruct] tactic to perform case analysis on
natural numbers and Booleans. Typically, we use [destruct t as ...] where [t]
is a variable. The [destruct] tactic can also be used on compound terms [t],
e.g., [f n] for a function [f]. The general form is [destruct t as ... eqn:H],
where [t] can be any term. This tactic also adds the corresponding equation
[H : t = ...] to each subgoal.

For instance, when doing [destruct t as [|n] eqn:H] where [t] is some term of
type natural number, we will have one subgoal with [H : t = 0], and one subgoal
with [H : t = S n]. *)

Lemma destruct_eqn_example (f : bool -> bool) :
  exists b, f b = b \/ f (f b) = b.
Proof.
  (** We use [destruct (f true) eqn:H] to determine whether
  [Htrue : f true = true] or [Htrue : f true = false]. *)
  destruct (f true) eqn:Htrue.
  - (** Case [f true = true] *)
    exists true. by left.
  - (** Case [f true = true] *)
    (** Now we do the same for [f false]. *)
    destruct (f false) eqn:Hfalse.
    + (** Case [f false = true] *)
      exists false. right. congruence.
    + (** Case [f false = false] *)
      exists false. left. congruence.
Qed.


(** ** Tacticals / tactic combinators *)

(** Coq supports various tactic combinators (aka "tacticals"). Below we provide
a list of the most common ones (the first two were already covered in week 1):

- [tac1; tac2]:           Execute [tac2] on all subgoals of [tac1]
- [tac1 || tac2]:         First execute [tac1], and use [tac2] if [tac1] fails
- [by tac]:               Syntactic suger for [tac; done]
- [try tac]:              Execute [tac] but do nothing if it fails
- [repeat tac]:           Repeatedly execute [tac]
- [do n tac]:             Execute [tac] [n] times
- [tac1; [tac2|tac3]]:    Execute [tac2] on the 1st subgoal and [tac3] on the 2nd
- [tac1; [tac2|..|tac3]]: Execute [tac2] on the 1st subgoal and [tac3] on the last

More examples:

- [tac1; [..|tac2|tac3|tac4]]
- [tac1; [tac2|..|tac3|tac4]]
- [tac1; [tac2|tac3|tac4|..]]

See the cheatsheet for more details about these combinators. We will give some
examples and use these combinators throughout this lecture. *)

Lemma add_0_r n :
  n + 0 = n.
Proof.
  (** The [ [tac|] ] combinator is often used when one subgoal can be solved
  immediately, for example, the base case of a proof by induction. *)
  induction n as [|n' IH]; [done|].
  simpl. by rewrite IH.
Qed.

Definition f (n : nat) : nat :=
  match n with
  | 0 => 1 | 1 => 0 | 2 => 4 | 3 => 3 | 4 => 2
  | n => n
  end.

Lemma f_twice n :
  f (f n) = n.
Proof.
  repeat (done || destruct n as [|n]).
  (** Why does [repeat (destruct n as [|n] || done)] loop? *)
Qed.


(** ** Introduction patterns *)

(** A considerable part of a Coq proof involves bookkeeping to introduce and
eliminate logical connectives, perform case analysis on data types, etc. To
reduce the amount of code that is needed to perform such bookkeeping, Coq has
so called "introduction patterns", which make it possible to unpack multiple
levels of logical connectives in one go. Introduction patterns [pat] can be used
as arguments of many Coq, including but not limited to [intros] and [destruct],
and can be nested. The most important introduction patterns are:

- [ [x H] ]     eliminate [exists x, P]
- [ [H1 H2] ]   eliminate [P /\ Q]
- [ [H1|H2] ]   eliminate [P \/ Q]
- [ [] ]        eliminate [False]
- [ [x y] ]     eliminate a pair [A * B]
- [ [|n] ]      eliminate a natural number
- [ [|] ]       eliminate a Boolean
- [ [->] ]      eliminate [x = y] by substituting [x] with [y]
- [ [<-] ]      eliminate [x = y] by substituting [y] with [x]
- [ [= H1 H2] ] turn [C x1 y1 = C x2 y2] into [x1 = x2] and [y1 = y2] for
                a constructor [C] (works for any arity)
- [ [=] ]       derive a contradiction from [C x1 y2 = C' x2 y2] if [C] and
                [C'] are different constructors (works for any arity)
- [ (x & y & z & ...) ] a shorthand for [ [x [y [z ...]] ]

Let us take a look at some basic examples. More realistic examples will appear
throughout the lecture. *)

Lemma intro_pattern_example_1 (P1 P2 P3 : Prop) :
  P1 /\ (P2 \/ P3) ->
  (P1 /\ P2) \/ (P1 /\ P3).
Proof.
  intros H.
  (* Without introduction pattens, the proof is quite verbose. We need multiple
  [destruct]s and we need to name auxilary hypotheses. *)
  destruct H as [H1 Haux].
  destruct Haux as [H2|H3].
Restart.
  intros H.
  (** With introduction patterns we eliminate the [/\] and [\/] in one go. *)
  destruct H as [H1 [H2|H3]].
Restart.
  intros [H1 [H2|H3]]. (** We can also do that as part of [intros]. *)
Abort.

Lemma intro_pattern_example_2 (P1 P2 P3 : nat -> Prop) :
  (exists x y, P1 x /\ P2 x /\ P3 y) ->
  (exists x, P1 x /\ P2 x) /\ (exists y, P3 y).
Proof.
  intros [x [y [H1 [H2 H3]]]]. (** Introduce [->], eliminate [exists] and [/\]. *)
Restart.
  intros (x & y & H1 & H2 & H3). (** The [(x & y & z & ...)] syntax is useful
  to "flatten" the introduction pattern when eliminating many nested [exists]
  and [/\] connectives. *)
Abort.

Lemma intro_pattern_example_3 (P : Prop) :
  P \/ False ->
  P.
Proof.
  intros [H|[]]. (** Perform [False] elimination in the second disjunct. *)
Abort.

Lemma intro_pattern_example_4 (P Q : nat -> Prop) :
  (exists nm, P (fst nm) /\ Q (snd nm)) ->
  exists n m, P n /\ Q m.
Proof.
  intros ([n m] & HP & HQ). (** You can also eliminate data types, e.g., pairs. *)
Abort.

Lemma intro_pattern_example_5 (P : nat -> Prop) n :
  (exists m, n = S m /\ P m) ->
  0 < n /\ P (pred n).
Proof.
  intros (m & -> & HP). (** And substitute equalities immediately. *)
Abort.

Lemma intro_pattern_example_6 (P : nat -> Prop) n :
  (exists m, S n = S m /\ P m) ->
  P n.
Proof.
  intros (m & [= Hn] & HP). (** And perform [injection]. *)
Abort.

Lemma intro_pattern_example_7 n :
  (exists m, (m < 2 /\ S n = S m) \/ (m < 3 /\ n < m)) ->
  n < 2.
Proof.
  (** A last example that combines a bunch of features. *)
  intros [m [[Hm2 [= ->]] | [Hm3 Hnm]]].
Abort.


(** ######################################################################### *)
(** * Exercises about Coq tactics and searching for lemmas *)
(** ######################################################################### *)

(** In the exercises below you are supposed to make use of the tactics we
discussed above ([tauto], [lia], [congruence], [simplify_eq], [done], the full
version of [destruct], and [replace]), tactic combinators, introduction
patterns, and Coq's [Search] command. Remember that the Coq cheatsheet in the
course repository gives an overview of the Coq tactics and commands that we
use. *)

(** All exercises can be solved using a script of 1-3 lines. *)

Lemma tactics_exercise_1 (f : bool -> bool) (b : bool) :
  f (f (f b)) = f b.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma tactics_exercise_2 n :
  (exists m, n = m /\ m < 2) \/ (exists p, p < 3 /\ n < p) ->
  Nat.sqrt n <= 3.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma tactics_exercise_3 (P Q R : nat -> Prop) :
  (exists n, (Q n <-> R n) /\ exists m, n = S m /\ P m /\ Q n) ->
  (exists i, R i /\ P (pred i)).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma tactics_exercise_4 a b c d :
  c <> 0 ->
  (b * c + a + d * c) mod c = a mod c.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.


(** ######################################################################### *)
(** * Prop versus bool *)
(** ######################################################################### *)

(** Coq's logic is constructive, which means that it distinguishes logical
propositions that can use quantifiers such as [forall] and [exists], from
Booleans that are always known to be [true] or [false].

Let us illustrate the difference with an example: even and odd numbers. In week
1 we have defined the functions [evenb : nat -> bool] and [oddb : nat -> bool].
We repeat their definitions here: *)

Fixpoint oddb (n : nat) : bool :=
  match n with
  | O => false
  | S n' => negb (oddb n')
  end.

Definition evenb (n : nat) : bool :=
  negb (oddb n).

(** Coq can automatically compute the truth or falsity of these functions
because they are algorithmic: *)

Compute (evenb 234). (** results in [true] *)
Compute (oddb 234).  (** results in [false] *)

(** In mathematics on the other hand, we usually define even and odd using
quantifiers: a number [n] is even if it can be written as [n = 2 * m], and
a number is odd if it can be written as [n = 2 * m + 1]. We can do that in Coq
too: *)

Definition even (n : nat) : Prop := exists m, n = m * 2.
Definition odd (n : nat) : Prop := exists m, n = S (m * 2).

(** These definitions are not algorithmic, so Coq cannot compute the truth or
falsity: *)

Compute (even 234). (** Gives some junk output *)

(** We can, on the other hand, manually prove these: *)

Lemma even_234 : even 234.
Proof.
  unfold even.
  exists 117. reflexivity.
Qed.

(** Clearly, Coq cannot be expected to automatically determine the truth or
falsity of an arbitrary quantified statement. Thus, the essential difference is
that [bool] does _not_ support quantifiers, but does support automatic
computation, whereas [Prop] does support quantifiers, but does _not_ support
automatic computation.

In classical mathematics, one typically does not distinguish [bool] and [Prop],
and nothing is automatically computed. A statement like [evenb 234 = true] would
have to be proven manually. Some proof assistants, like HOL, follow classical
mathematics and also do not distinguish [Prop] from [bool].

Note that in certain cases, Coq can only compute a result in theory, and not in
practice: [oddb (11 + 233 * 24 ^ (12 + 54 ^ (34 ^ 88)))] cannot be computed
automatically, but we can still manually "prove" it. To do that, we first need
to prove some lemmas about the [oddb] and [evenb] functions. *)

(** You proved [oddb_add] and [oddb_mul] in week 1. As a side note, we give
alternative proofs that showcase the new Coq features we introduced in the
previous section. *)

Lemma oddb_add n m :
  oddb (n + m) = xorb (oddb n) (oddb m).
Proof.
  induction n as [|n IH]; simpl; [done|].
  rewrite IH.
  (** We can "bruteforce" the proof by using [destruct] on compound terms
  instead of the lemma [xorg_negb_l] we used previous week. *)
  by destruct (oddb n), (oddb m).
Qed.

Lemma oddb_mul n m :
  oddb (n * m) = oddb n && oddb m.
Proof.
  induction n as [|n IH]; simpl; [done|]. (** The base case is trivial, so we
  use the tactic combinator [ [done|] ]*)
  rewrite oddb_add, IH. by destruct (oddb n), (oddb m).
Qed.

Lemma oddb_pow n m :
  oddb (n ^ m) = (m =? 0) || oddb n.
Proof.
  induction m as [|m IH]; simpl; [done|].
  rewrite oddb_mul, IH. by destruct (oddb n), (m =? 0).
Qed.

(** And let us get back to the example. *)

Lemma example_oddb :
  oddb (11 + 233 * 24 ^ (12 + 54 ^ (34 ^ 88))) = true.
Proof.
  (** The [reflexivity] tactic could prove this in theory, but not in practice.
  It will take very long (or will cause your machine to run out of memory or
  stack space). You can try to uncomment the following line if you are not
  convinced. *)
  (* reflexivity. *)

  (** We can prove it using our lemmas as follows: *)
  rewrite oddb_add, oddb_mul. rewrite oddb_pow.
  simpl. (** For this proof it is essential that [simpl] is lazy, why? *)
  done.
Qed.

(** Now that we have two definition of being odd/even, we would like to relate
these. We can do this by proving the following lemmas:

- [oddb_odd : forall n, oddb n = true <-> odd n]
- [evenb_even : forall n, evenb n = true <-> even n]

These lemmas say that the [bool] version results in [true] if and only if the
[Prop] version is true.

Let us take a look at [oddb_odd], you will prove the lemma [evenb_even] yourself
in the exercises. Since the lemma concerns a bi-implication, we need to prove
two implications [oddb n = true -> odd n] and [odd n -> oddb n = true]. Let us
start with the second one, for which we first include a lemma from week 1. *)

Lemma oddb_mul_2 n :
  oddb (n * 2) = false.
Proof.
  induction n as [|n IH]; simpl; [done|].
  by rewrite IH.
Qed.

Lemma oddb_odd_2 n :
  odd n -> oddb n = true.
Proof.
  intros [m ->]. (** Perform exist-elimination and substitution using the
  introduction pattern [[m ->]]. If you find introduction patterns hard to parse,
  you can desuraging them into [intros H. destruct H as [m H]. rewrite H]. *)
  simpl. by rewrite oddb_mul_2.
Qed.

(** The other direction, [oddb n = true -> odd n], is more challenging to
prove. If we attempt to prove it naively by induction, we will get stuck. Our
induction hypothesis is not strong enough. *)

Lemma oddb_odd_1 n :
  oddb n = true -> odd n.
Proof.
  intros Hn. induction n as [|n IH]; simpl in Hn; [done|].

  (** We know that [negb (oddb n) = true], or equivalently that
  [negb (oddb n) = false]. This does not help us much because the induction
  hypothesis talks has premise [oddb .. = true]. If we take a look at the
  definition of [oddb], we see that for consecutive numbers it flips between
  [true] and [false]. Let us thus perform a case analysis to end up with a
  number for which [oddb] is [true] again. *)

  destruct n as [|n]; simpl in Hn.
  - exists 0. lia.
  - (* Clean up the double [negb] *)
    rewrite negb_involutive in Hn.
    (** Now both [Hn] and the induction hypothesis [IH] talk about [oddb] being
    [true], but there is a mismatch: [Hn] talks about the number [n], but [IH]
    talks about the number [S n]. In other words, our induction hypothesis is
    too weak. *)

Restart.
  (** Instead of using structural induction, we will use "strong induction" (aka
  "well-founded" induction) to obtain an induction hypothesis for any smaller
  number. This principle is called [lt_wf_inf] in Coq. You can use [About] to
  see the definition. (You can try to prove this principle using ordinary
  structural induction yourself). *)
  About lt_wf_ind.
  (**
    forall (n : nat) (P : nat -> Prop),
      (forall n' : nat, (forall m : nat, m < n' -> P m) -> P n') ->
      P n
  *)

  intros Hn.
  (** We use the [using] flag of [induction] to tell Coq that we want to use
  the strong induction principle [lt_wf_ind]. *)
  induction n as [n IH] using lt_wf_ind.
  (** Now let us use the introduction pattern [ [|[|n']] ] to distinguish three
  cases [n = 0], [n = 1], and [n = S (S n')]. *)
  destruct n as [|[|n']]; simpl in Hn.
  - done.
  - exists 0. done.
  - rewrite negb_involutive in Hn.
    destruct (IH n') as [k Hk].
    { lia. }
    { done. }
    exists (S k). lia.
Qed.

(** Now we can derive the [<->] version using the lemmas for both directions. *)

Lemma oddb_odd n :
  oddb n = true <-> odd n.
Proof. split; [apply oddb_odd_1|apply oddb_odd_2]. Qed.

(** The following table gives an overview of the logical connectives in [Prop]
and the corresponding versions in [bool].

Prop             bool
------------------------
True             true
False            false
P /\ Q           p && q
P \/ Q           p || q
~ P              negb p
forall x, P x    N/A
exists x, P x    N/A
n = m            n =? m
n < m            n <? m
n <= m           n <=? m

If you need lemmas about (the interaction between) these connectives, remember
to use the [Search] command. For example: *)

Search "<" "<?".

(** Gives all lemmas involving both "<" and "<?". The most important lemma is
[Nat.ltb_lt n m : (n <? m) = true <-> n < m], which relates truth of the
Boolean and [Prop] version (compare this lemma to [oddb_odd]). *)


(** ######################################################################### *)
(** * Exercises about Prop versus bool *)
(** ######################################################################### *)

(** Prove the lemmas below. Many of these lemmas could be proven by (strong)
[induction], but for some lemmas there exist shorter (and easier) proofs that
reuse lemmas that have previously been proven. *)

Lemma even_or_odd n :
  even n \/ odd n.
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma not_even_and_odd n :
  ~ (even n /\ odd n).
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

(** Since Coq is constructive, we do not have the principle of excluded middle
[P \/ ~P] for every [P]. We can prove it for specific propositions [P], such as
[odd n], though. *)

Lemma odd_em n :
  odd n \/ ~odd n.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma evenb_even n :
  evenb n = true <-> even n.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.


(** ######################################################################### *)
(** * The list and option data types *)
(** ######################################################################### *)

(** In week 1 we have defined the natural numbers and Booleans as inductive
types in Coq. Now we take a look at the list and option types. An important
difference between natural numbers/Booleans and list/option is that list/option
are polymorphic, i.e., list/option are parametric in the type of elements.

In the module [list_defs] below we give the definition of the [list] and
[option] type and show the definitions of a number of familiar functions.
All of these definitions are also in Coq's standard library. *)

Module list_defs.
  (** With the [A : Type] syntax we tell Coq that the [list] type is parametric
  in the type [A] of elements. *)
  Inductive list (A : Type) :=
    | nil : list A
    | cons : A -> list A -> list A.

  (** Polymorphism is handled differently in Coq compared to functional
  languages like OCaml and Haskell. The constructors [nil] and [cons] have 1
  and 3 arguments, respectively---i.e., one more argument than you might expect.
  The additional is the type [A] of elements. *)

  About nil.  (** [nil : forall A : Type, list A] *)
  About cons. (** [cons : forall A : Type, A -> list A -> list A] *)

  Check cons nat 10 (cons nat 11 (nil nat)).

  (** In most cases Coq can infer the type argument [A] by type checking. For
  example, in the above list, [A] can be derived from the fact that [10] and
  [11] have type [nat]. We instruct Coq to automatically infer type arguments
  using the following commands (in Coq terminology, we say that we make the
  arguments between braces "implicit"): *)

  Arguments nil {_}.
  Arguments cons {_}.

  Check cons 10 (cons 11 nil).

  (** Side note: You can make _any_ argument implicit, including value arguments.
  For example, you can write [Arguments cons {_ _}] to also make the head of
  the list implicit. This is unlikely what you want, since we cannot expect Coq
  to invent values out of thin air. *)

  (** In some cases, we want to explicitly pass an implicit (type) argument.
  We can do so by prefixing a constructor or function name with an [@]. *)

  Check nil. (* [nil : list ?A] *)
  Check (@nil nat). (* [nil : list nat] *)

  (** We define the familiar notations for lists. *)

  Infix "::" := cons.
  Notation "[]" := nil.
  Notation "[ x ]" := (cons x nil).
  Notation "[ x ; y ; .. ; z ]" := (cons x (cons y .. (cons z nil) ..)).

  Check [10; 11].

  (** Now let us define some functions on lists. *)

  (** To define polymorphic functions, we need to add an implicit (i.e.,
  automatically inferred) type argument with syntax [{A}]. *)

  Fixpoint length {A} (xs : list A) : nat :=
    match xs with
    | [] => 0
    | x :: xs => S (length xs)
    end.

  Compute length [10; 11]. (** Gives [2]. *)

  Fixpoint app {A} (xs1 xs2 : list A) : list A :=
    match xs1 with
    | [] => xs2
    | x :: xs1 => x :: app xs1 xs2
    end.

  Infix "++" := app.

  Compute [10; 11] ++ [20; 21]. (** Gives [ [10; 11; 20; 21] ]. *)

  Fixpoint rev {A} (xs : list A) : list A :=
    match xs with
    | [] => []
    | x :: xs => rev xs ++ [x]
    end.

  Compute rev [10; 11; 20; 21]. (** Gives [ [21; 20; 11; 10] ]. *)

  (** The function [concat] takes a list of lists and appends all lists to
  each other. *)

  Fixpoint concat {A} (xss : list (list A)) : list A :=
    match xss with
    | [] => []
    | xs :: xss => xs ++ concat xss
    end.

  Compute concat [[10; 11]; [20; 21]; [30; 31]].
    (** Gives [ [10; 11; 20; 21; 30; 31] ]. *)

  (** Aside from functions that produce data (like another list or a value),
  Coq allows us to define functions that produce propositions. For example, the
  function [In x' xs] defines the proposition that specifies whether an element
  [x'] is in list [xs]. *)

  Fixpoint In {A} (x' : A) (xs : list A) : Prop :=
    match xs with
    | [] => False
    | x :: xs => x = x' \/ In x' xs
    end.

  (** The option data type, which is often used to define partial functions, is
  defined in a similar fashion as the list data type. *)

  Inductive option (A : Type) :=
    | None : option A
    | Some : A -> option A.
  Arguments None {_}.
  Arguments Some {_}.

  (** We use the option type to define the function [nth_error xs i]. This
  function looks up the [i]th element of the list. If the index [i] is out of
  bounds, it returns [None]. *)

  Fixpoint nth_error {A} (xs : list A) (i : nat) : option A :=
    match i, xs with
    | 0, [] => None
    | 0, x :: xs => Some x
    | S i, [] => None
    | S i, x :: xs => nth_error xs i
    end.
End list_defs.

(** Now let us take a look at some proofs about lists. All the tactics that we
have seen (e.g., [simpl], [destruct], [induction], [injection], [discriminate],
[f_equal]) generalize to lists in the obvious way. *)

Lemma app_nil_r {A} (xs : list A) :
  xs ++ [] = xs.
Proof.
  induction xs as [|x xs' IH].
  - (** Base case, [xs = []]. *)
    simpl. reflexivity.
  - (** Inductive case, [xs = x :: xs']. *)
    simpl. rewrite IH.
Restart.
  (** Let us put the material from the beginning of the lecture to practice to
  shorten the proof. Note that we omit the [as] argument of [induction] since
  we do not need to refer to variables ourselves. *)
  induction xs; simpl; congruence.
Qed.

Lemma app_assoc {A} (xs ys zs : list A) :
  xs ++ (ys ++ zs) = (xs ++ ys) ++ zs.
Proof.
  induction xs; simpl; congruence.
Qed.

(** The following lemma states that an element [x] is in a list [xs] if there
exists a corresponding index [i] at which we can find [x]. *)

Lemma in_nth_error {A} (xs : list A) x :
  In x xs <-> exists i, nth_error xs i = Some x.
Proof.
  (** Instead of proving both directions of the [<->] individually, let us prove
  both at the same time using a single induction. *)
  induction xs as [|x' xs' IH]; simpl.
  - (** Base case *)
    (** One direction is an immediate contradiction, we use [done]. *)
    split; [done|].
    (** Since [nth_error] is defined by pattern matching on both arguments, we
    need to perform a case analysis on the index to obtain a contradiction. We
    can do that concisely by nesting introduction patterns. *)
    intros [[|i] [=]].
  - (** Inductive case *)
    split.
    + intros [->|Hin].
      * by exists 0.
      * (** Note that we can also use [apply] on hypotheses. Additionally, we
        can use [as pat] to immediately perform some operations on the result. *)
        apply IH in Hin as [i Hin].
        by exists (S i).
    + intros [[|i] Hnth]; simplify_eq.
      * by left.
      * right. apply IH. by exists i.
Qed.


(** ######################################################################### *)
(** * Exercises about the list and option data types *)
(** ######################################################################### *)

Lemma in_app_iff {A} (xs1 xs2 : list A) x :
  In x (xs1 ++ xs2) <-> In x xs1 \/ In x xs2.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma length_app {A} (xs1 xs2 : list A) :
  length (xs1 ++ xs2) = length xs1 + length xs2.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(** To prove the following lemma, you should declare and prove a helper lemma. *)

Lemma rev_rev {A} (xs : list A) :
  rev (rev xs) = xs.
Proof. (* FILL IN HERE (8 LOC helpers and 2 LOC proof) *) Admitted.

(** Hint: Remember that [apply] and [apply ... in] work with bi-implications.
Furthermore, it might come in handy that [rewrite] also supports rewriting with
bi-implication [<->] in addition to equality [=]. *)

Lemma in_rev_iff {A} (xs : list A) x :
  In x (rev xs) <-> In x xs.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma in_concat_iff {A} (xss : list (list A)) x :
  In x (concat xss) <-> exists xs, In x xs /\ In xs xss.
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma nth_error_Some_iff {A} (l : list A) n x :
  nth_error l n = Some x <->
  exists l1 l2, l = l1 ++ x :: l2 /\ length l1 = n.
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.
