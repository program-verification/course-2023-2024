(** * Lecture 11: Separation logic (interactive proofs) *)
From pv Require Export proofmode.
Import language_notation hoare_notation.

(* REMINDER:

          #####################################################
          ###  PLEASE DO NOT DISTRIBUTE SOLUTIONS PUBLICLY  ###
          #####################################################

*)

(* ########################################################################## *)
(** * Introduction *)
(* ########################################################################## *)

(** In the previous two lectures, we have studied the theory of separation
logic. We showed how to model separation logic propositions and Hoare triples.
In this lecture, we will use separation logic to verify some concrete programs
that manipulate linked data structures such as lists and trees.

To verify a concrete program, we need prove a Hoare triple, which expands to an
entailment with a weakest precondition. We thus need an effective way to prove
separation logic entailments. We have seen two ways to prove entailments:

1. Go into the model by **unfolding** the definition of the separation logic
   connectives ([unfold sepSep, sepExist], etc).
2. **Derive** the entailment using the (primitive) proof rules of separation
   logic (and chaining these rules together with [sepEntails_trans]).

Option (1) is not appealing since it defeats the purpose of separation logic.
Its purpose is to hide reasoning about heap disjointness. By unfolding the
separation logic connectives, we expose ourselves to tedious reasoning about
disjointness again.

Option (2) avoids the reasoning about heap disjointness. Unfortunately, as we
have seen for small examples in the previous lectures, this option is very
tedious due to reasoning up to associativity and commutativity, and having to
perform many low-level proof steps by hand.

In this lecture, we consider a third option. Derive the entailment using tactics
for separation logic. The tactics that we consider come from the "proof mode" of
the Iris framework for separation logic in Coq. The Iris Proof Mode extends Coq
with an additional context for separation logic hypotheses and provides tactics
to manipulate this context. The tactics of the Iris Proof Mode are designed to
resemble the Coq tactics. For example, the Iris Proof Mode [iIntros], [iApply]
and [iDestruct] tactics behave almost identical to Coq's [intros], [apply] and
[destruct] tactics, respectively.

The objectives of this lecture are as follows:

- We familiarize ourselves with the Iris Proof Mode tactics by using it to
  prove some tautologies in separation logic.
- We use the Iris Proof Mode to prove properties of programs. For this, we will
  have formalize "representation predicates" (which we briefly and informally
  touched on in the previous lecture) in Coq.

IMPORTANT: In addition to the Iris Proof Mode tactics, Iris comes with an
expressive "higher-order impredicative concurrent separation logic" with its
own type of propositions [iProp] (replacing our [sepProp]) and its own [wp]
that supports languages with concurrency. In the lectures of this course we will
only use the Iris Proof Mode tactics and apply these to our own [sepProp]
separation logic. In the projects (or a master thesis or master internship)
there is the option to explore the full Iris.

The course file [proofmode.v] provides the setup to make the Iris Proof Mode
work with [sepProp]. You do not need to understand that file, and can treat it
as a black box. *)


(* ########################################################################## *)
(** * Installation *)
(* ########################################################################## *)

(** Make sure you have a working version of opam (2.0.0 or newer), and run:

  opam repo add coq-released https://coq.inria.fr/opam/released
  opam install coq-iris

Notes:

- See https://gitlab.mpi-sws.org/iris/iris#working-with-iris for detailed
  instructions. (Installing Iris via [make install] instead of opam is **not**
  recommended since you need to manually install the right versions of the
  dependencies).
- You should use Iris 4.1.0 or 4.2.0.
- For this lecture, you only need to install [coq-iris].

If you are using Windows, you have the following options to install Iris:

- Use a VM with an operating system that has a proper terminal so you can
  use opam.
- Use the Windows Subsystem for Linux (WSL) so that you get a proper terminal
  and can use opam.
- Use the Coq platform [https://github.com/coq/platform/releases/tag/2023.11.0],
  which provides an installer that bundles Coq with a collection of popular Coq
  libraries, such as Iris. We recommend to use version 2023.11.0 of the Coq
  platform, which includes Iris 4.1.0.

Resources:

- https://gitlab.mpi-sws.org/iris/iris#working-with-iris
  Installation instructions for Iris.
- https://gitlab.mpi-sws.org/iris/iris/-/blob/master/docs/proof_mode.md
  Overview of all the Iris Proof Mode tactics.
- https://robbertkrebbers.nl/research/articles/proofmode.pdf
  Research paper that describes the implementation of the Iris Proof Mode. This
  paper provides additional reading material. For the purpose of this course,
  you can consider the Iris Proof Mode as a black box. *)


(* ########################################################################## *)
(** Motivation *)
(* ########################################################################## *)

(** To motivate the need for specialized separation logic tactics, such as the
Iris Proof Mode, let us try to derive a simple lemma in separation logic. We
will do this using the proof rules that we established in week 9. *)

Lemma sep_exist_middle_manual {A} (P R : sepProp) (Phi : A -> sepProp) :
  P ** (Ex a, Phi a) ** R |~ Ex a, R ** Phi a ** P.

(** Intuitively, to prove this lemma, we need to:

- Distribute the existential over the separating conjunction (rule
  [sepSep_exists]).
- Change the order of the conjunctions using associativity and commutativity
  of separating conjunction (rules [sepSep_comm] and [sepSep_assoc]).

We put this together using transitivity of entailment, monotonicity of
separating conjunctions and existential quantifiers. *)

Proof.
  eapply sepEntails_trans; [|apply sepSep_exists].
  eapply sepEntails_trans; [|apply sepSep_comm].
  eapply sepEntails_trans; [apply sepSep_assoc|].
  apply sepSep_mono_l.
  eapply sepEntails_trans; [apply sepSep_exists|].
  apply sepExist_elim; intros x.
  apply sepExist_intro_alt with x.
  apply sepSep_comm.
Qed.

(** There are some issues here:

- The proof requires an excessive amount of work that is not in line with the
  simplicity of the lemma statement.
- The proof is very hard to understand. The intermediate proof states do not
  reflect the intuitive reasoning steps that a human performs in their head.
- If you were to change the lemma statement a bit, nearly the entire proof
  breaks down. That is, refactoring is very hard. *)

(** If you are not yet convinced that the above approach does not scale, try
to prove the following lemma (note that going into the model by unfolding the
definitions of the separation connectives is unbearable too). *)

Lemma too_many_wands {A} (Phi Psi : A -> sepProp) (P Q R : sepProp) :
  ((Ex x, P -** Phi x ** Psi x) -** Q -** R)
  |~ All x y, Phi x -** @[ x = y ] ** (P -** Psi y) ** Q -** R.
Proof. Abort.

(** What we instead would like is to have tactics that perform exactly the
introduction and elimination of the separation logic connectives. We would like
to have tactics that behave similar to Coq's. So let us briefly take a look at
a Coq proof (with [/\] instead of [*], and [->] instead of [|~]), so that we
have a baseline for our separation logic proof. *)

Lemma and_exist_middle {A} (P R : Prop) (Phi : A -> Prop) :
  P /\ (exists a, Phi a) /\ R -> exists a, R /\ Phi a /\ P.
Proof.
  intros [HP [HPhi HR]].
  destruct HPhi as [x HPhi].
  exists x.
  split.
  - assumption.
  - split; assumption.
Qed.

(* ########################################################################## *)
(** Basic interactive proofs *)
(* ########################################################################## *)

(** Now let us see how we can carry out separation logic proofs using the
Iris Proof Mode. As we will quickly see in the example below, we get goals of
the following shape:

  A, B .. : Type
  x, y, .. : A
  P1, P2, ... : sepProp
  ______________________________________(1/1)
  "H1" : P1
  "H2" : P2
  ..
  "Hn" : Pn
  --------------------------------------∗
  Q

What we see here is that in addition to the ordinary Coq context (above the
solid bar), there is a "separation logic context" (above the dashed bar with
the ∗). This context is often called the "spatial context" since separation
logic hypotheses express ownership. The semantics of an Iris Proof Mode goal is:

  P1 ** P2 ** ... ** Pn |~ Q

So, we have to prove the conclusion provided all hypotheses hold separately.

The names of the hypotheses have no semantic meaning, they are only there so
we can refer to them in tactics. *)

Lemma sep_exist_middle {A} (P R : sepProp) (Phi : A -> sepProp) :
  P ** (Ex a, Phi a) ** R |~ Ex a, R ** Phi a ** P.
Proof.
  (** Similar to Coq's [intros] tactic, but prefixed with an "i" to signify
  that it is an Iris Proof Mode tactic. *)
  iIntros "[HP [HPhi HR]]".
  (** After executing this tactic, you see that the separation logic context
  has appeared. *)
  (* Note that names and introduction patterns for manipulating the separation
  logic context are within string quotation marks "...". *)

  (** Similar to Coq's [destruct] tactic. *)
  iDestruct "HPhi" as (x) "HPhi".
  (** The variable [x] goes into the Coq context, so it is not a string, but put
  between parentheses. *)

  (** Similar to Coq's [exists] tactic. *)
  iExists x.

  (** Now we end up at the key point of the proof. We have to introduce the
  separating conjunction. Since we have to use every separation logic hypothesis
  **exactly once**, we need to tell how the hypotheses should be subdivided
  between the left and right separating conjunct. We do this using the
  [iSplitL Hs] tactic, where [Hs] is a list of hypotheses that should go to the
  left separating conjunct. The remaining hypotheses go to to the right
  separating conjunct. (There is also the [iSplitR] tactic, which behaves
  dually to [iSplitL].) *)
  iSplitL "HR".
  - iAssumption.
  - iSplitR "HP"; iAssumption.

  (** You can try to experiment with the above proof by checking out what
  happens if the split is _wrong_. You will notice that [iAssumption] fails
  because either the goal is not in the context, or because there are other
  hypotheses left. (Recall: separation logic hypotheses need to be used
  exactly once!)*)
Restart.
  (** Let us consider an alternative proof. *)
  (** We can inline the [iDestruct] into the introduction pattern. *)
  iIntros "[HP [[%x HPhi] HR]]".
  (** To escape to the Coq level from an Iris Proof Mode introduction pattern,
  we prefix the variable [x] with a percent sign [%]. *)

  (** A common proof technique in separation logic is "framing", which is to
  cancel a hypothesis in the goal. The Iris Proof Mode provides the [iFrame]
  tactic for that. *)
  iFrame "HP HR".

  (** We finish the proof by [eauto], which will instantiate the existential
  with an evar and solve the goal by [iAssumption]. *)
  eauto.
Restart.
  iIntros "[HP [[%x HPhi] HR]]".
  (** The [eauto] tactic will not solve the goal immediately. To introduce the
  separating conjunction, is has to split the context between both conjuncts.
  There are exponentially many ways of doing that, so it will not even attempt
  because it would simply be too ineffecient. *)
  Fail solve [eauto].

  (** We can tell [eauto] to perform [iFrame] if it encounters a separating
  conjunction. While this is much cheaper than trying all splits, it might
  still be expensive in larger proofs, so this behavior is opt-in, and not the
  default. *)
  eauto with iFrame.
Restart.
  (** We can also frame directly in the introduction pattern using the [$]
  sign. *)
  by iIntros "($ & ? & $)".
Qed.

(** The following two lemmas demonstrate the [iApply] tactic. *)

Lemma sep_curry (P Q R : sepProp) :
  (P ** Q -** R) |~ P -** Q -** R.
Proof.
  iIntros "H HP HQ". iApply "H". iFrame.
Qed.

Lemma sep_uncurry (P Q R : sepProp) :
  (P -** Q -** R) |~ P ** Q -** R.
Proof.
  iIntros "H [HP HQ]".
  (** Our "H" contains two nested magic wands, so we need to determine how our
  resources are split over the two premises. By default, [iApply] sends all
  hypotheses go to the first goal. In our example, that default behavior makes
  it impossible to complete the proof. *)
  iApply "H".
  - (* Goal containing hypotheses [HP] and [HQ] *) admit.
  - (* Goal containing no hypotheses *) admit.
Restart.
  iIntros "H [HP HQ]".
  (* We can specify how the hypotheses are split using the [with] clause. *)
  iApply ("H" with "[HP] [HQ]").
  - (* Goal containing just hypothesis [HP] *) iAssumption.
  - (* Goal containing just hypothesis [HQ] *) iAssumption.
Restart.
  iIntros "H [HP HQ]".
  (** If goals are trivial, you can put a [//] to call [done]. *)
  iApply ("H" with "[HP //] [HQ //]").
Restart.
  iIntros "H [HP HQ]".
  (** The brackets [ [ .. ] ] in the [with] clause signify the creation of a
  goal. If the hypothesis matches the goal exactly, you can leave out the
  brackets. Then no goal is generated. *)
  iApply ("H" with "HP HQ").
Restart.
  iIntros "H [HP HQ]".
  (* You can also leave out the [with] clause for the last goal. This goal will
  get all hypotheses that are not used for prior premises. *)
  by iApply ("H" with "HP").
Qed.

(** The following lemma demonstrates the [iClear] tactic. *)

Lemma sepSep_emp_r_inv (P : sepProp) :
  P ** EMP |~ P.
Proof.
  iIntros "[HP HEMP]".
  (** You generally cannot clear a hypothesis because it could assert ownership,
  and thus needs to be used exactly once. *)
  Fail iClear "HP".

  (** But you can clear hypotheses that _do not_ assert ownership, such as [EMP]
  and the "pure" connective [ @[ p ] ]. *)
  iClear "HEMP".
Restart.
  (** You can use the [_] introduction pattern to [iClear]. *)
  iIntros "[HP _]".
  done.
Restart.
  iIntros "[HP HEMP]".
  (** [iAssumption] will automatically clear unused hypotheses, provided they
  do not assert ownership. *)
  iAssumption.
Qed.

(** The following lemma demonstrates how to deal with universal quantifiers
[All x. P] and pure propositions [ @[ p ] ]. *)

Lemma sepPure_10_10 (P : sepProp) :
  P |~ @[ 10 = 10 ] ** P.
Proof.
  iIntros "HP".
  (** Pure propositions [ @[ p ] ] do not assert ownership, so all hypotheses
  should go to the right. *)
  iSplitR; [|done].
  (** We can introduce a pure propositions [ @[ p ] ] using [iPureIntro]. We
  will then leave the Iris Proof Mode, and obtain an ordinary Coq goal for
  proving [p]. *)
  iPureIntro.
  done.
Qed.

Lemma sepForall_example_1 {A} (Phi : A -> sepProp) x :
  Phi x |~ All y, @[ x = y ] -** Phi y.
Proof.
  iIntros "HPhi".
  (** Introduction of the universal quantifier [All] in separation logic is
  done using [iIntros]. *)
  iIntros (y).
  (** Since we introduce into the Coq context, the variable [y] is between
  parentheses and not in string quotation marks. *)

  iIntros "Heq".

  (** We can move a pure hypotheses to the Coq context using the tactic
  [iDestruct .. as %...]. *)
  iDestruct "Heq" as %Heq.

  (** And use regular Coq tactics like [subst] and [rewrite] on that variable. *)
  subst x.
  done.
Restart.
  (** We can combine the three [iIntros] into one. *)
  iIntros "HPhi" (y) "Heq".
Restart.
  (** We can also directly substitute using the [->] pattern. *)
  iIntros "HPhi" (y) "->".
Restart.
  (** We can also use [%y] instead of [(y)]. *)
  iIntros "HPhi %y ->".
  done.
Restart.
  (** Or put the [y] and [->] between parentheses. *)
  iIntros "HPhi" (y ->).
  done.
Qed.

Lemma sepForall_example_2 {A} (Phi : A -> sepProp) x :
  (All y, @[ x = y ] -** Phi y) |~ Phi x.
Proof.
  iIntros "HPhi".
  (** The hypothesis contains a universal quantifier, which we can instantiate
  using the [iSpecialize] tactic (similar to Coq's [specialize]). *)
  iSpecialize ("HPhi" $! x).
  (** We can do the same that for magic wands, but we have to specify which
  resources we need for the premise. *)
  iSpecialize ("HPhi" with "[]").
Restart.
  iIntros "HPhi".
  (** We can combine both [iSpecialize]s into one. *)
  iSpecialize ("HPhi" $! x with "[]").
Restart.
  iIntros "HPhi".
  (** To [iApply] a hypothesis with a universal quantifier, we can use the [$!]
  syntax to specify the witness. *)
  iApply ("HPhi" $! x).
Restart.
  iIntros "HPhi".
  (** If you do not specify the witness, it will be instantiated with an evar
  which might get unified. *)
  iApply "HPhi".
  done.
Qed.


(* ########################################################################## *)
(** Exercises on basic interactive proofs *)
(* ########################################################################## *)

(** Prove the following lemmas. In addition to standard Coq tactics like [done]
and [rewrite], you should use the Iris Proof Mode tactics [iIntros], [iFrame],
[iDestruct], [iExists], [iApply], [iSplitL], and [iSplitR].

Think carefully if variables should be between string quotation marks (if they
concern the separation logic context) or not (if they concern the Coq context).

Try to optimize your proof using introduction patterns:

- [ [H1 H2] ] for separating conjunction elimination
- [ (H1 & H2 & H3 & H4) ] for iterated separating conjunction elimination
- [ [%x H] ] for existential elimination
- [ %H ] for moving a pure hypothesis to the Coq context
- [ $ ] for framing
- [ -> ] and [ <- ] for rewriting *)

Lemma sepSep_emp_r (P : sepProp) :
  P |~ P ** EMP.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma sepSep_mono (P1 P2 Q1 Q2 : sepProp) :
  (P1 |~ P2) ->
  (Q1 |~ Q2) ->
  P1 ** Q1 |~ P2 ** Q2.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

Lemma sepExist_intro_alt {A} P (Psi : A -> sepProp) x :
  (P |~ Psi x) ->
  P |~ (Ex z, Psi z).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma sepForall_elim_alt {A} (Phi : A -> sepProp) Q x :
  (Phi x |~ Q) ->
  (All z, Phi z) |~ Q.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma sepWand_pure_elim (p : Prop) (Q : sepProp) :
  p ->
  (@[ p ] -** Q) |~ Q.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma sepSep_assoc' (P Q R : sepProp) :
  (P ** Q) ** R |~ P ** (Q ** R).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma sepExists_sep {A} P (Psi : A -> sepProp) :
  (Ex x, P ** Psi x) |~ P ** (Ex x, Psi x).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma sepSep_exists {A} P (Psi : A -> sepProp) :
  P ** (Ex x, Psi x) |~ (Ex x, P ** Psi x).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma sepSep_pure_intro (p : Prop) (P Q : sepProp) :
  p ->
  (P |~ Q) ->
  P |~ @[ p ] ** Q.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma sepWand_pure_intro (p : Prop) (P Q : sepProp) :
  (p -> P |~ Q) ->
  P |~ @[ p ] -** Q.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma too_many_wands {A} (Phi Psi : A -> sepProp) (P Q R : sepProp) :
  ((Ex x, P -** Phi x ** Psi x) -** Q -** R)
  |~ All x y, Phi x -** @[ x = y ] ** (P -** Psi y) ** Q -** R.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.


(* ########################################################################## *)
(** Interactive proofs of Hoare triples *)
(* ########################################################################## *)

(** Now that we have seen how to prove simple separation logic entailments, let
us continue with proving program Hoare triples. *)

(** Let us start with a very simple function, that computes the maximum of two
natural numbers. *)

Definition max :=
  closure: "x" "y" =>
    if: "x" <: "y" then "y" else "x".

(** Instead of writing [expr] ASTs by hand, we use notations to denote programs
in a more concise way. Notations are defined in [proofmode.v] and enabled by the
[Import language_notation] command at the top of this file. The notations are
designed so that they resemble Coq's notations (and have the same priorities and
associativity), but with colons [:] added to avoid ambiguity. An overview of the
notations is as follows:

  Notation              | Meaning
  ----------------------|-------------
  closure: x => e       | VClosure x e
  fun: x => e           | ELam x e
  recclosure: f x => e  | VRecClosure f x e
  rec: f x => e         | ERec f x e
  let: x := e1 in e2    | ELet x e1 e2
  if: e then e1 else e2 | EIf e e1 e2
  ! e                   | ELoad e
  e1 <- e2              | EStore e1 e2
  e1 +: e2              | EOp AddOp e1 e2
  e1 -: e2              | EOp SubOp e1 e2
  e1 <: e2              | EOp LtOp e1 e2
  e1 <=: e2             | EOp LeOp e1 e2
  e1 =: e2              | EOp EqOp e1 e2

If you use [closure:]/[recclosure:] with multiple variables, the outer one is a
[VClosure]/[VRecClosure], and the inner ones are [ELam]s.

The embeddings [EVal] and [EVar] are declared as coercions, so instead of
[EVar v] and [EVar x] you can just write [v] and [x]. Similarly, application
[EApp] declared as a coercion, so instead of [EApp e1 e2] you can write [e1 e2].

If you want to see the real AST, you can disable notation printing in your IDE
or invoke the [Unset Printing Notations] command.

The [Import hoare_notation] at the top of the file also gives us a nicer
notation for Hoare triples and WPs. *)

(** Now let us write a specification for our [max] function. Since the function
does not access the heap, the precondition is simply [EMP] and the postcondition
states the equality. In the postcondition, we use Coq's [Nat.max] function. *)

Lemma max_hoare n1 n2 :
  [{ EMP }]
    max (VNat n1) (VNat n2)
  [{ vret, @[ vret = VNat (Nat.max n1 n2) ] }].
Proof.
  iIntros "_". (** Introduce and clear the [EMP] precondition *)
  unfold max.
  iApply App_wp. (** Beta reduction *)
  iApply Lam_wp. (** Turn the lambda into a closure *)
  iApply App_wp. (** Beta reduction *)
  iApply Op_wp.  (** Reduction of the operator *)
  destruct (n1 <? n2) eqn:?. (* Perform a case distinction to consider the
  [true] and [false] outcome of the comparison. *)
  - iApply If_true_wp.
    iApply Val_wp.
    iPureIntro. f_equal. lia. (** We are left with a pure goal. We use
    [iPureIntro] to turn it into an ordinary Coq goal, where we use [lia] to
    solve the goal *)
  - iApply If_false_wp.
    iApply Val_wp. iPureIntro. f_equal. lia.
Qed.

(** Instead of having to apply the WP rules for pure expressions ([App_wp],
[Lam_wp], [Op_wp], [If_true_wp], [If_false_wp], etc.) and values ([Val_wp])
manually we use tactics to do that automatically:

- [wp_pure]: perform exactly 1 pure step
- [wp_pure!]: perform as many pure steps as possible (not at least 1)
- [wp_unfold]: unfold a function definition and perform the corresponding beta
  reduction step. The [wp_pure] tactic does not unfold definitions.
- [wp_unfold!]: similar to [wp_unfold], but followed by as many pure steps as
  possible (could also be 0). *)

Lemma max_hoare_wp_pure n1 n2 :
  [{ EMP }]
    max (VNat n1) (VNat n2)
  [{ vret, @[ vret = VNat (Nat.max n1 n2) ] }].
Proof.
  iIntros "_". (** Introduce and clear the [EMP] precondition *)
  Fail wp_pure. (** [wp_pure] never unfolds definitions. *)
  wp_unfold. (** So, we need to do that ourselves. *)
  wp_pure. (** Perform 1 pure step *)
  wp_pure!. (** Repeatedly perform pure steps *) 
  destruct (n1 <? n2) eqn:?. (* Perform a case distinction to consider the
  [true] and [false] outcome of the comparison. *)
  - wp_pure!.
    iPureIntro. f_equal. lia. (** We are left with a pure goal. We use
    [iPureIntro] to turn it into an ordinary Coq goal, where we use [lia] to
    solve the goal *)
  - wp_pure!. iPureIntro. f_equal. lia.
Restart.
  (* A shorter proof. *)
  iIntros "_". wp_unfold!.
  destruct (_ <? _) eqn:?; wp_pure!; eauto with f_equal lia.
Qed.

(** The swap function. We let it return the unit value; there are other choices,
but they require a different specification than we one we consider. *)

Definition swap : val :=
  closure: "x" "y" =>
    let: "tmp" := !"x" in
    "x" <- !"y";;
    "y" <- "tmp";;
    VUnit.

(** The precondition and the postcondition describe the heap before and after.
They express that the swap function indeed swaps. *)

Lemma swap_hoare l1 l2 v1 v2 :
  [{ l1 ~> v1 ** l2 ~> v2 }]
    swap (VRef l1) (VRef l2)
  [{ vret, @[ vret = VUnit ] ** l1 ~> v2 ** l2 ~> v1 }].
Proof.
  iIntros "[Hl1 Hl2]". (** The precondition is a separating conjunction, so we
    use the introduction pattern to eliminate it. *)

  (** We always start with [wp_unfold]. *)
  wp_unfold!.

  (** Now we use the load rule *)
  iApply (Load_wp with "Hl1").
  (** And use [iIntros] to get the [~>] back. *)
  iIntros "Hl1".

  iApply (Load_wp with "Hl2"); iIntros "Hl2".

  (** The store rule is similar to load, but now the [~>] that we give up (using
  the [with] clause] has a different value than the one we get back (i.e.,
  introduce using [iIntros]). *)
  iApply (Store_wp with "Hl1"); iIntros "Hl1".

  iApply (Store_wp with "Hl2"); iIntros "Hl2".
  wp_pure!. iFrame. done.
Restart.
  iIntros "[Hl1 Hl2]". wp_unfold!.
  (* Instead of having to [iApply] the WP rules, there are [wp_] tactics that
  automatically select the [~>] from the context and introduce it back. Similar
  to the other [wp] tactics, you can use the [!] suffix to normalize the goal
  using pure reduction. *)
  wp_load.
  wp_load.
  wp_store!.
  wp_store!.
  by iFrame.
Qed.

(** The rotate right function. To implement it, we swap twice. *)

Definition rotate_r :=
  closure: "x" "y" "z" =>
    swap "y" "z";;
    swap "x" "y".

(** The specification should not be surprising. The key ingredient of the proof
is the reuse of the proof for [swap] (instead of inlining that proof). *)

Lemma rotate_r_hoare l1 l2 l3 v1 v2 v3 :
  [{ l1 ~> v1 ** l2 ~> v2 ** l3 ~> v3 }]
    rotate_r (VRef l1) (VRef l2) (VRef l3)
  [{ vret, @[ vret = VUnit ] ** l1 ~> v3 ** l2 ~> v1 ** l3 ~> v2 }].
Proof.
  iIntros "(Hl1 & Hl2 & Hl3)". wp_unfold!.

  (** Since a Hoare triple expands to a WP, we can [iApply] it. *)
  iApply (swap_hoare with "[Hl2 Hl3]").
  { (** The first goal is to prove the precondition. Using the [with] clause
    above we made sure to give the right hypotheses to the precondition. *)
    iFrame. }
  (** [iApply] automatically uses framing and monotonicity of WP. We have to
  prove that our main goal follows from the postcondition of [swap], for any
  return value [vret] of [swap]. *)
  iIntros "[Hl2 Hl3]".

  (* We use the specification of [swap] again. This time we make the proof a
  bit more concise. Particularly, we use the [$] sign in the [with] clause to
  [iFrame] the hypothesis directly. *)
  iApply (swap_hoare with "[$Hl1 $Hl2]"); iIntros "[Hl1 Hl2]".
  iFrame. done.
Qed.

(** To specify programs that manipulate linked lists, we define the
representation predicate [is_list ll vs], which says that at location [ll] in
our heap, there resides a linked list represented by the Coq list [vs]. *)

Fixpoint is_list (ll : loc) (vs : list val) : sepProp :=
  match vs with
  | [] => ll ~> VInjL VUnit
  | hd :: vs => Ex ll',
     ll ~> VInjR (VPair hd (VRef ll')) **
     is_list ll' vs
  end.

(** Note that this version of [is_list] is a bit different from the one
we saw in the previous lecture. In this week's exercises we define a version
[is_list_nodes] that is similar to the one in the previous lecture, and prove a
correspondence to [ist_list] below. *)

(** A function that sums the list. *)

Definition sum_list :=
  recclosure: "rec" "li" "x" =>
    match: !"li" with
      InjL "_" => VUnit
    | InjR "node" =>
       "x" <- !"x" +: EFst "node";;
       "rec" (ESnd "node") "x"
    end.

(** In the representation predicate and the function we use sums [EInjL]/[VInjL]
and [EInjR]/[VInjR] and pattern matching [match:]. Sums are encoded as tagged
unions using Booleans and products. The exact encoding does not matter so much
(read [proofmode.v] if you are interested), but what what matters most are the
proof rules:

- [Match_InjL_wp] and [Match_InjR_wp]
- [InjL_wp] and [InjR_wp]

These rules are used automatically by [wp_pure], but you can also apply them by
hand. These rules are used often, so make sure to inspect them using the
[About] command. Note that they follow the same pattern as the proof rules
for products ([Fst_wp], [Snd_wp], [Pair_wp]). *)

(** To specify [sum_list] we use [foldr Nat.add 0] to perform the sum at the Coq
level. *)

Lemma sum_list_hoare ll k ns m :
  [{ is_list ll (map VNat ns) ** k ~> VNat m }]
    sum_list (VRef ll) (VRef k)
  [{ vret, @[ vret = VUnit ] ** is_list ll (map VNat ns) **
           k ~> VNat (m + foldr Nat.add 0 ns) }].
Proof.
  iIntros "[Hll Hk]".

  (** The function is recursive on the structure of the list. We perform
  induction on the Coq list [ns] using the the [iInduction] tactic. *)
  iInduction ns as [|n ns] "IH" forall (ll m); simpl.

  (** There are a couple of things to be aware of:

  - Using the [forall] clause, we generalize the induction hypothesis. The
    effect of this clause is similar to doing [revert; induction; intros] in
    ordinary Coq.
  - The induction hypothesis will end up in the separation logic proof context
    and is thus between string quotation marks.
  - The [; simpl] is used to unfold the definition of the representation
    predicate. If you do not do this, your proof contexts look really messy.
    (Rule of thumb: induction is always followed by [simpl].)

  **IMPORTANT** When verifying recursive functions, NEVER use Coq's [unfold]
  tactic, always use [wp_unfold]. The [wp_unfold] tactic makes sure that the
  recursive call is folded back (i.e., you do not see an anonymouse lambda in
  your context). This is similar to the fact that you should use [simpl]
  instead of [unfold] for fixpoint definitions in Coq.

  Furthermore it is important to notice that [wp_unfold] should always be
  performed after the induction, otherwise the induction hypothesis is too
  weak. *)
  - (** Base case (nil) *)
    wp_unfold!. wp_load!.
    (** Make sure our goal matches the hypotheses exactly. Then we finish the
    proof by framing. *)
    rewrite Nat.add_0_r. by iFrame.
  - (** Inductive case (cons) *)
    (** Unpack the representation predicate. As a general rule of thumb, you
    should do that immediately. *)
    iDestruct "Hll" as (ll') "[Hll Hll']".
    wp_unfold!. wp_load!. wp_load!. wp_store!.
    (** Now we have to prove a WP for the recursive call, For which we use the
    induction hypothesis. Since the induction hypothesis is a wand, we need
    to [iApply] it, and use the [with] clause to give the right hypotheses to
    the premises. *)
    iApply ("IH" with "Hll' Hk").
    (** We have to show that the postcondition of the WP in the IH implies our
    goal for any return value [vret]. *)
    iIntros "[? Hk]". iSplit; [done|].
    iSplitR "Hk".
    + eauto with iFrame.
    + rewrite Nat.add_assoc. iFrame.
Qed.

(** A function that frees all nodes of a list. *)

Definition free_list :=
  recclosure: "rec" "li" =>
    match: EFree "li" with
      InjL "_" => VUnit
    | InjR "node" => "rec" (ESnd "node")
    end.

(** The specification is as expected, and the proof follows the same pattern as
the proof of [sum_list_hoare] above. *)

Lemma free_list_hoare ll vs :
  [{ is_list ll vs }]
    free_list (VRef ll)
  [{ vret, @[ vret = VUnit ] }].
Proof.
  iIntros "Hll".
  iInduction vs as [|x vs] "IH" forall (ll); simpl.
  - wp_unfold!. by wp_free!.
  - wp_unfold!. iDestruct "Hll" as (ll') "[Hll Hll']".
    wp_free!. by iApply "IH".
Qed.

(** A function that looks up an element in a list. *)
(** An interesting aspect of this function is that it can fail in case we run
out of bounds. We use [VUnit VUnit] in that case. This expression gets stuck in
our operational semantics (there is no big step rule for it), so we need to
set up our specification to guarantee the absence of out-of-bound errors. *)

Definition list_lookup :=
  recclosure: "rec" "li" "i" =>
    match: !"li" with
      InjL "_" => VUnit VUnit (* out of bounds *)
    | InjR "node" =>
       if: "i" =: VNat 0 then EFst "node" else
       "rec" (ESnd "node") ("i" -: VNat 1)
    end.

Lemma list_lookup_hoare ll vs i w :
  nth_error vs i = Some w -> (* ensures that out of bounds cannot happen *)
  [{ is_list ll vs }]
    list_lookup (VRef ll) (VNat i)
  [{ vret, @[ vret = w ] ** is_list ll vs }].
Proof.
  iIntros (Hi) "Hll".
  iInduction vs as [|v vs] "IH" forall (ll i Hi); simpl.
  { (* The out of bounds case. This is a contradiction with [nth_error]. *)
    by destruct i. }
  iDestruct "Hll" as (ll') "[Hll Hll']".
  wp_unfold!. wp_load!.
  (** We have to prove a WP for [if: VBool (i =? 0) then ..]. We perform a
  case analysis on [i] so that the [=?] becomes [true]/[false], and use
  [wp_pure!] in the two cases. *)
  destruct i as [|i]; simplify_eq; wp_pure!.
  + eauto with iFrame.
  + (** Our induction hypothesis does not match nicely. We use [replace] and
    [lia] to turn our goal into the right shape. *)
    replace (S i - 1) with i by lia.
    iApply ("IH" with "[//] Hll'").
    iIntros "Hll'". eauto with iFrame.
Qed.

(** We proceed by proving a Hoare triple for a function that computes the depth
of a linked binary tree. We first define binary trees [tree] in Coq, and define
a representation predicate [is_tree]. *)

Inductive tree (A : Type) : Type :=
  | leaf : A -> tree A
  | node : tree A -> tree A -> tree A.
Arguments leaf {A} _.
Arguments node {A} _ _.

(** We use [VInjL] for leafs, and [VInjR]/[VPair] for nodes. *)

Fixpoint is_tree (tv : val) (t : tree val) : sepProp :=
  match t with
  | leaf v => @[ tv = VInjL v ]
  | node tl tr =>
     Ex ll lr tvl tvr,
       @[ tv = VInjR (VPair (VRef ll) (VRef lr)) ] **
       ll ~> tvl ** is_tree tvl tl **
       lr ~> tvr ** is_tree tvr tr
  end.

(** Depth on purely functional trees in Coq. *)

Fixpoint depth {A} (t : tree A) : nat :=
  match t with
  | leaf _ => 0
  | node tl tr => S (Nat.max (depth tl) (depth tr))
  end.

(** Depth on linked trees in our language with references. *)

Definition depth_tree :=
  recclosure: "rec" "li" =>
    match: "li" with
      InjL "_" => VNat 0
    | InjR "node" =>
       let: "m1" := "rec" !(EFst "node") in
       let: "m2" := "rec" !(ESnd "node") in
       max "m1" "m2" +: VNat 1
    end.

(** And finally the Hoare triple. Step through it carefully. We use the
specification [max_hoare] of the [max] function that we proved before. *)

Lemma depth_tree_hoare tv t :
  [{ is_tree tv t }]
    depth_tree tv
  [{ vret, @[ vret = VNat (depth t) ] ** is_tree tv t }].
Proof.
  iIntros "Htv".
  iInduction t as [w|tl tr] "IH" forall (tv); simpl.
  - iDestruct "Htv" as %->. by wp_unfold!.
  - iDestruct "Htv" as (ll lr tvl tvr ->) "(Hll & Htl & Hlr & Htr)".
    wp_unfold!. wp_load!.
    iApply ("IH" with "Htl"); iIntros "Htl".
    wp_load!.
    iApply ("IH1" with "Htr"); iIntros "Htr".
    iApply (max_hoare with "[//]"). wp_pure!. iSplitR.
    + iPureIntro. f_equal. lia.
    + eauto 10 with iFrame.
Qed.


(* ########################################################################## *)
(** Exercises *)
(* ########################################################################## *)

(** Prove the Hoare triples below. *)

(** Use [Store_wp] *)
Lemma Store_hoare l v w :
  hoare (l ~> v)
        (EStore (EVal (VRef l)) (EVal w))
        (fun vret => @[ vret = VRef l ] ** l ~> w).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(** Use [Load_wp] *)
Lemma Load_hoare l v :
  hoare (l ~> v)
        (ELoad (EVal (VRef l)))
        (fun vret => @[ vret = v ] ** l ~> v).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Definition rotate_l :=
  closure: "x" "y" "z" =>
    swap "x" "y";;
    swap "y" "z".

Lemma rotate_l_hoare l1 l2 l3 v1 v2 v3 :
  [{ l1 ~> v1 ** l2 ~> v2 ** l3 ~> v3 }]
    rotate_l (VRef l1) (VRef l2) (VRef l3)
  [{ vret, @[ vret = VUnit ] ** l1 ~> v2 ** l2 ~> v3 ** l3 ~> v1 }].
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

Definition clone_list :=
  recclosure: "rec" "li" =>
    match: !"li" with
      InjL "_" => EAlloc (EInjL VUnit)
    | InjR "node" =>
       let: "hd" := EFst "node" in
       let: "li'" := "rec" (ESnd "node") in
       EAlloc (EInjR (EPair "hd" "li'"))
    end.

(** Hint: You need the tactic [wp_alloc l as "Hl"] in the next proof. *)

Lemma clone_list_hoare ll vs :
  [{ is_list ll vs }]
    clone_list (VRef ll)
  [{ vret, Ex ll', @[ vret = VRef ll' ] ** is_list ll vs ** is_list ll' vs }].
Proof. (* FILL IN HERE (9 LOC proof) *) Admitted.

(** We consider two versions to compute the tail of a list.

- Version 1 returns a reference to the tail node.
- Version 2 changes the list reference to refer to the tail node, and returns
  the unit value.

The first version is easier to prove, while the second one is nicer to use
in other programs. *)

Definition list_tail_v1 :=
  closure: "li" =>
    match: !"li" with
      InjL "_" => VUnit VUnit (* out of bounds, our specification prevents this *)
    | InjR "node" => EFree "li";; ESnd "node"
    end.

Lemma list_tail_v1_hoare ll vs :
  vs <> [] ->
  [{ is_list ll vs }]
    list_tail_v1 (VRef ll)
  [{ vret, Ex ll', @[ vret = VRef ll' ] ** is_list ll' (tl vs) }].
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Definition list_tail_v2 :=
  closure: "li" =>
    match: !"li" with
      InjL "_" => VUnit VUnit (* out of bounds *)
    | InjR "node" => "li" <- EFree (ESnd "node");; VUnit
    end.

(** This proof is more complicated than the proof above. In the cons case, you
might need to perform yet another case analysis so you have the right hypotheses
for the [EFree]. *)

Lemma list_tail_v2_hoare ll vs :
  vs <> [] ->
  [{ is_list ll vs }]
    list_tail_v2 (VRef ll)
  [{ vret, @[ vret = VUnit ] ** is_list ll (tl vs) }].
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.

(** The mirror function on trees: first a version on purely functional Coq
binary trees, then a version on linked binary trees. *)

Fixpoint mirror {A} (t : tree A) : tree A :=
  match t with
  | leaf x => leaf x
  | node tl tr => node (mirror tr) (mirror tl)
  end.

Definition mirror_tree :=
  recclosure: "rec" "t" =>
    match: "t" with
      InjL "x" => VUnit
    | InjR "node" =>
       let: "l" := EFst "node" in
       let: "r" := ESnd "node" in
       "rec" !"l";;
       "rec" !"r";;
       swap "l" "r"
    end.

(** Make sure to not unfold [swap] in the verification. Rather, use the
specification [swap_hoare]. *)

Lemma mirror_tree_hoare tv t :
  [{ is_tree tv t }]
    mirror_tree tv
  [{ vret, @[ vret = VUnit ] ** is_tree tv (mirror t) }].
Proof. (* FILL IN HERE (8 LOC proof) *) Admitted.

(** An alternative version of the list representation predicate. This version
is the same as the one we considered in the previous lecture. It avoids a
reference indirection at the beginning. *)

Fixpoint is_list_node (lv : val) (vs : list val) : sepProp :=
  match vs with
  | [] => @[ lv = VInjL VUnit ]
  | hd :: vs => Ex ll' lv',
     @[ lv = VInjR (VPair hd (VRef ll')) ] **
     ll' ~> lv' **
     is_list_node lv' vs
  end.

(** Prove correspondences in both directions. *)

Lemma is_list_make ll lv vs :
  ll ~> lv ** is_list_node lv vs |~ is_list ll vs.
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.

Lemma is_list_unmake ll vs :
  is_list ll vs |~ Ex lv, ll ~> lv ** is_list_node lv vs.
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.

(** Yet another version of the list representation predicate where all values
are "boxed". That is, there is a reference indirection for each value (see
[hdl ~> hd]). *)

Fixpoint is_ptr_list (ll : loc) (vs : list val) : sepProp :=
  match vs with
  | [] => ll ~> VInjL VUnit
  | hd :: vs => Ex hdl ll',
     ll ~> VInjR (VPair (VRef hdl) (VRef ll')) **
     hdl ~> hd **
     is_ptr_list ll' vs
  end.

Fixpoint replace {A} (xs : list A) (i : nat) (y : A) : list A :=
  match xs with
  | [] => []
  | x :: xs =>
     match i with
     | 0 => y :: xs
     | S i => x :: replace xs i y
     end
  end.

(** With the [list_lookup] we get a reference to the element [w] at position
[i] in the list. We can use this reference to modify the element, which updates
the list. Prove the following specification. The postcondition might look
daunting, so first try to figure out the intuitive semantics. *)

Lemma ptr_list_lookup_hoare ll vs i w :
  nth_error vs i = Some w ->
  [{ is_ptr_list ll vs }]
    list_lookup (VRef ll) (VNat i)
  [{ vret, Ex l,
      @[ vret = VRef l ] **
      l ~> w **
      (All w', l ~> w' -** is_ptr_list ll (replace vs i w'))
  }].
Proof. (* FILL IN HERE (14 LOC proof) *) Admitted.

(** When using [iApply] on a WP, the rule [wp_apply] is implicitly used. This
rule combines [wp_frame], [wp_mono], and [wp_ctx]. Prove this rule. *)

(** IMPORTANT: You should not use the Proof Mode tactics, but rather perform
a manual proof. You need to use [sepEntails_trans] to chain [wp_frame],
[wp_mono] and [wp_ctx] together. Additionally, you might need some lemmas about
separating conjunction, wand, and forall from week 9. *)

Lemma wp_apply Phi Psi k e :
  ctx k ->
  wp e Phi ** (All v, Phi v -** wp (k (EVal v)) Psi) |~ wp (k e) Psi.
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.


(* ########################################################################## *)
(** Challenge exercise: In-place list reversal *)
(* ########################################################################## *)

(** The following functions perform an in-place list reversal. That means, it
reverses the list without performing any allocation/free. *)

Definition rev_app_list :=
  recclosure: "rec" "li" "acc" =>
    match: "li" with
      InjL "_" => "acc"
    | InjR "node" =>
       let: "li'" := !(ESnd "node") in
       ESnd "node" <- "acc";;
       "rec" "li'" "li"
    end.

Definition rev_list :=
  closure: "li" =>
    let: "acc" := EInjL VUnit in
    "li" <- rev_app_list (!"li") "acc";;
    VUnit.

(** You should prove that [rev_list] indeed reverses the list. You need to
define a suitable helping lemma for [rev_app_list]. Finding the right lemma
statement for the helping lemma is the hardest part. Once you have figured out
that, the proof will be straightforward. *)
Lemma rev_list_hoare ll vs :
  [{ is_list ll vs }]
    rev_list (VRef ll)
  [{ vret, @[ vret = VUnit ] ** is_list ll (rev vs) }].
Proof. (* FILL IN HERE (15 LOC helpers and 6 LOC proof) *) Admitted.


(* ########################################################################## *)
(** Challenge exercise: The map function on linked lists *)
(* ########################################################################## *)

(** We define a function [map_list] that maps a function [f] over a linked
list [li]. *)

Definition map_list :=
  recclosure: "rec" "li" "f" =>
    match: !"li" with
      InjL "_" => VUnit
    | InjR "node" =>
       let: "x" := "f" (EFst "node") in
       "li" <- EInjR (EPair "x" (ESnd "node"));;
       "rec" (ESnd "node") "f"
    end.

(** Prove the following specification for [map_list]. This specification
contains a Hoare triple for [f] in its premise. This Hoare triple relates the
language-level [f] to a pure Coq-level function [f_coq] and pure Coq-level
precondition [p_coq]. *)

Lemma map_list_hoare (f : val) (p_coq : val -> Prop) (f_coq : val -> val) ll vs :
  (forall w : val,
    [{ @[ p_coq w ] }] f w [{ vret, @[ vret = f_coq w ] }]) ->
  Forall p_coq vs ->
  [{ is_list ll vs }]
    map_list (VRef ll) f
  [{ vret, @[ vret = VUnit ] ** is_list ll (map f_coq vs) }].
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.

(** Use [map_list_hoare] to prove a specification for [inc_list]. The
difficult part is finding a the right definition of [f_coq] and [p_coq]. You
might have to prove some properties about Coq's [map] and Coq's [Forall]. *)

Definition inc_list :=
  closure: "li" "n" =>
    let: "f" := (fun: "m" => "n" +: "m") in
    map_list "li" "f".

Lemma inc_list_hoare ll ns n :
  [{ is_list ll (map VNat ns) }]
    inc_list (VRef ll) (VNat n)
  [{ vret, @[ vret = VUnit] ** is_list ll (map VNat (map (Nat.add n) ns)) }].
Proof. (* FILL IN HERE (9 LOC proof) *) Admitted.

(** Using the function [map_list] we can compute the length. We do this by
creating a reference, which is captured by the closure we give to [map_list]. *)

Definition length_list :=
  closure: "li" =>
    let: "r" := EAlloc (VNat 0) in
    let: "f" := (fun: "x" => "r" <- !"r" +: VNat 1;; "x") in
    map_list "li" "f";;
    EFree "r".

(** The specification [map_list_hoare] above is too weak to prove correctness
of [length_list]. The precondition of [f] cannot get access to the [~>] for the
reference [r]. You need to come up with a stronger specification
[map_list_hoare_strong] for [map_list] so that you can derive both
[length_list_hoare] and [map_list_hoare] from it. *)

Lemma length_list_hoare ll vs :
  [{ is_list ll vs }]
    length_list (VRef ll)
  [{ vret, @[ vret = VNat (length vs) ] ** is_list ll vs }].
Proof. (* FILL IN HERE (18 LOC helpers and 6 LOC proof) *) Admitted.

Lemma map_list_hoare' (f : val) (p_coq : val -> Prop) (f_coq : val -> val) ll vs :
  (forall w : val,
    [{ @[ p_coq w ] }] f w [{ vret, @[ vret = f_coq w ] }]) ->
  Forall p_coq vs ->
  [{ is_list ll vs }]
    map_list (VRef ll) f
  [{ vret, @[ vret = VUnit ] ** is_list ll (map f_coq vs) }].
Proof. (* FILL IN HERE (11 LOC proof) *) Admitted.
