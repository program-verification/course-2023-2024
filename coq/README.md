Getting started
---------------

Make sure to download our [**Coq cheat sheet**](coq/cheatsheet.pdf) with an overview of the tactics and commands that are used in this course.

---------------

For this course we will use **Coq 8.18.0**.
Coq proofs are usually developed interactively in an IDE like CoqIDE, Emacs, or Visual Studio Code.

The exercises are literate Coq files (`weekx.v`) in the [coq/](coq/) directory of this repository.
Open the exercises for that week in your editor and follow the instructions in the text.

In order to open the exercises, you need to compile the dependencies.
For this, you can type `make` in your shell.
Alternatively, if you have trouble with the `Makefile` or are on a platform where `make` is not easily available, open the [library.v](library.v) file in CoqIDE and click "Compile > Compile buffer".
This compiles `library.v` to `library.vo`, which is required by the exercise files.

To work on the exercises, make sure to open your IDE in the folder containing the `_CoqProject` file (i.e., the `coq/` subdirectory).
Otherwise it will not be able to import the library file.

So, to work on the exercises do something like:

```
$ cd coq
$ make -jN       # With N being the number of cores
$ coqide weekx.v # Replace coqide with your editor of choice
```

At certain points in the file you are expected to fill in definitions and proofs.
These points are marked using `(* FILL IN HERE *) Admitted`; you are supposed to replace this code with your answer.

If you want Coq to accept a missing definition or proof you may use the `Admitted` command or the `admit` tactic to do so.

## Installation

Follow the steps below to install Coq on your machine and to get started with the Coq exercises.

### Linux

You can install Coq and CoqIDE using your favorite package manager, or install [opam](https://opam.ocaml.org/) (the OCaml package manager), and use it to install Coq:

```
$ opam pin add coq 8.18.0
$ opam install coqide
```

### OSX

You can install Coq and CoqIDE using brew:

```
$ brew install coq
$ brew install coqide
```

Since OSX has an old version of `make`, you may want to install a newer version:

```
$ brew install make
$ brew install ocaml-findlib # called to `make` Coq files
```

Instead of using brew, you can download the Coq installer from the release page on [GitHub](https://github.com/coq/coq/releases/tag/V8.18.0).

Or install opam using brew, then follow the Linux instructions for opam.

### Windows

You can download the Coq installer from the release page on [GitHub](https://github.com/coq/coq/releases/tag/V8.18.0).

To use the `Makefile` you need to install `make`.
For the first weeks, where there is just a single dependency file, you could also open the [library.v](library.v) file in CoqIDE and click "Compile > Compile buffer".

## Other editors

- **Emacs** If you are an Emacs user, you may prefer [Proof General](https://proofgeneral.github.io/) (also known as the Coq layer in Spacemacs).
- **VSCode** If you are a Visual Studio Code user you may prefer using [the VSCoq plugin](https://marketplace.visualstudio.com/items?itemName=maximedenes.vscoq).

All these editors allow you load Coq files line-by-line and will show you the proof state at the boundary.
