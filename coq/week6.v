(** * Lecture 6: Type safety for languages with mutable references *)
From pv Require Export library.

(* REMINDER:

          #####################################################
          ###  PLEASE DO NOT DISTRIBUTE SOLUTIONS PUBLICLY  ###
          #####################################################

*)

(* ########################################################################## *)
(** * Introduction *)
(* ########################################################################## *)

(** Mutable state (in the form of references or pointers) is available in most
mainstream programming languages. In this lecture we will prove type safety for
a language with mutable state. This language extends our lambda calculus from
week 5 with three constructs:

1. [EAlloc v]: Return a mutable reference that initially contains value [v].
2. [ELoad r]: Read the value from reference [r].
3. [EStore r v]: Store value [v] in reference [r].

While we used substitution to give a semantics to *immutable* let/lambda-bound
variables in previous weeks, that approach does not work for *mutable*
references. Instead, we represent the program state as a configuration [(e,h)]
of an expression [e] and a heap [h]. The small-step semantics becomes of the
shape [(e,h) ~> (e',h')]. The heap [h : heap] is a a finite mapping
[heap := natmap val] from locations to values. If [lookup h l = None] then the
location [l] is not in use, and if [lookup h l = Some v] then the location [l]
contains value [v].

Our goal is to prove "type safety" of this language. Intuitively, the type
system ensures that no illegal operations are performed (like [0 + true]), but
also that no memory locations are used that have not been allocated, and that
memory locations are used with consistent types. We model this by letting the
programs with illegal memory operations get stuck.

Locations [l] are embedded into the program syntax using the value [VRef l].
Closed programs are not supposed to contain [VRef l] literals in the source
code, these should only enter into the program when the operational semantics
executees an [EAlloc] instruction. Thus our *static typing system* [Γ ⊢ e : A]
only handles [EAlloc], [EStore], [ELoad] and the other language constructs, but
does not have any typing rule for [VRef].

To prove type safety using the method of progress and preservation, we need to
make sure that the type of a [VRef l] is consistent with the type of the value
stored at location [l] in the heap [h]. To do so, we introduce a "heap typing"
[Σ : heap_ty] with [heap_ty := natmap ty] that tells us the types of all the
values in the heap. That is, if [lookup h l = Some v] then [lookup Σ l = Some A],
and the value [v] has type [A].

For expressions, we introduce a *run-time type system* [Σ; Γ ⊢ e : A] that *does*
have a typing rule for [VRef]. This run-time type system not only has a variable
context [Γ : stringmap ty], but depends on the heap context [Σ : heap_ty]. We
then have the following typing rule for [VRef]:

   lookup Σ l = Some A
  ---------------------
    Σ;Γ ⊢ VRef l : A

In all other typing rules, the context [Σ] is simply passed through to the sub-
expressions.

All of these ingredients allow us to prove:

- If a program is statically typed, it is run-time typed.
- Progress and preservation for run-time typing.

Together this gives type safety: (statically) well-typed programs cannot go
wrong. *)


(* ########################################################################## *)
(** * Call-by-value lambda calculus with references *)
(* ########################################################################## *)

(** We extend our call-by-value lambda calculus from week 5:

  Values:        v ::= ... | ref l
  Expressions:   e ::= ... | alloc e | load e | store e1 e2

This is done in Coq as follows: *)

Inductive bin_op :=
  | AddOp
  | SubOp
  | LeOp
  | LtOp
  | EqOp.

Inductive expr :=
  | EVal : val -> expr
  | EVar : string -> expr
  | ELam : string -> expr -> expr
  | EApp : expr -> expr -> expr
  | EOp : bin_op -> expr -> expr -> expr
  | EIf : expr -> expr -> expr -> expr
  | EAlloc : expr -> expr (* NEW *)
  | ELoad : expr -> expr (* NEW *)
  | EStore : expr -> expr -> expr (* NEW *)
with val :=
  | VUnit : val
  | VBool : bool -> val
  | VNat : nat -> val
  | VClosure : string -> expr -> val
  | VRef : nat -> val (* NEW *).

(** Substitution is mostly the same. It just propagates through the new
constructs for references. *)

Fixpoint subst (x : string) (w : val) (e : expr) : expr :=
  match e with
  | EVal _ => e
  | EVar y => if String.eq_dec y x then EVal w else EVar y
  | ELam y e => if String.eq_dec y x then ELam y e else ELam y (subst x w e)
  | EApp e1 e2 => EApp (subst x w e1) (subst x w e2)
  | EOp op e1 e2 => EOp op (subst x w e1) (subst x w e2)
  | EIf e1 e2 e3 => EIf (subst x w e1) (subst x w e2) (subst x w e3)
  | EAlloc e => EAlloc (subst x w e) (* NEW *)
  | ELoad e => ELoad (subst x w e) (* NEW *)
  | EStore e1 e2 => EStore (subst x w e1) (subst x w e2) (* NEW *)
  end.

(** We allow comparison of locations. *)

Definition eval_bin_op (op : bin_op) (v1 v2 : val) : option val :=
  match op, v1, v2 with
  | AddOp, VNat n1, VNat n2 => Some (VNat (n1 + n2))
  | SubOp, VNat n1, VNat n2 => Some (VNat (n1 - n2))
  | LeOp, VNat n1, VNat n2 => Some (VBool (Nat.leb n1 n2))
  | LtOp, VNat n1, VNat n2 => Some (VBool (Nat.ltb n1 n2))
  | EqOp, VUnit, VUnit => Some (VBool true)
  | EqOp, VNat n1, VNat n2 => Some (VBool (Nat.eqb n1 n2))
  | EqOp, VBool n1, VBool n2 => Some (VBool (Bool.eqb n1 n2))
  | EqOp, VRef l1, VRef l2 => Some (VBool (Nat.eqb l1 l2)) (* NEW *)
  | _, _, _ => None
  end.

(** We build up the [step] relation in three layers:

1. The [pure_step : expr -> expr -> Prop] relations that contains all the rules
   for the pure operations such as function application and operators. This
   relation is similar to the [head_step] relation from previous weeks.
2. The [head_step : expr -> head -> expr -> heap -> Prop] relation that contains
   the rules for all constructs in head position. It has the rules for the
   operations on references, and a rule for pure steps.
3. The [step : (expr * heap) -> (expr * heap)] relation that is the final
   operational semantics, where the steps do not necessarily have to be in head
   position.

To handle expressions not in head position, we used "congruence rules" in week 4
and 5. Two examples of congruence rules are:

           step e2 e2'                              step e1 e1'
  -------------------------------    ------------------------------------------
  step (EApp e1 e2) (EApp e1 e2')    step (EApp e1 (EVal v)) (EApp e1' (EVal v))

These rules make sure that arguments of an application are evaluated in
right-to-left order (before the first argument can be reduced, the second one
needs to be a value). We have similar rules for all other constructs, but these
congruence rules all have the following form:

      step e e'
  -----------------
  step (k e) (k e')

In the example above, [k := EApp e1] and [k x := EApp x (EVal v)], respectively.
This week, we are going to write down the operational semantics more compactly,
by having the *one* congruence rule for an generic context [k]. We separately
specify the set of "evaluation contexts" [k] that we allow using the inductive
predicate [ctx k]. For a pure language, the generic congruence rule would be:

  ctx k      head_step e e'
  -------------------------
     step (k e) (k e')

However, because our language also has heaps, our congruence rule will be:

  ctx k      head_step e h e' h'
  ------------------------------
    step (k e, h) (k e', h')

Factoring out the congruence rules like this also allows us to factorize some
lemmas, see e.g., [replacement].

Note that in the presence of heaps, the order of evaluation (left-to-right or
right-to-left) really matters. Try to write down a program for which this
difference becomes evident. *)

Inductive pure_step : expr -> expr -> Prop :=
  | Lam_pure_step x e :
     pure_step (ELam x e) (EVal (VClosure x e))
  | App_pure_step y e1 v2 :
     pure_step (EApp (EVal (VClosure y e1)) (EVal v2)) (subst y v2 e1)
  | Op_pure_step op v1 v2 v :
     eval_bin_op op v1 v2 = Some v ->
     pure_step (EOp op (EVal v1) (EVal v2)) (EVal v)
  | If_pure_step_true e2 e3 :
     pure_step (EIf (EVal (VBool true)) e2 e3) e2
  | If_pure_step_false e2 e3 :
     pure_step (EIf (EVal (VBool false)) e2 e3) e3.

Notation heap := (natmap val).

(** Important: A load and store can only step when the location exists in the
heap. *)
Inductive head_step : expr -> heap -> expr -> heap -> Prop :=
  | do_pure_step e e' h :
     pure_step e e' ->
     head_step e h e' h
  | EAlloc_step v h :
     head_step
       (EAlloc (EVal v)) h
       (EVal (VRef (NatMap.fresh h))) (NatMap.insert (NatMap.fresh h) v h)
  | ELoad_step v h l :
     NatMap.lookup h l = Some v ->
     head_step
       (ELoad (EVal (VRef l))) h
       (EVal v) h
  | EStore_step v h l :
     NatMap.lookup h l <> None ->
     head_step
       (EStore (EVal (VRef l)) (EVal v)) h
       (EVal (VRef l)) (NatMap.insert l v h).

(** Evaluation contexts are functions [k : expr -> expr] that wrap an expression
[e] in some context [k] to get [k e]. The stepping rule will later roughly say
that if [e] head steps to [e'], then [k e] steps to [k e'] for evaluation
contexts [k]. *)

Inductive ctx : (expr -> expr) -> Prop :=
  | App_r_ctx e : ctx (EApp e)
  | App_l_ctx v : ctx (fun x => EApp x (EVal v))
  | Op_r_ctx op e : ctx (EOp op e)
  | Op_l_ctx op v : ctx (fun x => EOp op x (EVal v))
  | If_ctx e2 e3 : ctx (fun x => EIf x e2 e3)
  | Alloc_ctx : ctx EAlloc
  | Load_ctx : ctx ELoad
  | Store_ctx_r e : ctx (EStore e)
  | Store_ctx_l v : ctx (fun x => EStore x (EVal v))
  | Id_ctx : ctx (fun x => x)
  | Compose_ctx k1 k2 : ctx k1 -> ctx k2 -> ctx (fun x => k1 (k2 x)).

(** A configuration is a pair [(e,h)] of an expression [e] and a heap [h]. *)

Definition cfg : Type := expr * heap.

Inductive step : cfg -> cfg -> Prop :=
  | do_step k e e' h h' :
     ctx k ->
     head_step e h e' h' ->
     step (k e, h) (k e', h').

Inductive steps : cfg -> cfg -> Prop :=
  | steps_refl s :
     steps s s
  | steps_step s1 s2 s3 :
     step s1 s2 ->
     steps s2 s3 ->
     steps s1 s3.


(* ########################################################################## *)
(** * Type system *)
(* ########################################################################## *)

(** We turn to defining a type system for our language. As a first step we
need to define an inductive type representing the syntax of types. Our types
are similar to those in week 5, but we add a type [TRef A] for the type of
mutable references pointing to values of type [A]. *)

Inductive ty :=
  | TUnit : ty
  | TNat : ty
  | TBool : ty
  | TFun : ty -> ty -> ty
  | TRef : ty -> ty (* NEW *).

(** Next we define the type signatures of our binary operations. This is similar
to week 5, but we add a rule for comparing references. *)

Inductive bin_op_typed : bin_op -> ty -> ty -> ty -> Prop :=
  | AddOp_typed :
     bin_op_typed AddOp TNat TNat TNat
  | SubOp_typed :
     bin_op_typed SubOp TNat TNat TNat
  | LeOp_typed :
     bin_op_typed LeOp TNat TNat TBool
  | LtOp_typed :
     bin_op_typed LtOp TNat TNat TBool
  | EqOp_Unit_typed :
     bin_op_typed EqOp TUnit TUnit TBool
  | EqOp_Nat_typed :
     bin_op_typed EqOp TNat TNat TBool
  | EqOp_Bool_typed :
     bin_op_typed EqOp TBool TBool TBool
  | EqOp_Ref_typed A :
     bin_op_typed EqOp (TRef A) (TRef A) TBool (* NEW *).

(** The static typing judgment [Γ ⊢ e : A]. The (new) rules for references are:

       Γ ⊢ e : A          Γ ⊢ e : ref A       Γ ⊢ e1 : ref A     Γ ⊢ e2 : A
  -------------------    ----------------    -------------------------------
  Γ ⊢ alloc e : ref A     Γ ⊢ load e : A         Γ ⊢ store e1 e2 : unit

As described in the introduction of this lecture, the static typing judgment
does not include a typing rule for references [VRef]. References [VRef] should
never occur in source programs, but will only appear at run-time as a
consequence of evaluating an allocation [EAlloc]. (Similarly, closures
[VClosure] should never appear in source programs, they should only occur at
run-time as a consequence of evaluating a lambda [ELam].) *)

Inductive typed : stringmap ty -> expr -> ty -> Prop :=
  | Unit_typed Gamma :
     typed Gamma (EVal VUnit) TUnit
  | Bool_typed Gamma b :
     typed Gamma (EVal (VBool b)) TBool
  | Nat_typed Gamma b :
     typed Gamma (EVal (VNat b)) TNat
  | Var_typed Gamma x A :
     StringMap.lookup Gamma x = Some A ->
     typed Gamma (EVar x) A
  | Lam_typed Gamma x e A B :
     typed (StringMap.insert x A Gamma) e B ->
     typed Gamma (ELam x e) (TFun A B)
  | App_typed Gamma e1 e2 A B :
     typed Gamma e1 (TFun A B) ->
     typed Gamma e2 A ->
     typed Gamma (EApp e1 e2) B
  | Op_typed Gamma op e1 e2 A1 A2 B :
     bin_op_typed op A1 A2 B ->
     typed Gamma e1 A1 ->
     typed Gamma e2 A2 ->
     typed Gamma (EOp op e1 e2) B
  | If_typed Gamma e1 e2 e3 B :
     typed Gamma e1 TBool ->
     typed Gamma e2 B ->
     typed Gamma e3 B ->
     typed Gamma (EIf e1 e2 e3) B
  | Alloc_typed Gamma e A : (* NEW *)
     typed Gamma e A ->
     typed Gamma (EAlloc e) (TRef A)
  | Load_typed Gamma e A : (* NEW *)
     typed Gamma e (TRef A) ->
     typed Gamma (ELoad e) A
  | Store_typed Gamma e1 e2 A : (* NEW *)
     typed Gamma e1 (TRef A) ->
     typed Gamma e2 A ->
     typed Gamma (EStore e1 e2) (TRef A).

(** The run-time typing judgment [Γ;Σ ⊢ e : A]. As explained in the introduction
of this lecture, this typing judgment contains a heap typing [Σ : heap_ty] that
maps locations (that occur in [e]) to their types. The key typing rule is:

     Σ(l) = Some A
  --------------------
   Σ ⊢ VRef l : ref A

This typing judgment is defined mutually for expressions and (closed) values. *)

Notation heap_ty := (natmap ty).

Inductive rtyped : heap_ty -> stringmap ty -> expr -> ty -> Prop :=
  | Val_rtyped Sigma Gamma v A :
     val_rtyped Sigma v A ->
     rtyped Sigma Gamma (EVal v) A
  | Var_rtyped Sigma Gamma x A :
     StringMap.lookup Gamma x = Some A ->
     rtyped Sigma Gamma (EVar x) A
  | Lam_rtyped Sigma Gamma x e A B :
     rtyped Sigma (StringMap.insert x A Gamma) e B ->
     rtyped Sigma Gamma (ELam x e) (TFun A B)
  | App_rtyped Sigma Gamma e1 e2 A B :
     rtyped Sigma Gamma e1 (TFun A B) ->
     rtyped Sigma Gamma e2 A ->
     rtyped Sigma Gamma (EApp e1 e2) B
  | Op_rtyped Sigma Gamma op e1 e2 A1 A2 B :
     bin_op_typed op A1 A2 B ->
     rtyped Sigma Gamma e1 A1 ->
     rtyped Sigma Gamma e2 A2 ->
     rtyped Sigma Gamma (EOp op e1 e2) B
  | If_rtyped Sigma Gamma e1 e2 e3 B :
     rtyped Sigma Gamma e1 TBool ->
     rtyped Sigma Gamma e2 B ->
     rtyped Sigma Gamma e3 B ->
     rtyped Sigma Gamma (EIf e1 e2 e3) B
  | Alloc_rtyped Sigma Gamma e A :
     rtyped Sigma Gamma e A ->
     rtyped Sigma Gamma (EAlloc e) (TRef A)
  | Load_rtyped Sigma Gamma e A :
     rtyped Sigma Gamma e (TRef A) ->
     rtyped Sigma Gamma (ELoad e) A
  | Store_rtyped Sigma Gamma e1 e2 A :
     rtyped Sigma Gamma e1 (TRef A) ->
     rtyped Sigma Gamma e2 A ->
     rtyped Sigma Gamma (EStore e1 e2) (TRef A)
with val_rtyped : heap_ty -> val -> ty -> Prop :=
  | Unit_val_rtyped Sigma :
     val_rtyped Sigma VUnit TUnit
  | Bool_val_rtyped Sigma b :
     val_rtyped Sigma (VBool b) TBool
  | Nat_val_rtyped Sigma n :
     val_rtyped Sigma (VNat n) TNat
  | Closure_val_rtyped Sigma x e A B :
     rtyped Sigma (StringMap.singleton x A) e B ->
     val_rtyped Sigma (VClosure x e) (TFun A B)
  | Ref_val_rtyped Sigma l A :
     NatMap.lookup Sigma l = Some A ->
     val_rtyped Sigma (VRef l) (TRef A) (* KEY RULE *).

(** Well-typedness of a heap [h] with respect to a heap typing [Σ]. It says that
for every location/type in [Σ] there is a location/well-typed value in [h]. *)

Definition heap_typed (Sigma : heap_ty) (h : heap) : Prop :=
  forall l A,
    NatMap.lookup Sigma l = Some A ->
    exists v, NatMap.lookup h l = Some v /\ val_rtyped Sigma v A.

(** Well-typedness of configurations. *)

Definition cfg_typed (s : cfg) (A : ty) : Prop :=
  exists Sigma,
    rtyped Sigma StringMap.empty (fst s) A /\
    heap_typed Sigma (snd s).


(* ########################################################################## *)
(** * Type safety via progress and preservation *)
(* ########################################################################## *)

(** Our goal is to prove "safety" of our language with references, that means
all well-typed closed expressions are safe. We generalize the definition of
safety in week 5 from expressions to configurations. *)

Definition final (s : cfg) := exists v, fst s = EVal v.
Definition can_step (s : cfg) := exists s', step s s'.
Definition safe (s : cfg) :=
  forall s',
    steps s s' ->
    final s' \/ can_step s'.

(** The formal safety theorem ([safety] at the bottom of this file) states that
[typed StringMap.empty e A] implies [safe (e,NatMap.empty)]. The key lemmas are:

- [initialization Gamma]:
    [typed StringMap.empty e A] implies [cfg_typed (e,NatMap.empty) A].
- [preservation]
    If [step s s'], then [cfg_typed s A] implies [cfg_typed s' A].
- [progress]
    [cfg_typed s A] implies [final s] or [can_step s].

Safety then follows by induction over the multi-step relation. *)

(** The [inv_rtyped] tactic can be used to repeatedly perform inversion on
[typed] and [typed_val] hypotheses where the argument is a constructor. *)

Ltac inv_rtyped :=
  repeat match goal with
  | _ => progress simplify_eq
  | H : rtyped _ _ (EVal _) _ |- _ => inv H
  | H : rtyped _ _ (EVar _) _ |- _ => inv H
  | H : rtyped _ _ (ELam _ _) _ |- _ => inv H
  | H : rtyped _ _ (EApp _ _) _ |- _ => inv H
  | H : rtyped _ _ (EOp _ _ _) _ |- _ => inv H
  | H : rtyped _ _ (EIf _ _ _) _ |- _ => inv H
  | H : rtyped _ _ (EAlloc _) _ |- _ => inv H
  | H : rtyped _ _ (ELoad _) _ |- _ => inv H
  | H : rtyped _ _ (EStore _ _) _ |- _ => inv H
  | H : val_rtyped _ _ TBool |- _ => inv H
  | H : val_rtyped _ _ TNat |- _ => inv H
  | H : val_rtyped _ _ (TFun _ _) |- _ => inv H
  | H : val_rtyped _ _ (TRef _) |- _ => inv H
  | H : val_rtyped _ (VBool _) _ |- _ => inv H
  | H : val_rtyped _ (VNat _) _ |- _ => inv H
  | H : val_rtyped _ (VClosure _ _) _ |- _ => inv H
  | H : val_rtyped _ (VRef _) _ |- _ => inv H
  end.

(** The following judgment expresses that [k : expr -> expr] is a context that
sends expression of type [A] to expressions of type [B], and the extra
expressions introduced by [k] may contain [VRef] from [Sigma]. *)

Definition ctx_typed (Sigma : heap_ty) (k : expr -> expr) (A B : ty) :=
  forall e' Sigma',
    NatMap.incl Sigma Sigma' ->
    rtyped Sigma' StringMap.empty e' B ->
    rtyped Sigma' StringMap.empty (k e') A.

(** The *weakening lemma* says that when an expression is typed in a heap
typing [Σ], it is also typed in any extension [Σ'] of that heap typing. This
means, typing is preserved under the allocation of new locations. This lemma
has to be proved mutually inductively (for the [Lemma lemma1... with lemma2...]
syntax, see week 5).*)

Lemma rtyped_weakening Sigma Sigma' Gamma e A :
  rtyped Sigma Gamma e A ->
  NatMap.incl Sigma Sigma' ->
  rtyped Sigma' Gamma e A
with val_rtyped_weakening Sigma Sigma' v A :
  val_rtyped Sigma v A ->
  NatMap.incl Sigma Sigma' ->
  val_rtyped Sigma' v A.
Proof.
  (** The [econstructor] tactic [eapply]s the matching constructor. *)
  - destruct 1; econstructor; eauto using StringMap.insert_mono.
  - destruct 1; econstructor;
      eauto using StringMap.insert_mono, StringMap.incl_refl.
Qed.

(** The replacement lemma allows us to split a typing judgment [Σ;Γ ⊢ k e : B]
into [Σ;Γ ⊢ e : A] for some [A], and a context typing [ctx_typed Σ k A B]. *)

Lemma replacement Sigma k e A :
  ctx k ->
  rtyped Sigma StringMap.empty (k e) A <->
    exists B, rtyped Sigma StringMap.empty e B /\
              ctx_typed Sigma k A B.
Proof.
  intros Hctx. unfold ctx_typed.
  split; [|intros [B [He Hk]]; eauto using NatMap.incl_refl].
  revert A e. induction Hctx; intros A e0 Ht; inv_rtyped;
    eauto 10 using rtyped, rtyped_weakening, StringMap.incl_refl, val_rtyped.
  eapply IHHctx1 in Ht as [B1 [H1 H2]].
  eapply IHHctx2 in H1 as [B2 [H1' H2']].
  eauto using rtyped.
Qed.

(* The substitution lemma. This proof is similar to the one of week 5. *)

Lemma rtyped_subst Sigma Gamma x e v A B :
  val_rtyped Sigma v A ->
  rtyped Sigma (StringMap.insert x A Gamma) e B ->
  rtyped Sigma Gamma (subst x v e) B.
Proof.
  intros Hv He.
  remember (StringMap.insert x A Gamma) as Gamma' eqn:HGamma'.
  revert Gamma HGamma'.
  induction He; intros ? ->; simpl; eauto using rtyped.
  - (** Var case *)
    rewrite StringMap.lookup_insert in H.
    destruct (String.eq_dec _ _); simplify_eq.
    + eauto using rtyped_weakening, StringMap.incl_empty_l, rtyped.
    + by apply Var_rtyped.
  - (** Lam case *)
    destruct (String.eq_dec _ _); simplify_eq.
    + rewrite StringMap.insert_insert in He.
      destruct (String.eq_dec _ _); [|done].
      by apply Lam_rtyped.
    + apply Lam_rtyped. apply IHHe; [done|]. StringMap.map_solver.
Qed.

Lemma typed_rtyped Gamma e A :
  typed Gamma e A ->
  rtyped NatMap.empty Gamma e A.
Proof.
  induction 1; eauto using rtyped, val_rtyped.
Qed.

Lemma heap_typed_empty :
  heap_typed NatMap.empty NatMap.empty.
Proof.
  intros ??. rewrite NatMap.lookup_empty. congruence.
Qed.


(* ########################################################################## *)
(** * Exercise: Complete the progress and preservation proof *)
(* ########################################################################## *)

(** Prove that the initial configuration is well typed. *)

Lemma initialization e A :
  typed StringMap.empty e A ->
  cfg_typed (e,NatMap.empty) A.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Prove that binary operators preserve typing. This proof can be copied from
week 5 (you might need some small adjustments if you did not automate your
proof using [eauto]). *)

Lemma bin_op_preservation op v1 v2 v A1 A2 B Sigma :
  bin_op_typed op A1 A2 B ->
  eval_bin_op op v1 v2 = Some v ->
  val_rtyped Sigma v1 A1 ->
  val_rtyped Sigma v2 A2 ->
  val_rtyped Sigma v B.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Prove that pure reduction preserves typing. Much of this proof can be copied
from the lemma about [head_step] from week 5. *)

Lemma pure_step_preservation e e' A Sigma :
  pure_step e e' ->
  rtyped Sigma StringMap.empty e A  ->
  rtyped Sigma StringMap.empty e' A.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Prove that head reduction preserves typing. This lemma is more complicated
than the one for pure reduction above. The heap operations will change the heap
from [h] to [h'] so we need to make sure that the new heap [h'] is typed w.r.t.
some heap typing [Σ']. While load and store only change the values of the heap
(so the [Σ] remains unchanged), allocation creates a new reference and thus
changes the structure of [h'] (so the [Σ] needs to be extended). The lemma thus
states that there exists a new heap typing [Σ'] that

1. is a superset of [Σ],
2. can be used to type the resulting expression [e'], and
3. can be used to type the resulting heap [h'].

You should prove the lemma [head_step_preservation] without unfolding
[heap_typed]. All cases that involve [heap_typed] can be proved using the helper
lemmas below (in whose proofs you *need* to unfold [heap_typed]). *)

Lemma heap_typed_load Sigma h l v A :
  heap_typed Sigma h ->
  NatMap.lookup h l = Some v ->
  NatMap.lookup Sigma l = Some A ->
  val_rtyped Sigma v A.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma heap_typed_free Sigma h l :
  heap_typed Sigma h ->
  NatMap.lookup h l = None ->
  NatMap.lookup Sigma l = None.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma heap_typed_store Sigma h l v A :
  heap_typed Sigma h ->
  NatMap.lookup Sigma l = Some A ->
  val_rtyped Sigma v A ->
  heap_typed Sigma (NatMap.insert l v h).
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Hint: Use [val_rtyped_weakening] and lemmas about [NatMap.incl] and
[NatMap.insert] in your proof. *)

Lemma heap_typed_alloc Sigma h l v A :
  NatMap.lookup h l = None ->
  heap_typed Sigma h ->
  val_rtyped Sigma v A ->
  heap_typed (NatMap.insert l A Sigma) (NatMap.insert l v h).
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma head_step_preservation e h e' h' Sigma A :
  head_step e h e' h' ->
  rtyped Sigma StringMap.empty e A ->
  heap_typed Sigma h ->
  exists Sigma',
    NatMap.incl Sigma Sigma' /\
    rtyped Sigma' StringMap.empty e' A /\
    heap_typed Sigma' h'.
Proof. (* FILL IN HERE (20 LOC proof) *) Admitted.

(** Prove that configuration typing is preserved under stepping. *)

Lemma preservation s s' A :
  step s s' ->
  cfg_typed s A ->
  cfg_typed s' A.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

(** Prove progress for binary operators. This proof can be copied from week 5
(you might need some small adjustments if you did not automate your proof using
[eauto]). *)

Lemma bin_op_progress Sigma op v1 v2 A1 A2 B :
  bin_op_typed op A1 A2 B ->
  val_rtyped Sigma v1 A1 ->
  val_rtyped Sigma v2 A2 ->
  exists w,
    eval_bin_op op v1 v2 = Some w /\
    rtyped Sigma StringMap.empty (EVal w) B.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** The following lemmas allows us to step in head position, and under a
context, respectively. They will come in handy in the progress proof. *)

Lemma do_head_step e h e' h' :
  head_step e h e' h' ->
  step (e, h) (e', h').
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma do_ctx_step k e e' h h' :
  ctx k ->
  step (e, h) (e', h') ->
  step (k e, h) (k e', h').
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Prove that well-typed configurations either have a value as their
expression, or they can step.

Hint: For all the "can step" cases you have to use either the lemma
[do_head_step] or [do_ctx_step] above. In some cases, [apply ctx_step] will
figure out the right evaluation context [k], but in some cases it will not. You
can use [apply (ctx_step (fun x => ...)] to give the evaluation context by hand.
For example, [apply (ctx_step (fun x => EStore x (EVal v2)))]. *)

Lemma progress s A :
  cfg_typed s A ->
  final s \/ can_step s.
Proof. (* FILL IN HERE (41 LOC proof) *) Admitted.

(* Prove type safety for our language with mutable references. *)

Lemma preservation_steps s s' A :
  steps s s' ->
  cfg_typed s A ->
  cfg_typed s' A.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Theorem safety e A :
  typed StringMap.empty e A ->
  safe (e,NatMap.empty).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
