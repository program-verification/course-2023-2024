From pv Require Export library.

(* ########################################################################## *)
(** * Introduction *)
(* ########################################################################## *)

(* This file contains challenging exercises for students who already have
experience with Coq or would like to exercise more. These challenges are
optional.

IMPORTANT: This file is not a substitute for the material of week 1-3. Even if
you have experience with Coq (for example, because you have completed the course
IMC010: Type Theory and Coq), you should go through the material of these weeks.
There are a number of tactics, notions, and conventions that are likely not
covered in the Coq course you have taken. In particular, pay careful attention
to tactics and introduction patterns (week 2) and finite maps (week 3). *)


(** ######################################################################### *)
(** * Challenge #1: Divisibility *)
(** ######################################################################### *)

(** Prove the lemma [factorial_divides] below. The proof of this lemma involves
a tricky induction. Make sure to use [lia] to automate (the rather excruciating)
reasoning about natural numbers. *)

Fixpoint factorial (n : nat) : nat :=
  match n with
  | O => 1
  | S n => factorial n * S n
  end.

Definition divides (n m : nat) : Prop := exists k, m = k*n.

Lemma factorial_divides n m :
  divides (factorial n * factorial m) (factorial (n + m)).
Proof. (* FILL IN HERE (9 LOC proof) *) Admitted.


(** ######################################################################### *)
(** * Challenge #2: Permutations *)
(** ######################################################################### *)

(** To define the notion of permutations in Coq, we can either define an
algorithm that computes all permutations of a list, or state a mathematical
definition that specifies if a list is a permutation of another. In this
challenge, we define both versions and prove that they correspond:

- Soundness (`permutations_sound`): `In xs2 (permutations xs1) -> perm xs1 xs2`
- Completeness (`permutations_complete`): `perm xs1 xs2 -> In xs2 (permutations xs1)`

*)

Fixpoint interleave {A} (x : A) (xs : list A) : list (list A) :=
  match xs with
  | [] => [[x]]
  | x' :: xs => (x :: x' :: xs) :: map (cons x') (interleave x xs)
  end.
Fixpoint permutations {A} (xs : list A) : list (list A) :=
  match xs with
  | [] => [[]]
  | x :: xs => concat (map (interleave x) (permutations xs))
  end.

Compute interleave 0 [1;2;3].
(** Gives [ [[0; 1; 2; 3]; [1; 0; 2; 3]; [1; 2; 0; 3]; [1; 2; 3; 0]] ] *)

Compute permutations [1;2;3].
(** Gives [ [[1; 2; 3]; [2; 1; 3]; [2; 3; 1]; [1; 3; 2]; [3; 1; 2]; [3; 2; 1]] ] *)

(** The definition of the algorithm [permutations] is rather complicated. Aside
from testing this algorithm, we would like to prove a property to increase our
confidence in its correctness. To do so, we give a mathematical definition of
what it means to be a permutation, and prove that any list in [permutations xs]
is a permutation of [xs]. *)

(** We mathematically define [xs1] to be a permutation of [xs2] if both lists
have the same length, and there exists an injection that maps the indices of the
one list to the other while preserving the values of the elements. (Note that
the condition [length xs1 = length xs2] is essential, take [S] as a counter
example. You could omit the length condition if you require [f] to be bijective
rather than injective.) *)

Definition injective {A B} (f : A -> B) : Prop :=
  forall x1 x2, f x1 = f x2 -> x1 = x2.

Definition perm {A} (xs1 xs2 : list A) :=
  exists f : nat -> nat,
    length xs1 = length xs2 /\
    injective f /\
    forall i, nth_error xs1 i = nth_error xs2 (f i).

(** Let us prove some basic properties of permutations *)

(** Clearly, a list should be a permutation of itself, i.e., [perm l l] for any
list [l]. This result follows from the fact that the identity function is
injective. *)

Lemma injective_id {A} : injective (@id A).
Proof. by unfold injective. Qed.

Lemma perm_refl {A} (l : list A) : perm l l.
Proof.
  exists id.
  split; [|split].
  - done.
  - apply injective_id.
  - done.
Qed.

(** To show that we have [perm (x :: y :: l) (y :: x :: l)] we need to construct
a function that swaps the indices of the first two elements. This happens to be
the [swap] function we defined in week 3. *)

Definition swap : nat -> nat :=
  fun i =>
    match i with
    | 0 => 1
    | 1 => 0
    | _ => i
    end.

Lemma injective_swap : injective swap.
Proof. intros [|[|i']] [|[|j']]; simpl; lia. Qed.

Lemma perm_swap {A} (l : list A) x y :
  perm (x :: y :: l) (y :: x :: l).
Proof.
  exists swap. (** Only the indices of the first two elements are swapped, so
  we use the [swap] function that we defined previously. *)
  split; [|split]; simpl.
  - done.
  - apply injective_swap.
  - by intros [|[|i]].
Qed.

(** Now that you have seen how to prove some lemmas about [perm], you should
prove the lemmas below. A key step a number of proofs is picking the right
injection for mapping the indices. (Hint: For some lemmas, you can borrow
functions and results from week 3, for some you have to define your own
functions.) *)

Lemma perm_cons {A} (xs1 xs2 : list A) x :
  perm xs1 xs2 ->
  perm (x :: xs1) (x :: xs2).
Proof. (* FILL IN HERE (19 LOC helpers and 8 LOC proof) *) Admitted.

Lemma perm_trans {A} (xs1 xs2 xs3 : list A) :
  perm xs1 xs2 -> perm xs2 xs3 -> perm xs1 xs3.
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

(** In the following two lemmas, do not forget that you might have to generalize
the induction hypothesis. *)

Lemma interleave_sound {A} (xs1 xs2 : list A) x :
  In xs2 (interleave x xs1) ->
  perm (x :: xs1) xs2.
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.

Lemma permutations_sound {A} (xs1 xs2 : list A) :
  In xs2 (permutations xs1) ->
  perm xs1 xs2.
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.

(** Prove completeness. Although the lemma statement is simple, the proof is
very difficult. You need to invent a number of non-trivial helping lemmas. (The
teachers used 1 definition and 8 lemmas, let us know if you can do better!). *)

Lemma permutations_complete {A} (xs1 xs2 : list A) :
  perm xs1 xs2 ->
  In xs2 (permutations xs1).
Proof. (* FILL IN HERE (90 LOC helpers and 7 LOC proof) *) Admitted.

(** Prove that [perm] is symmetric. Intuitively, this property holds because
any injection between finite sets with the same cardinality is a bijection.
However, proving that property in Coq might be a lot of work.

There exists an alternative proof of [perm_sym] that does not involve unfolding
the definition of [perm]. (The proof of the teachers is only 5 lines, but uses
some of the lemmas about [perm] that we used in the previous exercise.) *)

Lemma perm_sym {A} (xs1 xs2 : list A) :
  perm xs1 xs2 ->
  perm xs2 xs1.
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

(** The number of permutations of a list of length [n] is equal to
[factorial n]. Convince Coq of this result by proving the following lemma.
The proof is not very hard, but you need to come up with some helper lemmas
about [length]. All of these helper lemmas should be provable by a simple
induction. *)

Lemma permutations_length {A} (xs : list A) :
  length (permutations xs) = factorial (length xs).
Proof. (* FILL IN HERE (13 LOC helpers and 4 LOC proof) *) Admitted.


(** ######################################################################### *)
(** * Challenge #3: Finite maps *)
(** ######################################################################### *)

Section fin_maps.
Import NatMap.

(** Prove the following lemma that gives a "positive" account of the
non-disjointness. In classical logic, this lemma trivially follows from excluded
middle/double negation, but in constructive logic like Coq the left-to-right
direction is non-trivial since you have to give explicit witnesses [k], [x1],
and [x2]. For this direction you will need to perform finite map induction:

     P empty    (forall k x m, lookup m k = None -> P m -> P (insert k x m))
    -------------------------------------------------------------------------
                                 forall m, P m

With the tactic [induction m as [|k x m Hk IH] using map_ind] you can use finite
map induction in your proof. *)

Lemma not_disjoint_alt {A} (m1 m2 : natmap A) :
  ~disjoint m1 m2 <->
    exists k x1 x2, lookup m1 k = Some x1 /\ lookup m2 k = Some x2.
Proof. (* FILL IN HERE (10 LOC proof) *) Admitted.

(** Define a function [imap f m]. The result of [imap f m] is the map:

    { (k,y) | lookup m k = Some x and f k x = Some y }

In other words, it applies [f] to each key/value pair. If the result of [f]
is [Some], the value is updated. If the result of [f] is [None], the key/value
pair is removed.

To program this function, you need to make use of the function
[dom : natmap A -> list nat] that gives the domain of a map. You might need a
number of auxiliary functions. *)

Definition imap {A B}
    (f : nat -> A -> option B) (m : natmap A) : natmap B. (* FILL IN HERE *) Admitted.

(** Prove that your function behaves as expected. This lemma is challenging to
prove, and requires a number of auxiliary results that you might need to prove
by induction. *)

Lemma lookup_imap {A B} (f : nat -> A -> option B) m k :
  lookup (imap f m) k = option_bind (f k) (lookup m k).
Proof. (* FILL IN HERE (23 LOC proof) *) Admitted.

End fin_maps.
