From pv Require Import library.

(*
  Student 1: FILL IN YOUR NAME HERE (FILL IN YOUR STUDENT NUMBER HERE)
  Student 2: FILL IN YOUR NAME HERE (FILL IN YOUR STUDENT NUMBER HERE)
*)

(* ########################################################################## *)
(** * Introduction *)
(* ########################################################################## *)

(** Deliverables: You should hand in a single Coq file with your solution. The
project consists of a basic part and several extensions. You have to pick
**at least** one extension. You are advised to first complete the basic part
of the project before starting to work on an extension. *)

(** The grade of the project is based on the following criteria:

1. Difficulty: Depending on the difficulty of the extension, you can get a
   higher grade. You are also allowed to do multiple extensions if you want a
   very high grade. (Note that doing multiple extensions does not guarantee a
   high grade, you should also score well on the other criteria.)
2. Correctness: Whether the Coq definitions and lemmas correctly model the
   problem at hand.
3. Completeness: Whether all lemmas are proven (i.e., no lemmas are omitted and
   no proofs are finished with [admit] or [Admitted]).
4. Style: Whether the Coq code follows sensible style guidelines (consistent
   indentation, proper use of parentheses, sensible variable names, proper use
   of implicit arguments, documentation where needed, etc.).
5. Effectiveness: Whether the definitions and proofs are carried out effectively
   (no redundant proof steps, no use of the induction tactic for lemmas that can
   be proved without induction).
*)

(* ########################################################################## *)
(** * Definitions: Syntax and interpreter *)
(* ########################################################################## *)

(** During the lectures, we have studied two styles of semantics: big step and
small step semantics. In this project, we take a look at yet another approach:
an interpreter. In the basic part, you have to prove an equivalence between
the semantics given by the interpreter and the big step semantics. *)

(** Our language is the same as the one from week 4 and 5; but we remove lambdas
and add let bindings as primitives instead. *)

Inductive bin_op :=
  | AddOp
  | SubOp
  | LeOp
  | LtOp
  | EqOp.

Inductive val :=
  | VBool : bool -> val
  | VNat : nat -> val.

Inductive expr :=
  | EVar : string -> expr
  | EVal : val -> expr
  | ELet : string -> expr -> expr -> expr
  | EOp : bin_op -> expr -> expr -> expr
  | EIf : expr -> expr -> expr -> expr.

Definition eval_bin_op (op : bin_op) (v1 v2 : val) : option val :=
  match op, v1, v2 with
  | AddOp, VNat n1, VNat n2 => Some (VNat (n1 + n2))
  | SubOp, VNat n1, VNat n2 => Some (VNat (n1 - n2))
  | LeOp, VNat n1, VNat n2 => Some (VBool (Nat.leb n1 n2))
  | LtOp, VNat n1, VNat n2 => Some (VBool (Nat.ltb n1 n2))
  | EqOp, VNat n1, VNat n2 => Some (VBool (Nat.eqb n1 n2))
  | EqOp, VBool n1, VBool n2 => Some (VBool (Bool.eqb n1 n2))
  | _, _, _ => None
  end.

(** We define an interpreter as a function

  eval : stringmap val -> expr -> option val

The interpreter takes a variable environment [stringmap val] that assigns
values to all free variables in the expression. The interpreter returns [Some v]
if the expression successfully evaluates to [v], and it returns [None] if there
is an error. In particular, the interpreter returns [None] if the expression
has a free variable that is not bound by any [ELet]. *)

Fixpoint eval (st : stringmap val) (e : expr) : option val :=
  match e with
  | EVal v => Some v
  | EVar x => StringMap.lookup st x
  | ELet x e1 e2 =>
     match eval st e1 with
     | Some n1 => eval (StringMap.insert x n1 st) e2
     | None => None
     end
  | EOp op e1 e2 =>
     match eval st e1, eval st e2 with
     | Some v1, Some v2 => eval_bin_op op v1 v2
     | _,_ => None
     end
  | EIf e1 e2 e3 =>
     match eval st e1 with
     | Some (VBool true) => eval st e2
     | Some (VBool false) => eval st e3
     | _ => None
     end
  end.

(** To evaluate a closed expression we do [eval] in the empty environment. *)

Definition eval0 := eval StringMap.empty.

(** We also define a big step semantics as a relation in the same way we have
done in the homework of week 4 and week 5. The big step semantics does not keep
track of a variable environment. Instead, it immediately substitutes the
variables upon encountering an [ELet]. For this, we define the usual
substitution function [subst]. *)

Fixpoint subst (x : string) (w : val) (e : expr) : expr :=
  match e with
  | EVal _ => e
  | EVar y => if String.eq_dec y x then EVal w else EVar y
  | ELet y e1 e2 =>
     ELet y (subst x w e1) (if String.eq_dec y x then e2 else subst x w e2)
  | EOp op e1 e2 => EOp op (subst x w e1) (subst x w e2)
  | EIf e1 e2 e3 => EIf (subst x w e1) (subst x w e2) (subst x w e3)
  end.

Inductive big_step : expr -> val -> Prop :=
  | Val_big_step v :
     big_step (EVal v) v
  | Let_big_step y e1 v1 e2 v2 :
     big_step e1 v1 ->
     big_step (subst y v1 e2) v2 ->
     big_step (ELet y e1 e2) v2
  | Op_big_step e1 e2 op v1 v2 v :
     big_step e1 v1 ->
     big_step e2 v2 ->
     eval_bin_op op v1 v2 = Some v ->
     big_step (EOp op e1 e2) v
  | If_big_step_true e1 e2 e3 v :
     big_step e1 (VBool true) ->
     big_step e2 v ->
     big_step (EIf e1 e2 e3) v
  | If_big_step_false e1 e2 e3 v :
     big_step e1 (VBool false) ->
     big_step e3 v ->
     big_step (EIf e1 e2 e3) v.


(* ########################################################################## *)
(** * Proof: Equivalence of the interpreter and big step semantics *)
(* ########################################################################## *)

(** Prove the following theorem that states that the big step semantics and
interpreter are equivalent in the sense that they assign the same return
values to expressions.

Important: This theorem involves function [eval0], which is defined in terms
of the recursive function [eval]. Since [eval0] itself is not recursive, it is
unlikely that you can prove this property directly by induction. You should
figure out a way how to state a generalized "helping" theorem that expresses an
equivalence between [eval] and [big_step]. You should prove that theorem first,
and then prove that the property about [eval0] follows as a consequence.

To state this "helping" theorem, you might need to introduce some auxilary
definitions. *)

Theorem eval0_big_step_iff e n : eval0 e = Some n <-> big_step e n.
Proof. (* FILL IN HERE (71 LOC helpers and 1 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Extensions *)
(* ########################################################################## *)

(** Carry out at least one of the extensions below. *)

(* ========================================================================== *)
(** ** Extension 1 (easy) *)
(* ========================================================================== *)

(** We have defined [eval] as a function, and it uses variable environments.
We have defined [big_step] as a relation, and it uses substitution.

Could we have defined [eval] with variable environments as an inductive relation
(i.e., [Inductive])?

- If you answer "yes", give the definition and prove that it is equivalent to
  [eval].
- If your answer is "no", explain what trouble we run into.

Could we have defined [big_step] with substitution as a recursive function
(i.e., [Fixpoint])?

- If you answer "yes", give the definition and prove that it is equivalent to
  [big_step].
- If your answer is "no", explain what trouble we run into. *)

(* ========================================================================== *)
(** ** Extension 2 (moderate)*)
(* ========================================================================== *)

(** Define a type system for the language and prove a type soundness result for
your interpreter. That is, prove that if an expression is well-typed [eval0]
always returns a [Some]. *)

(* ========================================================================== *)
(** ** Extension 3 (moderate) *)
(* ========================================================================== *)

(** Add references to the language. You can obtain a big step semantics by
taking the small step semantics from week 6 as inspiration. You should design
your own solution for adapting the interpreter [eval]. Adapt the equivalence
proof to show that your new big step semantics and interpreter are equivalent. *)

(* ========================================================================== *)
(** ** Extension 4 (difficult) *)
(* ========================================================================== *)

(** Extend the language with lambdas (since the interpreter does not use
substitution, you need to think carefully how to represent closures). The
language extended with lambdas is no longer terminating (you can implement the
Ω combinator or a fixpoint combinator).

- Explain why we can no longer define the semantics as a function
  [eval : stringmap val -> expr -> option val].
- Explain why we can still define the semantics as a relation
  [big_step : expr -> val -> Prop] (we have done that in week 4 and week 5).

Instead we could define [eval_fuel : stringmap val -> nat -> expr -> result],
where the [nat] gives a so called "fuel" that ensures that the number of steps
is bounded. The return type [result] is defined as follows: *)

Inductive result :=
  | OutOfFuel : result
  | Stuck : result
  | Terminates : val -> result.

(** This function executes the expression, and returns [OutOfFuel] if the [nat]
was not big enough. It returns [Stuck] if there is an error (say, [true + 10]),
and [Terminates v] if the expression has result [v].

Define [eval_fuel] and prove its equivalence with your [big_step] semantics.
Think carefully about how to state the equivalence (how should the fuel appear
in the theorem statement?). *)
